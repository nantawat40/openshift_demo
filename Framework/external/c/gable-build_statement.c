/*
#---------------------------------------------------------------------------
#
#              Copyright C - 2010 All rights reserved
#                        G-Able Thailand.
#
#       No part of this program may be reproduced or adapted in any
#             form or by any means, electronic or mechanical,
#       without permission from G-Able Thailand.
#          This program is confidential and may not be disclosed,
#          decompiled or reverse engineered without permission in
#             writing from G-Able Thailand.
#
#--------------------------------------------------------------------
# DESCRIPTION:
#    transform source file input to framework
#
#--------------------------------------------------------------------
# Revision   Date        CR No.   Author         Description
#====================================================================
#  0.2       08/03/2020  XXX      Suebsakul.A    Project Push to SEE, Your DATA
#
#
#
#
*/

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdlib.h>

#if __linux__ 	
	#include <sys/time.h>
	#include <sys/types.h>
	#include <sys/uio.h>
#endif

#include "getopt.h"

#define	MAXBLK 1024
#define iY2K(year)      (pTm->tm_year<2000 ?1900+pTm->tm_year :pTm->tm_year)
#define TRACE			0	//This use for open trance log
#define VERIFY_OUTPUT	0	//This use verify output automatic but effect performance of script

#define ST_CODE_SUCCESS         0
#define ST_CODE_ERR_PARAMETER  13

//This vartiable for verify statu and prove output for converted hex double to double
int  show_verbose=0;

int print_usage(int argc,char *argv[])
{
	fprintf(stderr,"Invalid parameters: %s -c <CONFIG.yaml> -q <QUERY_STR> [ -v -s ]\n",argv[0]);
	fprintf(stderr,"\t -c FILE Config\t\t: Configuration file\n");
	fprintf(stderr,"\t -q String Query\t\t: Statement query\n");
	fprintf(stderr,"\t -v Verbose for show stat to standard error\n");
	// fprintf(stderr,"\t -s Show Summery but only field's type double\n");
	fprintf(stderr,"e.g. %s -conf config_build.yaml -query \"Select 1 from dual;\"\n",argv[0]);
	exit(ST_CODE_ERR_PARAMETER);
}

int main(int argc, char *argv[])
{
	//Declare variable
	int c; 
	int summary=0;

	double sum_bignumber[256];

	char szData_File[256];
	char szQueryStatement[256];

    // Check invalid argment
    if (argc <= 3 || argc > 6)
	{
		print_usage(argc,argv);
	}


    while((c =  getopt(argc, argv, "c:q:vs")) != EOF) 
    { 
        switch (c) 
        { 
			case 'c': 
				//cout << optarg << endl; 
				if(access(optarg,R_OK) !=0 ) //Can't read file file
				{
					fprintf(stderr,"Can't read/access data file!!!\n");
					fprintf(stderr,"FILE_CONFIG : [%s]\n",optarg);
					exit(13);
				}

				strcpy(szData_File,optarg);
				break; 
			case 'q': 
                 //cout << optarg << endl; 		

				// if(access(optarg,R_OK) !=0 ) //Can't read file file
				// {
				// 	fprintf(stderr,"Can't read/access data file!!!\n");
				// 	fprintf(stderr,"FILE_CONTROL : [%s]\n",optarg);
				// 	exit(13);
				// }

				strcpy(szQueryStatement,optarg);
				break; 
			case 'v':
				show_verbose=1;
				break;
			case 's':
				summary=1;
				memset(sum_bignumber,0x0,sizeof(sum_bignumber));
				break;
			// case ':':
			// case '?': 
            //      //cerr << "Missing option." << endl; 
			// 	if (optopt == 'f')
			// 		fprintf(stderr,"-%c filename (input file)\n",optopt);
		    //     else if (isprint (optopt))
			// 		fprintf (stderr, "Unknown option `-%c'.\n", optopt);
			// 	else
			// 		fprintf(stderr,"Missing option. [-%c]\n",optopt);

			// 	exit(13); 

			// 	break; 
				
        } 
    }


    printf("sssssss\n");

    return(ST_CODE_SUCCESS);    
}
