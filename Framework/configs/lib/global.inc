#!/bin/bash

#--------------------------------------------------------------------
#              Copyright C - 2012 All rights reserved
#                        G-Able Thailand.
#
#       No part of this program may be reproduced or adapted in any
#             form or by any means, electronic or mechanical,
#       without permission from G-Able Thailand Thailand.
#          This program is confidential and may not be disclosed,
#          decompiled or reverse engineered without permission in
#             writing from G-Able Thailand Thailand.
#
#--------------------------------------------------------------------
# DESCRIPTION:
# Global library
#
#--------------------------------------------------------------------
# Revision   Date	 CR No.	      Author        Description
#====================================================================
#      1.0   02/01/2020	 CRxxx	  Suebsakul A.  Program Initialed
#
#--------------------------------------------------------------------

#Force run profile for support Job Schduler
. $HOME/.profile

#Initial variable
HOME_ETL=$HOME/Framework
CONF_PATH=$HOME/Framework/configs
TMP_PATH=$HOME/Framework/output/tmp
LOG_PATH=$HOME/Framework/output/logs
CREATE_PATH=$HOME/Framework/create
DEPLOY_PATH=$HOME/Framework/deploy
LND_PATH=$HOME/Framework/landing
INPUT_PATH=$HOME/Framework/input
TEST_PATH=$HOME/Framework/output/tests
ARCHIVE_PATH=$HOME/Framework/archive

MAIN_CONFIG=$CONF_PATH/common/main.config

#set return fail on any pipe
set -o pipefail

#Define environment variable LANG because syncsort production not allow for define this variable to etl user
#LANG=en_US.utf8
#LC_ALL=en_US.utf8
#export LANG LC_ALL

ST_COMPLETE=0
ST_COMPLETE_NAME='Completed'
ST_BATCH_PROCESSING=1
ST_BATCH_PROCESSING_NAME='Another process is running'
ST_NO_INPUT=11
ST_NO_INPUT_NAME='No input file'
ST_ZERO_BYTE_FILE=12
ST_ZERO_BYTE_FILE_NAME='Zero byte file'
ST_CONVERSION_ERROR=13
ST_CONVERSION_ERROR_NAME='C Conversion Error'
ST_INSERT_STG_ERROR=14
ST_INSERT_STG_ERROR_NAME='Insert STG error'
ST_RECONCILE_REC_ERROR=15
ST_RECONCILE_REC_ERROR_NAME='Reconcile number of record error'
ST_RECONCILE_SUM_ERROR=16
ST_RECONCILE_SUM_ERROR_NAME='Reconcile sum amount in'
ST_COMPLETE_WITH_CONDITION=20
ST_COMPLETE_WITH_CONDITION_NAME='Completed with condition'
ST_UNABLE_INSERT_FACT=21
ST_UNABLE_INSERT_FACT_NAME='Unable to insert Fact '
ST_OVERLAP_PERIOD=22
ST_OVERLAP_PERIOD_NAME='Error overlap period'
ST_RECORD_ZERO=23
ST_RECORD_ZERO_NAME='Error zero record on stg table'
ST_FACT_RECORD_ZERO=24
ST_FACT_RECORD_ZERO_NAME='No transaction on fact table'
ST_UNABLE_TRUNCATE_TABLE=31
ST_UNABLE_TRUNCATE_TABLE_NAME='Unable to truncate table'
ST_UNABLE_INSERT_COMMON=32
ST_UNABLE_INSERT_COMMON_NAME='Unable to insert Common'
ST_NONEW_OR_INSERT=33
ST_NONEW_OR_INSERT_NAME='No new or updated records'

#ST_UNABLE_INSERT_TMP=34
#ST_UNABLE_INSERT_TMP_NAME='Unable to insert temp table'
ST_UNABLE_TRUNCATE_PERIOD=34
ST_UNABLE_TRUNCATE_PERIOD_NAME='Unable to truncate table'

ST_UNABLE_INSERT_DIM=35
ST_UNABLE_INSERT_DIM_NAME='Unable to insert dimension'

ST_UNABLE_INSERT_SUM=36
ST_UNABLE_INSERT_SUM_NAME='Unable to insert summary'
ST_UNABLE_TRUNCATE_SUM=37
ST_UNABLE_TRUNCATE_SUM_NAME='Unable to truncate table summary'

ST_PREMART_RECORD_ZERO=38
ST_PREMART_RECORD_ZERO_NAME='No transaction on premart table'

ST_UNABLE_DELETE_RECORD=39
ST_UNABLE_DELETE_RECORD_NAME='Unable to delete records'

ST_FACT_DUPLICATE=51
ST_FACT_DUPLICATE_NAME='Duplicate data found between STG and FACT'

ST_UNABLE_PURG_FACT=41
ST_UNABLE_PURG_FACT_NAME='Unable to purging data in fact table'
ST_UNABLE_CREATE_SPOOL=42
ST_UNABLE_CREATE_SPOOL_NAME='Unable to create spool file'
ST_UNEXPECTED_ERROR=99
ST_UNEXPECTED_ERROR_NAME='Unexpected error'

#new error code in dwh enhancement
ST_ERR_PARAM=51
ST_ERR_PARAM_NAME='Paramete messing'
ST_ERR_FILE_ACCESS=52
ST_ERR_FILE_ACCESS_NAME='No input file for landing'
ST_ERR_FILE_ZERO=53
ST_ERR_FILE_ZERO_NAME='Zero byte file'
ST_ERR_FILE_UNKNOWN_FILE=54
ST_ERR_FILE_UNKNOWN_FILE_NAME='Error unknown file type'
ST_ERR_FILE_WRITE=55
ST_ERR_FILE_WRITE_NAME='Can not write file'
ST_ERR_REC_UNKNOWN=56
ST_ERR_REC_UNKNOWN_NAME='Found unknown record'
ST_ERR_REC_MISSING=57
ST_ERR_REC_MISSING_NAME='Amount record input <> Output'

ST_ERR_SQL_EXECUTE=58
ST_ERR_SQL_EXECUTE_NAME='Executing SQL Error'

ST_UNKNOW_ERROR=99
ST_UNKNOW_ERROR_NAME='Unknow Error'

ST_CODE=$ST_COMPLETE
ST_NAME=$ST_COMPLETE_NAME

#--------------------------------------
# Load Global Configuration
#--------------------------------------
TEST_DATE_YYYYMMDD=19990101
MAX_STG_LENGTH__FIELDS=256
#--------------------------------------
# Global Functions
#--------------------------------------
_usage()
{
  echo "Invalid parameters: $EXEC_NAME $EXEC_PARA"
  exit $ST_UNEXPECTED_ERROR
}

_logTst()
{
  MESSAGE="$1"

  if [ -f "$MESSAGE" ]
  then
    # echo "File access..."
    cat ${MESSAGE} >> $2    
  else
    # echo "Not File... [$MESSAGE]"
    # replace datetime e.g. Mar 02 00:19:19 to MMM DD HH:MM:SS    
    # echo "${MESSAGE/[JFMAJSOND][a-z][a-z] [0-9][0-9] [0-9][0-9]:[0-9][0-9]:[0-9][0-9]/MMM DD HH:MM:SS}" >> $2

    # replace datetime e.g. 2020-04-02 00:19:19 to YYYY-MM-DD HH:MM:SS    
    echo "${MESSAGE/20[2-9][0-9]-[01][0-9]-[0-9][0-9] [012][0-9]:[0-9][0-9]:[0-9][0-9]/YYYY-MM-DD HH:MM:SS}" >> $2


  fi

  
}

# System Logger
_logger()
{
  # format date's Apr 02 00:21:56
  # FORM_DATE=`date +"%b %d %T"`

  PID=$$

  # format date's 2020-04-02 01:15:51
  FORM_DATE=`date +"%Y-%m-%d %T"`
  
  MESSAGE="$FORM_DATE $EXEC_NAME: ${PID}: $1"

  #remove folder home_etl
  

  #echo "$MESSAGE"
  #echo "$MESSAGE" >> $2

  # Replace all matches of $substring with $replacement.
  # echo ${stringZ/abc/xyz}       # xyzABC123ABCabc
  # echo ${stringZ//abc/xyz}      # xyzABC123ABCxyz
  echo  ${MESSAGE//$HOME_ETL\//\~../}
  echo "${MESSAGE//$HOME_ETL\//\~../}" >> $2

  wait
}

# System loggerError
_logError()
{
  exec 10<&0
  # >stdin
  exec < $1
  # >
  while read LINE; do
    #_logger "$LINE" $2
    echo "$LINE"
    echo "$LINE" >> $2
  done
}

#Function read file to veriable
_readFile()
{
#  exec 10<&0
#  # >stdin
#  exec < $1
#  # >
#  let cnt=1
#  while read LINE; do
#    if [ $cnt -eq $2 ]
#    then
#      echo "$LINE"
#      return
#    fi
#    ((cnt++))
#  done

  echo `awk "NR==$2 {print;exit}" $1`

}

_fileToLogger()
{
  FILE_SHOW=$1
  FILE_LOG=$2
  cat $FILE_SHOW | while read MSG
  do
    _logger "$MSG" $FILE_LOG
  done

}

#Function for calculate ElapseTime
_calcElapsedTime()
{
  local elapsed="$(($(date +%s%N)-$1))"
  #local elapsed="$((${SECONDS}-$1))"

  local OS=`uname`

  if [ $OS = "Linux" ]
  then
	  local sec="$((elapsed/1000000000))"
	  local mil="$((elapsed%1000000000/1000000))"
  else
	  local sec="$((elapsed/100))"
	  local mil="$((elapsed%100))"
  fi

  if [ $mil -lt 100 ]
  then
    mil="0${mil}"
  fi

  echo "$sec.$mil"
  #echo "$elapsed"
}

#Function for get only filename
#parameter ($Filename)
_getFilename()
{
  #basename $1 | split
  filename=$(basename -- "$1")
  # extension="${filename##*.}"
  filename="${filename%.*}"

  echo "$filename"
}

#Function for get only extension
#parameter ($Filename)
_getExtension()
{
  #basename $1 | split
  filename=$(basename -- "$1")
  extension="${filename##*.}"
  # filename="${filename%.*}"

  echo "$extension"
}

# 
_getValueYAML()
{  
  FILE_YAML=$1
  NODE=$2
  KEY=$3
  
  VALUE=$(grep -A20 "${NODE}:" $FILE_YAML | grep "${KEY}: " | awk '{print $2}')

  echo "$VALUE"
}

# Prevent Multiple Instance running in the same time
_preventMultipleInstance()
{
  local OS=`uname`

  _logger "Checking another running process..." $3
  
  # Check for running process including itself and sub process
  
  #Check access file.proc
  if [ -s $2 ]
  then
    _logger "Found file : $2" $3
    _logger "STATUS=$ST_BATCH_PROCESSING $ST_BATCH_PROCESSING_NAME $CountProcess" $3
    _logger "END" $3
    exit $ST_BATCH_PROCESSING
  fi

  # When variable USER is Blank
  if [ -z $USER ]
  then
    USER=$(whoami)
  fi

  #ps -ef | grep 'ut_batch.sh -s Warehouse' | grep -v grep

  # Write result getting process to file.proc
  # command by Suebsakul.A because not wait child
  # wait
  sleep .5
  

  PID=$$

  if [ "$OS" == "Linux" ]
  then    

	  # ps -ef  | grep -v " $PID " | grep -e "$1\$" | grep $USER | grep -v grep | sort > $2

    # switch off Exit immediately if a command exits with a non-zero status
    set +e

    # fixed problem linux to show script name e.g. 'dwh_type01.sh.*\ HW_KACE_MASTER' to 'dwh_type01.sh -s HW_KACE_MASTER'
    # echo "Script : $1"
    SCRIPT_NAME=$(echo "$1" | sed 's/\.\*\\/ -s/g')

    ps -ef  | grep -v " $PID " | grep -v $SCRIPT_NAME | grep $USER | sort | grep -v grep  > $2

    # switch on Exit immediately if a command exits with a non-zero status
    set -e
    
  else
	  #HP-UX
	  ps -efx | grep -v " $PID " | grep -e "$1\$" | grep $USER | grep -v grep | sort > $2
  fi

  #CountProc=`ps -efx | grep -e "$1" | grep -v grep | wc -l`
  CountProc=$(cat $2 | wc -l)
 
  if [ $CountProc -gt 0 ]
  then
  
    _logger "PID's $PID of current process" $3
    _logger "List found another process running..." $3

    cat $2 | while read line
    do
        _logger "$line" $3
    done

    _logger "STATUS=$ST_BATCH_PROCESSING $ST_BATCH_PROCESSING_NAME $CountProcess" $3
    _logger "END" $3

    # Clear file.proc
    rm -f $2

    exit $ST_BATCH_PROCESSING
  fi

  
  # Clear file.proc
  rm -f $2
}

# Format Number #,##0
# require set locale to en_US.UTF-8
_format()
{
  if echo $1 | egrep -q '^[0-9]+$';
  then
    echo `printf "%'d" $1`
  elif echo $1 | egrep -q '^[0-9.]+$';
  then
    echo `printf "%'.2f" $1`
  else
    #comment by Suebsakul.A 27-06-2014 Input "DD/MM/YYYY HH:MI:SS" Output lost value time
    #echo `printf "%'s" $1`
    echo "---error---"
    echo "$1"
  fi
}


_length()
{
  echo `expr length $1`
}

_chkDate()
{
  LEN=$(_length $1)

  if [ $LEN -ne 8 ]
  then
    echo "Invalid date '$1'"
    exit $ST_UNEXPECTED_ERROR
  elif echo $1 | egrep -vq '^[0-9]+$';
  then
      echo "Invalid date '$1'"
      exit $ST_UNEXPECTED_ERROR
  else
    

    CURDT=`date "+%Y%m%d"`

    ty=`echo $1 |cut -b1-4` # today year
    tm=`echo $1 |cut -b5-6` # today month
    td=`echo $1 |cut -b7-8` # today day 
    
    if [ $tm -gt 12 -o $td -gt 31 ]
    then
        echo "Invalid date '$1'"
	    exit $ST_UNEXPECTED_ERROR
    elif [ $td -eq 31 ] && [ $tm -eq 4 -o $tm -eq 6 -o $tm -eq 9 -o $tm -eq 11 ]
    then
        echo "Invalid date '$1'"
        exit $ST_UNEXPECTED_ERROR
    elif [ $tm -eq 2 -a $td -gt 29 ]
    then
	    echo "Invalid date '$1'"
	    exit $ST_UNEXPECTED_ERROR
    elif [ $tm -eq 2 -a `expr $ty % 4` -ne 0 -a $td -eq 29 ]
    then
	     echo "Invalid date '$1'"
	     exit $ST_UNEXPECTED_ERROR
    elif [ $1 -gt $CURDT ]
    then
	     echo "Invalid date '$1' because not support future date"
	     exit $ST_UNEXPECTED_ERROR
    fi



  fi
}

# Verity date input
_chkDateMonth()
{
  LEN=$(_length $1)
  if [ $LEN -ne 6 ]
  then
    echo "Invalid date '$1'"
    exit $ST_UNEXPECTED_ERROR
  else
    # Verify Digit
    #if [[ ! $1 =~ [0-9]\{6\} ]]
    if echo $1 | egrep -vq '^[0-9]+$';
    then
      echo "Invalid date '$1'"
      exit $ST_UNEXPECTED_ERROR
    fi
    # Verify Month
    month=${1:4:2}
    if [ $month -gt 12 ]
    then
      echo "Invalid date '$1'"
      exit $ST_UNEXPECTED_ERROR
    fi
  fi
}

# Remove carriage return
function _removeCR()
{
  
  # local str=`echo "$1" | tr -d '\r' | tr -d '\n' | tr -d '\t'`

  # fixed problem remove but not space on variable sql
  local str=`echo "$1" | tr -d '\r' | tr -d '\n' | tr '\t' ' ' | tr '  ' ' '`
  echo "$str"
}

function _replace()
{
  local str=$(_removeCR "$1")
  local old=$2
  local new=$3

  local result=${str//$old/$new}
  echo "$result"
}

function _getDBresult()
{
  
  local SQL_QUERY_GET_RESULT=$(echo $1)


  # switch off Exit immediately if a command exits with a non-zero status
  # set +e


  ${DATABASE_CLIENT_COMMAND}  --user ${DATABASE_ETL_USERNAME} \
                          --password=${DATABASE_ETL_PASSWORD} \
                          >$RESULT_FILE 2>$ERROR_FILE \
                          --unbuffered --silent --skip-column-names \
  <<EOF                           
  $SQL_SELECT_DATABASE
  $SQL_QUERY_GET_RESULT
  \q
EOF

  local RESULT=$?

  # echo "$RESULT"

  # switch on Exit immediately if a command exits with a non-zero status
  # set -e  

  if [ $RESULT -ne 0 -o -s $ERROR_FILE ]
  then
      #_logError $ERROR_FILE $LOG_FILE
      #ERROR=$(_readFile $ERROR_FILE 1)
      ERROR=$(_readFile $ERROR_FILE 1)

      echo "$ERROR"

      ST_CODE=$ST_ERR_SQL_EXECUTE
      ST_NAME="$ST_ERR_SQL_EXECUTE_NAME $ERROR"
      exit $ST_CODE
  fi

  #cat $RESULT_FILE
  # echo "$RESULT_FILE"

  local result=$(cat $RESULT_FILE | tr -d '\n')

  # Return
  echo "$result"
  
}

function _getDBrepo_conf_sql()
{
  
  local SQL_QUERY_GET_RESULT=$(echo $1)

  ${DATABASE_CLIENT_COMMAND}  --user ${DATABASE_ETL_USERNAME} \
                          --password=${DATABASE_ETL_PASSWORD} \
                          >$RESULT_FILE 2>$ERROR_FILE \
                          --unbuffered --skip-column-names \
  <<EOF                           
  $SQL_SELECT_DATABASE
  $SQL_QUERY_GET_RESULT
  \q
EOF

  local RESULT=$?

  if [ $RESULT -ne 0 -o -s $ERROR_FILE ]
  then
      #_logError $ERROR_FILE $LOG_FILE
      #ERROR=$(_readFile $ERROR_FILE 1)
      ERROR=$(_readFile $ERROR_FILE 1)

      echo "$ERROR"

      ST_CODE=$ST_ERR_SQL_EXECUTE
      ST_NAME="$ST_ERR_SQL_EXECUTE_NAME $ERROR"
      exit $ST_CODE
  fi


  # cat $RESULT_FILE
  # echo "$RESULT_FILE"

  # local result=$(cat $RESULT_FILE | tr -d '\n')
  local result=$(cat $RESULT_FILE)

  
  # Return
  # echo -e "$result"
  result=$(echo -e "$result")

  echo "RESULT_REPO_CONFIG_SQL=\"" > $RESULT_FILE
  echo "$result"                  >> $RESULT_FILE
  echo "\""                       >> $RESULT_FILE

  . $RESULT_FILE

  echo "$RESULT_REPO_CONFIG_SQL"

}

# check EOF <> blank line
function file_ends_with_newline() {
    [[ $(tail -c1 "$1" | wc -l) -gt 0 ]]
}
