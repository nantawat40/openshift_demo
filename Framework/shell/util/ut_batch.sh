#!/bin/bash

#--------------------------------------------------------------------
#              Copyright C - 2012 All rights reserved
#                        G-Able Thailand.
#
#       No part of this program may be reproduced or adapted in any
#             form or by any means, electronic or mechanical,
#       without permission from G-Able Thailand Thailand.
#          This program is confidential and may not be disclosed,
#          decompiled or reverse engineered without permission in
#             writing from G-Able Thailand Thailand.
#
#--------------------------------------------------------------------
# DESCRIPTION:
#    create table/view/object on database on area staging
#
#--------------------------------------------------------------------
# Revision   Date        CR No.   Author         Description
#====================================================================
#  0.2       29/02/2020  XXX      Suebsakul.A    Project Push to SEE, Your DATA
#
#
#
#


# Parameters
# -s [Initial | Staging | Warehouse] : stage for unit test PUSH to SEE, build unit test on all Stage

#Force run profile for support Job Schduler
. $HOME/.profile

# Load global variables and function library
GLOBAL_LIB_CONFIG=$HOME/Framework/configs/lib/global.inc
. $GLOBAL_LIB_CONFIG

EXEC_NAME="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")"
EXEC_PARA='[-s < Initial_stg | Initial_dwh | Staging | Warehouse >] [-t <TABLE> -typ <STG|TYPE_0|TYPE_2|FACT> -bsd <YYYYMMDD>] [ -L <FILTER TABLE NAME>] [-T <TEST MODE>]'
BUILD_NUM='on_demand'
STAGE='unit_test'
TEST_MODE='No'
UNIT_TEST_MODE='N/A'
TABLE_CONDITION=""
AREA='unit_test'

TABLE_TYPE=''
TABLE_NAME=''
P_BUSINESS_DATE=''

set -e
set -o pipefail


# Load global variables and function library
. $MAIN_CONFIG

#Check number of argment
if [ $# -ne 2 -a $# -ne 3 -a $# -ne 4 -a $# -ne 6 ]
then
  _usage
else
  let i=1
  for p in $*;
  do
    case "$p" in
      "-s")
        AREA=${@:$((i+1)):1}
        UNIT_TEST_MODE='AREA'
      ;;   
      "-t")
        TABLE_NAME=${@:$((i+1)):1}
        UNIT_TEST_MODE='TABLE'
      ;;
      "-typ")
        TABLE_TYPE=${@:$((i+1)):1}
        UNIT_TEST_MODE='TABLE'
      ;;   
      "-bsd")
        P_BUSINESS_DATE=${@:$((i+1)):1}
      ;;
      "-b")
        BUILD_NUM=${@:$((i+1)):1}
      ;;         
      "-L")
        TABLE_CONDITION=${@:$((i+1)):1}
        UNIT_TEST_MODE='List All Tables'
        AREA='List_All_Tables'

        # list all tables when parameter 'All'
        if [ "$TABLE_CONDITION" == "All" -o "$TABLE_CONDITION" == "all" -o "$TABLE_CONDITION" == "ALL" ]
        then
            TABLE_CONDITION=''
        fi        
      ;;         
      "-T")
        TEST_MODE='Yes'
      ;;      
    esac
    let i=i+1
  done;

  if [ -z $STAGE ]
  then
    _usage
  fi

  if [ "$UNIT_TEST_MODE" == "N/A" ]
  then
    _usage
  fi

  if [ "$UNIT_TEST_MODE" == "TABLE" ]
  then
    if [ -z $TABLE_TYPE ] || [ -z $TABLE_NAME ]
    then
        _usage
    fi
  fi

  if [ -z $BUILD_NUM ]
  then
    BUILD_NUM='on_demand'
  fi
  
fi

# echo "$AREA"
# echo "$TEST_MODE"

# initial variable
LOG_PATH=$LOG_PATH/$STAGE
# INPUT_PATH=$INPUT_PATH/$STAGE
# ARCHIVE_PATH=$ARCHIVE_PATH/$STAGE
TEST_PATH=$TEST_PATH/$STAGE/$UNIT_TEST_MODE/$BUILD_NUM
CONF_COMMON=$CONF_PATH/common

if [ ! -d $LOG_PATH ]
then
  mkdir -p $LOG_PATH
fi

if [ ! -d $ARCHIVE_PATH ]
then
  mkdir -p $ARCHIVE_PATH
fi

if [ ! -d $TEST_PATH ]
then
  mkdir -p $TEST_PATH
fi

# CONF_FILE=$CONF_PATH/$STAGE/$AREA.conf
LOG_FILE=$LOG_PATH/$AREA.log
RESULT_FILE=$LOG_PATH/$AREA.out
ERROR_FILE=$LOG_PATH/$AREA.err
SQL_FILE=$CONF_PATH/sql/$STAGE/unit_test.sql
TEST_FILE=$TEST_PATH/$AREA.result
PROC_FILE=$LOG_PATH/$AREA.proc


#--------------------------------------
# STEP 1. Check running process
#--------------------------------------
_preventMultipleInstance "$EXEC_NAME.*\ $AREA"  $PROC_FILE $LOG_FILE

#Define start time
START_TIME="$(date +%s%N)"

#--------------------------------------
# STEP 2. Loading Configuration
#--------------------------------------
_logger "Started Unit Test Mode=${UNIT_TEST_MODE}" $LOG_FILE

# 1.2 SQL Configure
if [ -s $SQL_FILE ]
then

  #replace default BUSINESS_DATE by P_BUSINESS_DATE
  if [ ! -z $P_BUSINESS_DATE ]
  then
    BUSINESS_DATE=${P_BUSINESS_DATE}
    _logger " Change business_date=${BUSINESS_DATE}" $LOG_FILE    
  else
    BUSINESS_DATE="19990101"
    _logger " Change business_date=${BUSINESS_DATE}" $LOG_FILE    
  fi

  . $SQL_FILE

else
  _logger "Unable to load SQL Configuration file: $SQL_FILE" $LOG_FILE
  _logger "STATUS=$ST_UNEXPECTED_ERROR $ST_UNEXPECTED_ERROR_NAME" $LOG_FILE

  exit $ST_UNEXPECTED_ERROR
fi
if [ "$UNIT_TEST_MODE" == "List All Tables" ]
then
    _logger "Started Show All TABLE on Schema" $LOG_FILE    

    _logger "List all Tables on Database" $LOG_FILE
    ${DATABASE_CLIENT_COMMAND}  --user ${DATABASE_ETL_USERNAME} \
                            --password=${DATABASE_ETL_PASSWORD} \
                            >$RESULT_FILE 2>$ERROR_FILE \
                            --unbuffered -t \
    <<EOF                           
    $SQL_LIST_TABLES
    \q
EOF

    RESULT=$?

    if [ $RESULT -ne 0 -o -s $ERROR_FILE ]
    then
        #_logError $ERROR_FILE $LOG_FILE
        #ERROR=$(_readFile $ERROR_FILE 1)
        ERROR=$(_readFile $ERROR_FILE 1)

        echo "$ERROR"

        ST_CODE=$ST_ERR_SQL_EXECUTE
        ST_NAME="$ST_ERR_SQL_EXECUTE_NAME $ERROR"
        exit $ST_CODE
    fi

    # cat $RESULT_FILE  | sed -e 's/-->\tNULL//g' -e 's/\t/ /g' -e 's/  / /g' -e 's/^\([1-9]\) / \1 /g'
    cat $RESULT_FILE
    
    exit 1
    
    # read result to variable
    # count=$(wc -l $RESULT_FILE | awk '{print $1}')
    # echo "-------------------------------"
    # echo " List: $count table(s)"
    # echo "-------------------------------"        
    # cat $RESULT_FILE
    # echo "-------------------------------"
    # echo " Total: $count table(s)"
    # echo "-------------------------------"

elif [ "$UNIT_TEST_MODE" == "TABLE" ]
then

    TEST_FILE=$TEST_PATH/${AREA}_${UNIT_TEST_MODE}_${TABLE_NAME}.result

    #Clear Test file
    rm -f $TEST_FILE

    _logger "TABLE NAME=${TABLE_NAME} and TYPE=${TABLE_TYPE}" $LOG_FILE

    _logTst "TABLE NAME=${TABLE_NAME} and TYPE=${TABLE_TYPE}" $TEST_FILE

    case "$TABLE_TYPE" in

        "STG" | "TYPE_0")

            _logger "-------------------------------" $LOG_FILE
            _logger "All rules for unit test on Table" $LOG_FILE
            
            _logTst "-------------------------------" $TEST_FILE
            _logTst "All rules for unit test on Table" $TEST_FILE

            SQL_LIST_TEST_RULES=$(_removeCR "$SQL_LIST_TEST_RULES")
            LIST_TEST_RULES=$(_getDBresult "$SQL_LIST_TEST_RULES")

            # _logger "Get Reult from DB:SQL_LIST_FIELDS" $LOG_FILE
            SQL_LIST_FIELDS=$(_removeCR    "$SQL_LIST_FIELDS")
            LIST_ALL_FIELDS=$(_getDBresult "$SQL_LIST_FIELDS")    

            LIST_ALL_FIELDS_CHECK_NULL=$(echo "$LIST_ALL_FIELDS" | sed 's/,/ IS NULL OR /g' | sed 's/$/ IS NULL/')            
            LIST_ALL_FIELDS_CHECK_BLANK=$(echo "LENGTH($LIST_ALL_FIELDS" | sed 's/, /) = 0 OR LENGTH(/g' | sed 's/$/) = 0/')

            # load sql config of dwh (type_0, type_2, fact)
            if [ "$TABLE_TYPE" != "STG" ]; then
                            
                dim_typ="${TABLE_TYPE,,}"
                SQL_FILE_DWH=$CONF_PATH/sql/dwh/dwh_${dim_typ}.sql

                DWH_TABLE_NAME=${TABLE_NAME}

                . $SQL_FILE_DWH

                LIST_PK_FIELDS=$(_getDBresult "$SQL_GET_LIST_PK")

            fi

            # echo "[$LIST_TEST_RULES]"

            # _logTst "-------------------------------" $TEST_FILE

            echo "$LIST_TEST_RULES" | sed 's/[ ]*,[ ]*$//g' | sed 's/,/\n/g' | while read CONNECTION TEST_RULE_ID SHOW_TYPE RULE_NAME
            do
                _logger "-------------------------------" $LOG_FILE
                _logger "  STG $TEST_RULE_ID ($RULE_NAME's $SHOW_TYPE)" $LOG_FILE

                _logTst "-------------------------------" $TEST_FILE
                _logTst "  STG $TEST_RULE_ID ($RULE_NAME's $SHOW_TYPE)" $TEST_FILE

                # echo "[$TEST_RULE_ID] [$RULE_NAME] [$SHOW_TYPE] [$CONNECTION]"

                # check acccess file.config Y or N

                LOAD_SQL_FILE=$CONF_COMMON/$CONNECTION.config

                . $LOAD_SQL_FILE

                . $SQL_FILE
                
                # echo "$SQL_GET_TEST_SQL_STATMENT"
                TEST_SQL_STATEMENT=$(_getDBrepo_conf_sql "$SQL_GET_TEST_SQL_STATMENT")
                
                # echo "After DB      : $TEST_SQL_STATEMENT"
                # exit 1

                _logger "-------------------------------" $LOG_FILE

                _logTst "-------------------------------" $TEST_FILE
                # _logger " STG ${TEST_RULE_ID} : ${RULE_NAME}" $LOG_FILE
                # _logger "-------------------------------" $LOG_FILE                
                
                # For Unit Type's "CHECK"
                if [ "$SHOW_TYPE" == "CHECK" ] 
                then
                    SQL_CHECK_RULE=$TEST_SQL_STATEMENT

                    . $SQL_FILE

                    _logger "Result of SQL : " $LOG_FILE
                    _logTst "Result of SQL : " $TEST_FILE
                    _logTst "--------------" $TEST_FILE
                    

                    _logger "Execute SQL : Start" $LOG_FILE
                    # _logTst "Execute SQL : Start" $TEST_FILE


                    SQL_RULE_CND=$(echo "$SQL_RULE_CND" | sed -e 's/\t\t\t\t//g' -e 's/\t\t\t//g' -e 's/\t\t//g')

                    echo "-------------------------------"
                    echo "$SQL_RULE_CND"
                    echo "-------------------------------"

                    _logTst "$SQL_RULE_CND" $TEST_FILE
                    _logTst "--------------" $TEST_FILE
                    ${DATABASE_CLIENT_COMMAND}  --host=${DATABASE_SERVER} \
                                                --port=${DATABASE_PORT} \
                                                --user ${DATABASE_ETL_USERNAME} \
                                            --password=${DATABASE_ETL_PASSWORD} \
                                            >$RESULT_FILE 2>$ERROR_FILE \
                                            --unbuffered --silent --skip-column-names \
                    <<EOF
                    $SQL_RULE_CND
                    \q
EOF
                    _logger "Execute SQL : Done" $LOG_FILE
                    # _logTst "Execute SQL : Done" $TEST_FILE
                    


                    RESULT_RULE=$(cat $RESULT_FILE)
                
                # For Unit Type's "LIST"
                elif [ "$SHOW_TYPE" == "LIST" ] 
                then
                    SQL_RULE_CND=$TEST_SQL_STATEMENT

                    _logger "Execute SQL : Start" $LOG_FILE
                    # _logTst "Execute SQL : Start" $TEST_FILE

                    ${DATABASE_CLIENT_COMMAND}  --host=${DATABASE_SERVER} \
                                                --port=${DATABASE_PORT} \
                                                --user ${DATABASE_ETL_USERNAME} \
                                            --password=${DATABASE_ETL_PASSWORD} \
                                            >$RESULT_FILE 2>$ERROR_FILE \
                                            --unbuffered -v -t\
                    <<EOF
                    $SQL_RULE_CND
                    \q
EOF
                    _logger "Execute SQL : Done" $LOG_FILE
                    # _logTst "Execute SQL : Done" $TEST_FILE

                    RESULT=$?

                    _logger "Result of SQL : " $LOG_FILE
                    _logTst "Result of SQL : " $TEST_FILE

                    cat $RESULT_FILE

                    _logTst "$RESULT_FILE" $TEST_FILE

                    # Check return code's TRUE/FALSE
                    if [ $RESULT -eq 0 ]
                    then
                        RESULT_RULE="TRUE"
                    else
                        RESULT_RULE="FALSE"
                    fi
                else
                    _logger "Unknlow SHOW_TYPE : ${SHOW_TYPE}" $LOG_FILE
                    exit $ST_UNEXPECTED_ERROR
                fi

                # echo "$SQL_RULE_CND"
                
                _logger "-------------------------------" $LOG_FILE
                _logger "  STG ${TEST_RULE_ID} : RESULT = $RESULT_RULE" $LOG_FILE
                _logger "-------------------------------" $LOG_FILE

                _logTst "-------------------------------" $TEST_FILE
                _logTst "  STG ${TEST_RULE_ID} : RESULT = $RESULT_RULE" $TEST_FILE
                _logTst "-------------------------------" $TEST_FILE
                
                # exit 1
            done
  
        ;;

    esac

elif [ "$UNIT_TEST_MODE" == "AREA" ]
then

    _logger "Started AREA=${AREA}" $LOG_FILE

    _logger "Delete file test: $TEST_FILE" $LOG_FILE
    rm -f $TEST_FILE

    case "$AREA" in

        "Initial_stg")

            INITI_LOG_FILE=$HOME/Framework/output/logs/initial

            # delete all file on log of stg
            _logger "Delete file log on path: $INITI_LOG_FILE/" $LOG_FILE
            rm -f $INITI_LOG_FILE/*

            # assign shell script of initial stg from lnd
            shellInitial_STG=$HOME_ETL/shell/0_initial/initial_stg_from_lan.sh

            if [ -z $LND_PATH ]
            then    
                _logger "Unable to command variable LND_PATH: $LND_PATH" $LOG_FILE
                exit $ST_UNEXPECTED_ERROR
            fi

            if [ ! -f $shellInitial_STG ]
            then    
                _logger "Unable to access file: $shellInitial_STG" $LOG_FILE
                exit $ST_UNEXPECTED_ERROR        
            fi
            
            # Reset
            count_unit_test=0

            # list file *.csv on path landing
            ls $LND_PATH/*.csv | while read FULLPATH_FILE_CSV
            do
            
                FILENAME_CSV=$(_getFilename $FULLPATH_FILE_CSV)

                _logger "Found csv file: : [$FILENAME_CSV]" $LOG_FILE

                # echo $FILENAME_CSV
                # echo "$shellInitial_STG"

                EXEC="$shellInitial_STG -f $FILENAME_CSV.csv -Y"

                $EXEC

                RESULT=$?

                if [ $RESULT -ne 0 ]
                then

                    _logger "Unable execute script : [$EXEC]" $LOG_FILE

                    exit $ST_UNEXPECTED_ERROR
                fi

                CONF_FILE=$CONF_PATH/stg
                DEPLOY_FILE=$DEPLOY_PATH/stg
                INPUT_FILE=$INPUT_PATH/stg

                FILENAME_DEPLOY_STG=$DEPLOY_FILE/STG_${FILENAME_CSV^^}.sql
                FILENAME_CONFIG_STG=$CONF_FILE/STG_${FILENAME_CSV^^}.conf
                FILENAME_INPUT_STG=$INPUT_FILE/${FILENAME_CSV}_19990101.csv
                FILENAME_CONTROL_STG=$INPUT_FILE/${FILENAME_CSV}_19990101.control

                # verify result generate of initial staging
                # deploy stg file
                # config stg file
                # input stg file or control

                # command because found error on option set -e
                # ((count_unit_test++))
                count_unit_test=$(( $count_unit_test + 1 ))
                
                _logger "-------------------------------" $LOG_FILE
                _logger "Verify result of script initial stg" $LOG_FILE
                _logger "-------------------------------" $LOG_FILE

                _logTst "-------------------------------" $TEST_FILE
                _logTst "$count_unit_test. Verify result of script initial stg: $FILENAME_CSV.csv" $TEST_FILE
                _logTst "-------------------------------" $TEST_FILE            

                if [ ! -f $FILENAME_DEPLOY_STG ]
                then    
                    _logger "1. Unable to access deploy file: $FILENAME_DEPLOY_STG" $LOG_FILE
                    exit $ST_UNEXPECTED_ERROR
                else
                    _logger "1. Found deploy file: $FILENAME_DEPLOY_STG" $LOG_FILE
                    _logTst "1. Found deploy file: $FILENAME_DEPLOY_STG" $TEST_FILE
                fi

                if [ ! -f $FILENAME_CONFIG_STG ]
                then    
                    _logger "2. Unable to access config file: $FILENAME_CONFIG_STG" $LOG_FILE
                    exit $ST_UNEXPECTED_ERROR
                else
                    _logger "2. Found config file: $FILENAME_CONFIG_STG" $LOG_FILE
                    _logTst "2. Found config file: $FILENAME_CONFIG_STG" $TEST_FILE           
                fi

                if [ ! -f $FILENAME_INPUT_STG ]
                then    
                    _logger "3. Unable to access input file csv: $FILENAME_INPUT_STG" $LOG_FILE
                    exit $ST_UNEXPECTED_ERROR
                else
                    _logger "3. Found input file csv: $FILENAME_INPUT_STG" $LOG_FILE
                    _logTst "3. Found input file csv: $FILENAME_INPUT_STG" $TEST_FILE           
                fi

                if [ ! -f $FILENAME_CONTROL_STG ]
                then    
                    _logger "4. Unable to access input file csv: $FILENAME_CONTROL_STG" $LOG_FILE
                    exit $ST_UNEXPECTED_ERROR
                else
                    _logger "4. Found input file csv: $FILENAME_CONTROL_STG" $LOG_FILE
                    _logTst "4. Found input file csv: $FILENAME_CONTROL_STG" $TEST_FILE           
                fi            

                _logger "-------------------------------" $LOG_FILE
                _logTst "-------------------------------" $TEST_FILE           
                _logTst "" $TEST_FILE

            done
        ;;

        "Staging")

            # assign shell script of initial stg from lnd
            shellDeploy_STG=$HOME_ETL/shell/1_deploy/deploy_stg.sh
            shellLoad_STG=$HOME_ETL/shell/stg/stg_load.sh

            

            DEPLOY_FILE=$DEPLOY_PATH/stg
            STG_LOG_FILE=$HOME/Framework/output/logs/stg

            # delete all file on log of stg
            _logger "Delete file log on path: $STG_LOG_FILE/" $LOG_FILE
            rm -f $STG_LOG_FILE/*        
            
            count_unit_test=0
            # list file *.csv on path landing
            ls $LND_PATH/*.csv | while read FULLPATH_FILE_CSV
            do
                FILENAME_CSV=$(_getFilename $FULLPATH_FILE_CSV)

                FILENAME_DEPLOY_STG=$DEPLOY_FILE/STG_${FILENAME_CSV^^}.sql

                TABLE_STG=$(_getFilename $FILENAME_DEPLOY_STG)

                _logger "Found stg deploy file: : [$TABLE_STG]" $LOG_FILE

                EXEC="$shellDeploy_STG -f $TABLE_STG.sql"

                $EXEC

                RESULT=$?

                if [ $RESULT -ne 0 ]
                then

                    _logger "Unable execute script : [$EXEC]" $LOG_FILE

                    exit $ST_UNEXPECTED_ERROR
                fi

                EXEC="$shellLoad_STG -t $TABLE_STG -d 19990101"

                $EXEC

                RESULT=$?

                if [ $RESULT -ne 0 ]
                then

                    _logger "Unable execute script : [$EXEC]" $LOG_FILE

                    exit $ST_UNEXPECTED_ERROR
                fi            


                FILENAME_LOG_STG=$STG_LOG_FILE/$TABLE_STG.log

                 # command because found error on option set -e
                #((count_unit_test++))
                count_unit_test=$(( $count_unit_test + 1 ))

                _logger "-------------------------------" $LOG_FILE
                _logger "$count_unit_test. Verify result of script stg on Table: $TABLE_STG" $LOG_FILE
                _logger "-------------------------------" $LOG_FILE

                _logTst "-------------------------------" $TEST_FILE
                _logTst "$count_unit_test. Verify result of script stg on Table: $TABLE_STG" $TEST_FILE
                _logTst "-------------------------------" $TEST_FILE


        
                if [ ! -f $FILENAME_LOG_STG ]
                then    
                    _logger " 1. Unable to access log file: $FILENAME_LOG_STG" $LOG_FILE
                    exit $ST_UNEXPECTED_ERROR
                else
                    _logger " 1. Found log file: $FILENAME_LOG_STG" $LOG_FILE
                    _logTst " 1. Found log file: $FILENAME_LOG_STG" $TEST_FILE
                fi

                # get line Pattern Filename
                MSG_RESULT=$(grep 'Pattern Filename' $FILENAME_LOG_STG | tail -1)

                if [ -z "$MSG_RESULT" ]
                then    
                    _logger " 2. Can't find Pattern Filename: $FILENAME_LOG_STG" $LOG_FILE
                    exit $ST_UNEXPECTED_ERROR
                else
                    _logger " 2. Found log Pattern: $MSG_RESULT" $LOG_FILE
                    _logTst " 2. Found log Pattern: $MSG_RESULT" $TEST_FILE
                fi            

                # get line Loading File
                MSG_RESULT=$(grep 'Loading File' $FILENAME_LOG_STG | tail -1)

                if [ -z "$MSG_RESULT" ]
                then    
                    _logger " 3. Can't find Loading File: $FILENAME_LOG_STG" $LOG_FILE
                    exit $ST_UNEXPECTED_ERROR
                else
                    _logger " 3. Found log Loading: $MSG_RESULT" $LOG_FILE
                    _logTst " 3. Found log Loading: $MSG_RESULT" $TEST_FILE
                fi                        

                # get line verify record number
                MSG_RESULT=$(grep 'Verify Record Number' $FILENAME_LOG_STG | tail -1)

                if [ -z "$MSG_RESULT" ]
                then    
                    _logger " 4. Can't find log Verify on file: $FILENAME_LOG_STG" $LOG_FILE
                    exit $ST_UNEXPECTED_ERROR
                else
                    _logger " 4. Found log Verify : $MSG_RESULT" $LOG_FILE
                    _logTst " 4. Found log Verify : $MSG_RESULT" $TEST_FILE
                fi            

                # get status of job
                MSG_RESULT=$(grep 'STATUS' $FILENAME_LOG_STG | tail -1)   
                if [ -z "$MSG_RESULT" ]
                then    
                    _logger " 5. Can't find log Status on file: $FILENAME_LOG_STG" $LOG_FILE
                    exit $ST_UNEXPECTED_ERROR
                else
                    _logger " 5. Found log Status : $MSG_RESULT" $LOG_FILE
                    _logTst " 5. Found log Status : $MSG_RESULT" $TEST_FILE
                fi                     
            
                _logger "-------------------------------" $LOG_FILE
                _logTst "-------------------------------" $TEST_FILE           
                _logTst "" $TEST_FILE

            done
        ;;

        "Initial_dwh")

            # load repository
            shellLoad_CONF=$HOME_ETL/external/python/run_cnv.sh

            shellInitial_DWH=$HOME_ETL/shell/0_initial/initial_dwh_from_stg.sh

            INITI_LOG_FILE=$HOME/Framework/output/logs/initial
            DEPLOY_LOG_FILE=$HOME/Framework/output/logs/deploy

            # delete all file on log of stg
            # _logger "Delete file log on path: $INITI_LOG_FILE/" $LOG_FILE
            # rm -f $INITI_LOG_FILE/*

            EXEC=$shellLoad_CONF

            $EXEC

            RESULT=$?

            if [ $RESULT -ne 0 ]
            then

                _logger "Unable execute script : [$EXEC]" $LOG_FILE
                exit $ST_UNEXPECTED_ERROR
            else
                _logger "Load repository success : [$EXEC]" $LOG_FILE
            fi


            _logger "Get Reult from DB:TABLES REPO" $LOG_FILE
            ${DATABASE_CLIENT_COMMAND}  --user ${DATABASE_ETL_USERNAME} \
                                    --password=${DATABASE_ETL_PASSWORD} \
                                    >$RESULT_FILE 2>$ERROR_FILE \
                                    --unbuffered --silent --skip-column-names \
            <<EOF                           
            $SQL_SELECT_DATABASE_STG
            $SQL_GET_REPO_STG_TABLES
            \q
EOF

            RESULT=$?

            if [ $RESULT -ne 0 -o -s $ERROR_FILE ]
            then
                #_logError $ERROR_FILE $LOG_FILE
                #ERROR=$(_readFile $ERROR_FILE 1)
                ERROR=$(_readFile $ERROR_FILE 1)

                echo "$ERROR"

                ST_CODE=$ST_ERR_SQL_EXECUTE
                ST_NAME="$ST_ERR_SQL_EXECUTE_NAME $ERROR"
                exit $ST_CODE
            fi

            # cat $RESULT_FILE
            # echo "$SQL_SELECT_DATABASE"
            # echo "$SQL_GET_REPO_TABLES"
            # exit 1
            
            # read result to variable
            cat $RESULT_FILE | while read -r TABLE_STG MODE
            do
                echo "[$TABLE_STG] [$MODE]"

                EXEC="$shellInitial_DWH -t $TABLE_STG -m $MODE -Y"

                $EXEC
                
                RESULT=$?

                if [ $RESULT -ne 0 ]
                then

                    _logger "Unable execute script : [$EXEC]" $LOG_FILE

                    exit $ST_UNEXPECTED_ERROR
                fi


                # get script deploy dwh
                EXEC=$(grep 'Create Table by script' $INITI_LOG_FILE/$TABLE_STG.log | tail -1 | awk -F' : ' '{print $2}')

                DWH_TABLE=$(echo "$EXEC" | awk '{print $3}' | awk -F'.' '{print $1}')

                EXEC=${EXEC/\$HOME_ETL/$HOME_ETL}

                # echo "After : $EXEC"

                $EXEC

                RESULT=$?

                if [ $RESULT -ne 0 ]
                then

                    _logger "Unable execute script : [$EXEC]" $LOG_FILE

                    exit $ST_UNEXPECTED_ERROR
                fi


                _logger "-------------------------------" $LOG_FILE
                _logger " Verify result of script initial dwh on Table: $TABLE_STG" $LOG_FILE
                _logger "-------------------------------" $LOG_FILE

                _logTst "-------------------------------" $TEST_FILE
                _logTst " Verify result of script initial dwh on Table: $TABLE_STG" $TEST_FILE
                _logTst "-------------------------------" $TEST_FILE

                STG_LOG_FILE=$INITI_LOG_FILE/$TABLE_STG.log
                DWH_LOG_FILE=$DEPLOY_LOG_FILE/$DWH_TABLE.log

                if [ ! -f $STG_LOG_FILE ]
                then    
                    _logger " 1. Unable to access log file: $STG_LOG_FILE" $LOG_FILE
                    exit $ST_UNEXPECTED_ERROR
                else
                    _logger " 1. Found log file: $STG_LOG_FILE" $LOG_FILE
                    _logTst " 1. Found log file: $STG_LOG_FILE" $TEST_FILE
                fi

                if [ ! -f $DWH_LOG_FILE ]
                then    
                    _logger " 2. Unable to access log file: $DWH_LOG_FILE" $LOG_FILE
                    exit $ST_UNEXPECTED_ERROR
                else
                    _logger " 2. Found log file: $DWH_LOG_FILE" $LOG_FILE
                    _logTst " 2. Found log file: $DWH_LOG_FILE" $TEST_FILE
                fi            

                _logger "-------------------------------" $LOG_FILE
                _logTst "-------------------------------" $TEST_FILE           
                _logTst "" $TEST_FILE            

            done
            
        ;;

        "Warehouse")

            DWH_LOG_FILE=$HOME/Framework/output/logs/dwh
            # INITI_LOG_FILE=$HOME/Framework/output/logs/initial

            # delete all file on log of stg
            _logger "Delete file log on path: $DWH_LOG_FILE/" $LOG_FILE
            rm -f $DWH_LOG_FILE/*
            
            shellInitial_DWH_TYPE01=$HOME_ETL/shell/dwh/dwh_type01.sh
            shellInitial_DWH_TYPE2=$HOME_ETL/shell/dwh/dwh_type2.sh
            shellInitial_DWH_FACT=$HOME_ETL/shell/dwh/dwh_fact.sh

            _logger "Get Reult from DB:TABLES REPO" $LOG_FILE
            ${DATABASE_CLIENT_COMMAND}  --user ${DATABASE_ETL_USERNAME} \
                                    --password=${DATABASE_ETL_PASSWORD} \
                                    >$RESULT_FILE 2>$ERROR_FILE \
                                    --unbuffered --silent --skip-column-names \
            <<EOF                           
            $SQL_SELECT_DATABASE_STG
            $SQL_GET_REPO_DWH_TABLES
            \q
EOF

            RESULT=$?

            if [ $RESULT -ne 0 -o -s $ERROR_FILE ]
            then
                #_logError $ERROR_FILE $LOG_FILE
                #ERROR=$(_readFile $ERROR_FILE 1)
                ERROR=$(_readFile $ERROR_FILE 1)

                echo "$ERROR"

                ST_CODE=$ST_ERR_SQL_EXECUTE
                ST_NAME="$ST_ERR_SQL_EXECUTE_NAME $ERROR"
                exit $ST_CODE
            fi

            #cat $RESULT_FILE
            # echo "$SQL_SELECT_DATABASE"
            # echo "$SQL_GET_REPO_TABLES"
            
            # read result to variable
            cat $RESULT_FILE | while read -r TABLE_DWH MODE
            do
                echo "[$TABLE_DWH] [$MODE]"


                case "$MODE" in
                "TYPE_0")
                    EXEC="$shellInitial_DWH_TYPE01 -t $TABLE_DWH"
                ;;   
                "TYPE_2")
                    EXEC="$shellInitial_DWH_TYPE2 -t $TABLE_DWH"
                ;;         
                "FACT")
                    EXEC="$shellInitial_DWH_FACT -t $TABLE_DWH"
                ;;      
                esac
            
                $EXEC
                
                RESULT=$?

                if [ $RESULT -ne 0 ]
                then

                    _logger "Unable execute script : [$EXEC]" $LOG_FILE

                    echo "$ST_UNEXPECTED_ERROR"
                    ST_CODE=$ST_UNEXPECTED_ERROR
                    exit $ST_UNEXPECTED_ERROR
                fi

                # echo "INITI_LOG_FILE : $INITI_LOG_FILE"

                # # get script deploy dwh
                # EXEC=$(grep 'Create Table by script' $INITI_LOG_FILE/$TABLE_DWH.log | tail -1 | awk -F' : ' '{print $2}')

                # DWH_TABLE=$(echo "$EXEC" | awk '{print $3}' | awk -F'.' '{print $1}')

                # EXEC=${EXEC/\$HOME_ETL/$HOME_ETL}

                # # echo "After : $EXEC"

                # $EXEC

                # RESULT=$?

                # if [ $RESULT -ne 0 ]
                # then

                #     _logger "Unable execute script : [$EXEC]" $LOG_FILE

                #     exit $ST_UNEXPECTED_ERROR
                # fi


                _logger "-------------------------------" $LOG_FILE
                _logger " Verify result of script load dwh on Table: $TABLE_DWH" $LOG_FILE
                _logger "-------------------------------" $LOG_FILE

                _logTst "-------------------------------" $TEST_FILE
                _logTst " Verify result of script load dwh on Table: $TABLE_DWH" $TEST_FILE
                _logTst "-------------------------------" $TEST_FILE

                # STG_LOG_FILE=$INITI_LOG_FILE/$TABLE_STG.log
                # DWH_LOG_FILE=$DEPLOY_LOG_FILE/$DWH_TABLE.log

                # if [ ! -f $STG_LOG_FILE ]
                # then    
                #     _logger " 1. Unable to access log file: $STG_LOG_FILE" $LOG_FILE
                #     exit $ST_UNEXPECTED_ERROR
                # else
                #     _logger " 1. Found log file: $STG_LOG_FILE" $LOG_FILE
                #     _logTst " 1. Found log file: $STG_LOG_FILE" $TEST_FILE
                # fi

                # if [ ! -f $DWH_LOG_FILE ]
                # then    
                #     _logger " 2. Unable to access log file: $DWH_LOG_FILE" $LOG_FILE
                #     exit $ST_UNEXPECTED_ERROR
                # else
                #     _logger " 2. Found log file: $DWH_LOG_FILE" $LOG_FILE
                #     _logTst " 2. Found log file: $DWH_LOG_FILE" $TEST_FILE
                # fi            

                _logger "-------------------------------" $LOG_FILE
                _logTst "-------------------------------" $TEST_FILE           
                _logTst "" $TEST_FILE            

            done
        ;;

    esac

else
  _logger "Unknow Unit Test Mode: $UNIT_TEST_MODE" $LOG_FILE
  _logger "STATUS=$ST_UNEXPECTED_ERROR $ST_UNEXPECTED_ERROR_NAME" $LOG_FILE
  exit $ST_UNEXPECTED_ERROR

fi


#--------------------------------------
# Summary
#--------------------------------------
ELAPSED=$(_calcElapsedTime $START_TIME)

_logger "Elapsed $ELAPSED sec." $LOG_FILE
_logger "STATUS=$ST_CODE $ST_NAME" $LOG_FILE
_logger "END" $LOG_FILE
echo " " >> $LOG_FILE
