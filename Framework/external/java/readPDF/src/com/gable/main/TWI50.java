package com.gable.main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;


public class TWI50 {
	
	public static void main(String[] args) throws Throwable {	
		
		int setnum_index=0;
		
		List<String> myList = new ArrayList<>();
		
		String[] PDFInputPathData = new String[] {
				
				"D:\\Workspaces\\PDF_Converter\\50Tawi",
				"D:\\Workspaces\\PDF_Converter\\50Tawi",
				"D:\\Workspaces\\PDF_Converter\\50Tawi"
		};
		 
		String[] PDFInputFileNameData = new String[] {				
//				"ADC-50Tawi-2019_700Emps_Active_20200110.pdf",
//				"HL-50Tawi_323Emps_2019_20200114.pdf",
//				"AG-50Tawi_89Emps_2019_20200115.pdf"
				
//				"D:\\Workspace\\Mariadb\\Framework\\input\\landing\\PDF\\ADC-Active_700Emps_20200110\\ADC-50Tawi-2019_700Emps_Active_20200110.pdf",
//				"D:\\Workspace\\Mariadb\\Framework\\input\\landing\\PDF\\AG-Active_89Emps_20200110\\AG-50Tawi_89Emps_2019_20200115.pdf",
//				"D:\\Workspace\\Mariadb\\Framework\\input\\landing\\PDF\\HL-Active_323Emps_20200110\\HL-50Tawi_323Emps_2019_20200114.pdf",
				
				"D:\\Workspace\\Mariadb\\Framework\\input\\landing\\PDF\\Tawi\\HL_Emp65_NotSuccessFactors.pdf",
				"D:\\Workspace\\Mariadb\\Framework\\input\\landing\\PDF\\Tawi\\HL_Emp320.pdf",
				
				"D:\\Workspace\\Mariadb\\Framework\\input\\landing\\PDF\\Tawi\\ADC_Emp4_LL1_NotSuccessFactors.pdf",
				"D:\\Workspace\\Mariadb\\Framework\\input\\landing\\PDF\\Tawi\\ADC_Emp21_LL1.pdf",
				"D:\\Workspace\\Mariadb\\Framework\\input\\landing\\PDF\\Tawi\\ADC_Emp197_NotSuccessFactors.pdf",
				"D:\\Workspace\\Mariadb\\Framework\\input\\landing\\PDF\\Tawi\\ADC_Emp681.pdf",
				
				"D:\\Workspace\\Mariadb\\Framework\\input\\landing\\PDF\\Tawi\\AG_Emp45_NotSuccessFactors.pdf",
				"D:\\Workspace\\Mariadb\\Framework\\input\\landing\\PDF\\Tawi\\AG_Emp93.pdf"
				
		};
		
		String[] PDFTawi = new String[10];
		
//		String[] CompanyData = new String[] {
////				"����ѷ ͹ѹ�� ������ͻ��鹷� ",	
////				"���ԡ�� ", 
//				"�����鹷� "
////		
//		};
		
		String result;
		String Temp;
		String basename = "";
		String extension = "";		
		int tot_process_rec_num=0; 
		
		for (String PDFInputFileName : PDFInputFileNameData) {		
			
//			String PDFInputFile = PDFInputPathData[setnum_index]+"\\"+PDFInputFileName;
			String PDFInputFile = PDFInputFileName;
			
			System.out.println(setnum_index+"=> Processing for file =>"+PDFInputFile);
			
			basename = FilenameUtils.getBaseName(PDFInputFile);
			extension = FilenameUtils.getExtension(PDFInputFile);
			
			int process_rec_num=0;
			File PDFFile = new File(PDFInputFile);			
			if (PDFFile.exists()) {
				PDDocument document = PDDocument.load(PDFFile);
				PDFTextStripper textReader = new PDFTextStripper();
				
				System.out.println("PDFInputFile -> " + document.getNumberOfPages());
				int FilePageNum = document.getNumberOfPages();
	//			FilePath = PDFInputFile;
				
	//			if(FilePageNum >= csvData.getRecordNum()) {
					int datRec = 0;
					
					for (int i=1;i<=FilePageNum;i++) {
						PDDocument tempDocument = new PDDocument();
						datRec++;
						tempDocument.addPage(document.getPage(i - 1));
						
//						String PayDate = PayDateData[setnum_index];
							
							String pageText = textReader.getText(tempDocument);
							
							tempDocument.close();


							
							//Rule Regular
							String[]  reg_company = new String[] {	
									"(บริษัท .* (\\(มหาชน\\)|จำกัด))",
									"(.*)\\r\\n2562",
									"2562\\r\\n(.*)\\r\\n",
									"\\(ตัวอักษร\\)\\r\\n(.*)\\r\\n2563",
									"[0-9]{5}\\r\\n([0123456789 ,]*)\\r\\n",
									"[0-9]{5}\\r\\n[0123456789 ,]*\\r\\n(.*)\\r\\n",
									"[0-9]{5}\\r\\n[0123456789 ,]*\\r\\n[0123456789\\.,]*\\r\\n([0123456789 ,]*)\\r\\\n"
							};
							
//							รหัสไปรษณีย์.*
							
							System.out.println(" Loop rules process on Page : "+i);
							int iLoop = 0;
							for (String list_rule : reg_company) {	
								iLoop++;
						      // Create a Pattern object
						      Pattern r = Pattern.compile(list_rule);							
																					
							//Match Rule Regular
							Matcher m = r.matcher(pageText);
						      if (m.find( )) {
//						         System.out.println("Found value["+iLoop+"]: " + m.group(0) );
						    	 System.out.println("Found value["+iLoop+"]: " + m.group(1) );						    	  
//						         System.out.println("Found value: " + m.group(1) );
//						         System.out.println("Found value: " + m.group(2) );
						    	 
						    	 //Keep data into Array
						    	 
						    	 if (iLoop == 4) {
						    		 // Replace space
						    		 Temp = m.group(1);
						    		 
						    		 PDFTawi[iLoop-1] = Temp.replace(" ", "");
						    	 }else if (iLoop == 5 || iLoop == 7) {
						    		 Temp = m.group(1);
						    		 PDFTawi[iLoop-1] = Temp.replace(" ", "|");
						    	 } else						    	 
						    		 PDFTawi[iLoop-1] = m.group(1);
						      }else {
						         System.out.println("NO MATCH Rule : " + list_rule);
						         System.out.println(pageText);
						         
						         //Default N/A
						         PDFTawi[iLoop-1] = "N/A";
						         
//						         System.exit(1);
						      }
							} // for
							
							
							// Debug
//							if (i == 2)
//							{
//								System.out.println(pageText);
//								System.exit(1);
//							}
							
							// Debug
//							System.exit(1);
							
//							String[] pageTextArrayed = pageText.split("\r\n");
//							int strNum=0;						
//							for (String str : pageTextArrayed) {							
//								System.out.println(strNum+"=>["+str.replace("\\n","").replace("\\r", "").replace("[\\u0E00-\\u0E7F]","")+"]");
//								System.out.println(strNum+"=>["+str+"]");
//								
//
//								
////								if (str.length()>=1 && str.length()<=4) {
////								if(str.contains(CompanyData[setnum_index])) {
//	//								String regex = ".*([0-9]).*";
//	//								System.out.println("str=>"+str+"=>"+str.matches(regex));
////									if (isNumeric(pageTextArrayed[strNum+1])) {
////										String seq_no = pageTextArrayed[strNum+1];
//////										System.out.println(pageText);
////										String emp_name = pageTextArrayed[strNum+3];
////										String soc_mny_txt = pageTextArrayed[strNum+5].replace(",", "");
////										String pvd_mny_txt = pageTextArrayed[strNum+9].replace(",", "");
////										String money_txt = pageTextArrayed[strNum+10];
////										String[] money_txt_chk = money_txt.split(" ");
////										if(!((money_txt_chk.length == 4 && isNumeric(money_txt_chk[0].replace(",", ""))))) {
////											 money_txt = pageTextArrayed[strNum+11];
////											 pvd_mny_txt = pageTextArrayed[strNum+10].replace(",", "");
////										}
////										String[] money_txt_chk2 = money_txt.split(" ");
////										if(!(money_txt_chk2.length == 4 && isNumeric(money_txt_chk2[0].replace(",", "")))) {
////											 money_txt = pageTextArrayed[strNum+9];
////											 pvd_mny_txt = pageTextArrayed[strNum+8].replace(",", "");
////										}
//////										System.out.println("=> seq_no=>"+seq_no);
//////										System.out.println("=> emp_name=>"+emp_name);
//////										System.out.println("=> pvd_mny_txt=>"+pvd_mny_txt);
//////										System.out.println("=> soc_mny_txt=>"+soc_mny_txt);
//////										System.out.println("=> money_txt=>"+money_txt);
////										String[] strset = money_txt.split(" ");
////										String income_mny = "";
////										String tax_mny = "";
////										 income_mny = strset[0].replace(",", "");
////										 tax_mny = strset[2].replace(",", "");
////
////										 
////										System.out.println("=> income_mny=>"+income_mny);
////										System.out.println("=> tax_mny=>"+tax_mny);

											// Debug
//											System.exit(1);
//										 
//										String dat_str = PDFInputFileName+","+CompanyData[setnum_index]+","+seq_no+","+emp_name+","+income_mny+","+tax_mny+","+soc_mny_txt+","+pvd_mny_txt;
//										System.out.println(dat_str);
//										myList.add(dat_str);
//										process_rec_num++;
//										tot_process_rec_num++;
//									}
//								}					
//								strNum++;
//							}
							
//							String dat_str = PDFInputFileName+","+CompanyData[setnum_index]+","+seq_no+","+emp_name+","+income_mny+","+tax_mny+","+soc_mny_txt+","+pvd_mny_txt;
							// Show value on Array
//							System.out.println(Arrays.toString(PDFTawi));
							
							
							PDFTawi[9] = basename;
							
							result = Arrays.stream(PDFTawi).collect(Collectors.joining("|"));
							System.out.println(result);
//							System.exit(1);
							
							myList.add(result);
							tot_process_rec_num++;
							
					}// end for
				document.close();
				
				System.out.println(setnum_index+"=> Process done total record=>"+process_rec_num+", FilePageNum=>"+FilePageNum);
			}
			else {
				System.err.println("File not Found !");
			}
						
			setnum_index++;
		}//end PDFInputFileData
		
		System.out.println("Generating Result...");
		GenerateResult(myList, basename.concat(extension+".csv"));
		System.out.println("Generating Result Done");
		System.out.println("Total File =>"+setnum_index);
		System.out.println("Total Record =>"+tot_process_rec_num);

	}
	
	public static boolean isNumeric(String strNum) {
		
		if (strNum == null)
			return false;
			
		try {
			
//			Double d = Double.parseDouble(strNum);
			
//			String txt="XXX_$(GetDateValue:d)_YYY";
			//
					String re1=".*([0-9]).*";	// Non-greedy match on filler
//				    String re2="(\\$\\(.*\\))";	// Variable Name 1
			//		^\d{1,}(,\d+)?\.\d+$
			//
				    String regDat = re1;
//				    System.out.println(regDat);
				    Pattern p = Pattern.compile(regDat,Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
				    Matcher m = p.matcher(strNum);
				    if (m.find())
				    {
				        String var1=m.group(1);				  
//				        System.out.println(var1);
				        return true;
				  
//				        String var2=m.group(3);
//				        String var3=m.group(4);
//				        System.out.print("("+word1.toString()+")"+"("+var1.toString()+")"+"("+var2.toString()+")"+"("+var3.toString()+")"+"\n");
				    }
				    
			
		}
		catch(Exception ex) {
			return false;
		}
		
		return false;
	}
	
	public static void GenerateResult(List<String> dataList, String FilenameOutput) throws IOException, Exception {		
		
//		String CSVOutputFile = "D:\\Workspaces\\PDF_Converter\\TWI50.txt";
		String CSVOutputFile = "D:\\Workspace\\Mariadb\\Framework\\input\\stg\\" + FilenameOutput;
		
		int datRec = 0;		
		BufferedWriter bw = null;
		FileWriter fw = null;
		
		try {
			fw = new FileWriter(CSVOutputFile);
			bw = new BufferedWriter(fw);	
//			bw.write(outputHeader);
//			bw.newLine();
			for (String dat : dataList) {				
				datRec++;
				bw.write(dat);
				bw.newLine();
			}// end for

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (bw != null)
				bw.close();
			if (fw != null)
				fw.close();
		}		
		
		System.out.println("Write file Output : "+CSVOutputFile);
		
		
	}
	


}
