-- THIS FILE Generate by G-Able Framework
-- COMMENT Generate by G-Able Framework @2020-01-29 12:56:05

-- SELECTED DATABASE
USE EMPLOYEE_TESTING_STG;

-- DROP TABLE IF FOUND
DROP TABLE IF EXISTS `STG_STATUS`;

-- CREATE TABLE STG
CREATE TABLE `STG_STATUS` (
	STATUS_CODE 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',
	STATUS_NAME 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',
	REMARK 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',


    -- Standard Field by G-Able Framework
    STG_SOURCE_DT       VARCHAR(128)   DEFAULT NULL  COMMENT 'Generate this field by G-Able Framework for DateTime on Filename of Source',
    STG_SOURCE_FILE     VARCHAR(128)   DEFAULT NULL  COMMENT 'Generate this field by G-Able Framework for Filename of Source',
    STG_LOAD_DT     DATETIME       DEFAULT NOW() COMMENT 'Generate this field by G-Able Framework for DateTime Stage load'
) 
CHARACTER SET UTF8 
COLLATE UTF8_GENERAL_CI;
