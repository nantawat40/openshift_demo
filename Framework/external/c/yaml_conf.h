
#define	MAXBLK 1024
#define	MAXFIELD 255

#define ST_CODE_SUCCESS 0
#define ST_CODE_FALSE   1

struct gable_arr_field {
    char name[MAXBLK/4];
    char type[MAXBLK/4];
	  char description[MAXBLK/4];
	  int offset;
};

struct gable_conf {
  char filename[MAXBLK/4];
  char md5sum[MAXBLK/4];
  char original_header[MAXBLK*2];
  char line[MAXBLK/4];
  struct gable_arr_field data[MAXFIELD];

  int field_no;
  int record_length;
};

_Bool IsNumeric(char* String);
int Str2Int(char *szbuf,int *out);
// int pattern_regular(char *reg_rule, int maxGroups, char *szInput, char *szre_subMatch);
int pattern_regular(const char *reg_rule, int maxGroups, char *szInput, char *szre_subMatch);
void diplay_record_structure(struct gable_conf *YAML_Conf);
int yaml_decode_file(char *FILE_YAML,struct gable_conf *YAML_Conf);

