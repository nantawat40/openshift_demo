#!/bin/bash

#--------------------------------------------------------------------
#              Copyright C - 2012 All rights reserved
#                        G-Able Thailand.
#
#       No part of this program may be reproduced or adapted in any
#             form or by any means, electronic or mechanical,
#       without permission from G-Able Thailand Thailand.
#          This program is confidential and may not be disclosed,
#          decompiled or reverse engineered without permission in
#             writing from G-Able Thailand Thailand.
#
#--------------------------------------------------------------------
# DESCRIPTION:
#    initial database and any object on platform data analysis (initiai_db)
#
#--------------------------------------------------------------------
# Revision   Date        CR No.   Author         Description
#====================================================================
#  0.2       16/01/2020  XXX      Suebsakul.A    Project Push to SEE, Your DATA
#
#
#
#


# Parameters
# -f [Filename.sql] : new initial database, tables and object for data processing

#Force run profile for support Job Schduler
. $HOME/.profile

# Load global variables and function library
GLOBAL_LIB_CONFIG=$HOME/Framework/configs/lib/global.inc
. $GLOBAL_LIB_CONFIG

EXEC_NAME="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")"
EXEC_PARA='-t <TABLE STG> -m "(TYPE_0|TYPE_2|FACT)"'
INITIAL='initial'
STAGE='dwh'

# Load global variables and function library
. $MAIN_CONFIG

#Check number of argment
if [ $# -ne 4 -a $# -ne 5 ]
then
  _usage
else
  let i=1
  for p in $*;
  do
    case "$p" in
      "-t")
        STG_TABLE=${@:$((i+1)):1}
      ;;
      "-m")
        DWH_TYPE=${@:$((i+1)):1}
      ;;
      "-Y")
        FORCE_CONFIRM="YES"
      ;;      
      *)
        if [ $((i%2)) -eq 1 ]
        then
          _usage
        fi
      ;;
    esac
    let i=i+1
  done;

  if [ -z $FORCE_CONFIRM ]
  then
    FORCE_CONFIRM="NO"
  fi

  if [ -z $STG_TABLE ]
  then
    _usage
  elif [ -z $DWH_TYPE ] || [ "$DWH_TYPE" != "TYPE_0" -a "$DWH_TYPE" != "TYPE_2" -a "$DWH_TYPE" != "FACT" ]
  then 
    _usage
  fi
  
fi

# echo $FILE_DDL

LOG_PATH=$LOG_PATH/$INITIAL

if [ ! -d $LOG_PATH ]
then
  mkdir -p $LOG_PATH
fi

if [ ! -d $CONF_PATH/$STAGE ]
then
  mkdir -p $CONF_PATH/$STAGE
fi

# Transform DWH TABLE
case "$DWH_TYPE" in
    "TYPE_0")
    DWH_TABLE=${STG_TABLE/STG_/}_MASTER
    FILE_SQL="dwh_type_0.sql"
    ;;
    "TYPE_2")
    DWH_TABLE=${STG_TABLE/STG_/}_HIST_MASTER
    FILE_SQL="dwh_type_2.sql"
    ;;
    "FACT")
    DWH_TABLE=${STG_TABLE/STG_/}_FACT
    FILE_SQL="dwh_fact.sql"
    ;;
    *)
    _usage
    ;;
esac

STG_FILE_DDL=$DEPLOY_PATH/stg/$STG_TABLE.sql
DWH_FILE_DDL=$DEPLOY_PATH/$STAGE/$DWH_TABLE.sql
LOG_FILE=$LOG_PATH/$STG_TABLE.log
SQL_FILE=$CONF_PATH/sql/$STAGE/$FILE_SQL
RESULT_FILE=$LOG_PATH/$STG_TABLE.out
ERROR_FILE=$LOG_PATH/$STG_TABLE.err
STG_TABLE_NAME=${STG_TABLE}
DWH_TABLE_NAME=${DWH_TABLE}

# 1.2 SQL Configure
if [ -s $SQL_FILE ]
then
  . $SQL_FILE
else
  _logger "Unable to load SQL Configuration file: $SQL_FILE" $LOG_FILE
  _logger "STATUS=$ST_UNEXPECTED_ERROR $ST_UNEXPECTED_ERROR_NAME" $LOG_FILE
  exit $ST_UNEXPECTED_ERROR
fi

#Clear Screen
clear

#Define start time
START_TIME="$(date +%s%N)"

_logger "Started STG_TABLE=${STG_TABLE} DWH_TYPE=${DWH_TYPE}" $LOG_FILE

if [ ! -f $STG_FILE_DDL ]
then    
  _logger "Unable to access FILE : $STG_FILE_DDL" $LOG_FILE
  exit $ST_UNEXPECTED_ERROR

fi

_logger "Create DWH TABLE=${DWH_TABLE}" $LOG_FILE

# full path tempalte file
FULLPATH_TEMPLATE_DWH_TABLE=$CREATE_PATH/dwh_table.sql
FULLPATH_TEMPLATE_DWH_CONF=$CREATE_PATH/DWH_template.conf

if [ ! -f $FULLPATH_TEMPLATE_DWH_TABLE ]
then    
  _logger "Unable to access file: $FULLPATH_TEMPLATE_DWH_TABLE" $LOG_FILE
  exit $ST_UNEXPECTED_ERROR
fi

if [ ! -f $FULLPATH_TEMPLATE_DWH_CONF ]
then    
  _logger "Unable to access file: $FULLPATH_TEMPLATE_DWH_CONF" $LOG_FILE
  exit $ST_UNEXPECTED_ERROR
fi

#Get Header
#--------------------------------------
# STEP 1. Get Header of File CSV
#--------------------------------------
NOW=$(date '+%F %T')

# clear temp file
rm -f $TMP_PATH/$DWH_TABLE.sql
rm -f $TMP_PATH/$DWH_TABLE.conf

_logger "Copy File $FULLPATH_TEMPLATE_DWH_TABLE to $TMP_PATH/$DWH_TABLE.sql" $LOG_FILE
_logger "Copy File $FULLPATH_TEMPLATE_DWH_CONF  to $TMP_PATH/$DWH_TABLE.conf" $LOG_FILE

cp $FULLPATH_TEMPLATE_DWH_TABLE $TMP_PATH/$DWH_TABLE.sql
cp $FULLPATH_TEMPLATE_DWH_CONF  $TMP_PATH/$DWH_TABLE.conf

#${FILENAME^^} is The ^ operator converts to uppercase, while , converts to lowercase.
sed -e "s/\${DWH_TABLENAME\}/${DWH_TABLE}/" \
    -e "s/\${DATABASE_NAME\}/${DATABASE_NAME}/" \
    -e "s/\${STG_TABLENAME\}/${STG_TABLE}/" \
    -e "s/\${NOW\}/$NOW/g" $TMP_PATH/$DWH_TABLE.sql > $TMP_PATH/DWH_template.sql

mv $TMP_PATH/DWH_template.sql $TMP_PATH/$DWH_TABLE.sql

# Get mapping field from STG_TABLE
${DATABASE_CLIENT_COMMAND}  --user ${DATABASE_ETL_USERNAME} \
                        --password=${DATABASE_ETL_PASSWORD} \
                        >$RESULT_FILE 2>$ERROR_FILE \
                        --unbuffered --silent --skip-column-names \
<<EOF                           
$SQL_SELECT_DATABASE
$SQL_LIST_COLUMNS
$SQL_GET_PK_UK_COLUMNS
\q
EOF

RESULT=$?

if [ $RESULT -ne 0 -o -s $ERROR_FILE ]
then
    #_logError $ERROR_FILE $LOG_FILE
    #ERROR=$(_readFile $ERROR_FILE 1)
    ERROR=$(_readFile $ERROR_FILE 1)

    ST_CODE=$ST_ERR_SQL_EXECUTE
    ST_NAME="$ST_ERR_SQL_EXECUTE_NAME $ERROR"
fi

_logger "Replace list of Fields to file ddl" $LOG_FILE

cat $RESULT_FILE | while read line
do    
    if [ "$line" != "NULL" ]
    then
      
      #Replace / to \/ when default value is DateTime e.g. 1999/01/01 1999\/01\/01
      line=$(echo "$line" | sed 's/\//\\\//g')

      sed -i "s/--<EOF Fields>/\t${line}\\n--<EOF Fields>/" $TMP_PATH/$DWH_TABLE.sql
    fi
done

# Remove marking string
sed -i "s/--<EOF Fields>//g" $TMP_PATH/$DWH_TABLE.sql

_logger "Get Reult from DB" $LOG_FILE
SQL_QUERY_GET_RESULT=$(_removeCR "$SQL_RULE_CHECK_ERR")
DB_RESULT=$(_getDBresult "$SQL_QUERY_GET_RESULT" | sed 's/,/,\n/g')

# Customize for DWH TYPE
case "$DWH_TYPE" in
    "TYPE_2")
      sed -i "s/-- DROP SEQUENCE IF EXISTS/DROP SEQUENCE IF EXISTS/" $TMP_PATH/$DWH_TABLE.sql
      sed -i "s/-- CREATE SEQUENCE ${DWH_TABLE}_/CREATE SEQUENCE ${DWH_TABLE}_/" $TMP_PATH/$DWH_TABLE.sql
      
    ;;
esac

# echo "$SQL_RULE_CHECK_ERR"
# echo "$DB_RESULT"

_logger "Replace list of Fields to file ddl" $LOG_FILE
# cat $RESULT_FILE | while read line
echo "$DB_RESULT" | while read line
do
    sed -i "s/--<Flag Condition>/\t\t${line}\\n--<Flag Condition>/" $TMP_PATH/$DWH_TABLE.sql    
done


# Remove marking string (Move to below by Suebsakul.a)
# sed -i "s/--<Flag Condition>//g" $TMP_PATH/$DWH_TABLE.sql

_logger "Get Reult from DB" $LOG_FILE
SQL_QUERY_GET_RESULT=$(_removeCR "$SQL_GET_LIST_PK")
DB_RESULT=$(_getDBresult "$SQL_QUERY_GET_RESULT")

ALIAS_DUP="PK_DUP"
LIST_FiED_DUP=$(echo $DB_RESULT)

. $SQL_FILE

_logger "Get Reult from DB" $LOG_FILE
SQL_QUERY_GET_RESULT=$(_removeCR "$SQL_ON_JOIN_FiED_DUP_PK")
DB_RESULT=$(_getDBresult "$SQL_QUERY_GET_RESULT")

LIST_ONJOIN_FiED_DUP=$(echo $DB_RESULT)

. $SQL_FILE

_logger "Replace list of Fields to file ddl" $LOG_FILE
echo "$SQL_DUP_FIELD" | while read line
do
    sed -i "s/--<List Table Join>/\t\t${line}\\n--<List Table Join>/" $TMP_PATH/$DWH_TABLE.sql
done

_logger "Get Reult from DB" $LOG_FILE
SQL_QUERY_GET_RESULT=$(_removeCR "$SQL_GET_LIST_UK")
DB_RESULT=$(_getDBresult "$SQL_QUERY_GET_RESULT")


ALIAS_DUP="UK_DUP"
LIST_FiED_DUP=$(echo $DB_RESULT)

. $SQL_FILE

_logger "Get Reult from DB" $LOG_FILE
SQL_QUERY_GET_RESULT=$(_removeCR "$SQL_ON_JOIN_FiED_DUP_UK")
DB_RESULT=$(_getDBresult "$SQL_QUERY_GET_RESULT")

LIST_ONJOIN_FiED_DUP=$(echo $DB_RESULT)

# Skip build JOIN FILE DUP becase not have Field UK
# if [ "$LIST_ONJOIN_FiED_DUP" != "NULL" ]
if [ "$LIST_FiED_DUP" != "NULL" ]
then

  #echo "$LIST_FiED_DUP"
  #echo "$LIST_ONJOIN_FiED_DUP"
  #exit 1

  . $SQL_FILE

  _logger "Replace list of Fields to file ddl" $LOG_FILE
  echo "$SQL_DUP_FIELD" | while read line
  do
      sed -i "s/--<List Table Join>/\t\t${line}\\n--<List Table Join>/" $TMP_PATH/$DWH_TABLE.sql
  done

fi

_logger "Get Reult from DB" $LOG_FILE
SQL_QUERY_GET_RESULT=$(_removeCR "$SQL_REF_FIELD")
DB_REF_FIELD=$(_getDBresult "$SQL_QUERY_GET_RESULT")

echo "$DB_REF_FIELD" | while read line
do
    sed -i "s/--<List Table Join>/\t\t${line}\\n--<List Table Join>/" $TMP_PATH/$DWH_TABLE.sql
done

_logger "Get Reult from DB" $LOG_FILE
SQL_QUERY_GET_RESULT=$(_removeCR "$SQL_GET_JOIN_COLUMNS")
DB_REF_FIELD=$(_getDBresult "$SQL_QUERY_GET_RESULT" | sed 's/,/,\n/g')
echo "$DB_REF_FIELD" | while read line
do
    # line <> NULL
    if [ "$line" != "NULL" ]
    then
      sed -i "s/--<Field Join>/\t\t${line}\\n--<Field Join>/" $TMP_PATH/$DWH_TABLE.sql
    fi
done

_logger "Remove marking to file ddl" $LOG_FILE
sed -i "s/--<Field Join>//g" $TMP_PATH/$DWH_TABLE.sql
sed -i "s/--<Flag Condition>//g" $TMP_PATH/$DWH_TABLE.sql
sed -i "s/--<List Table Join>//g" $TMP_PATH/$DWH_TABLE.sql

#cat $TMP_PATH/$DWH_TABLE.sql
#cat $TMP_PATH/$DWH_TABLE.conf

_logger "Generate File Config DWH : $TMP_PATH/$DWH_TABLE.conf" $LOG_FILE

# Generate config for stg load
CONF_FILE=$CONF_PATH/dwh/$DWH_TABLE.conf

if [ -f $CONF_FILE ]
then
  cp $CONF_FILE $TMP_PATH/$DWH_TABLE.conf
  . $CONF_FILE

fi

_logger "-------------------------------" $LOG_FILE
_logger "Input of STG Load Config" $LOG_FILE
_logger "-------------------------------" $LOG_FILE
# get variable from template file

# replace value on file conf
sed -i "s/\${STG_TABLE_NAME\}/$STG_TABLE/" $TMP_PATH/$DWH_TABLE.conf
sed -i "s/\${DWH_TABLE_NAME\}/$DWH_TABLE/" $TMP_PATH/$DWH_TABLE.conf
sed -i "s/\${DIM_TYPE\}/$DWH_TYPE/" $TMP_PATH/$DWH_TABLE.conf


cat  $TMP_PATH/$DWH_TABLE.conf | grep "=" | grep -v ^'#' | grep -v 'DIM_TYPE\|SRC_FIELDS\|SOUR_TABLE\|DEST_TABLE' | awk -F'=' '{print $1}' | while read variable
do

  DEFAULT=$(eval echo \${$variable})

  if [ "$FORCE_CONFIRM" != "YES" ]
  then
    echo "$variable : [$DEFAULT] " | tr -d '\r\n'
    #read inputKeyboard
    read KEYINPUT < /dev/tty
  fi
  
  if [ -z $KEYINPUT ]
  then
    NEW_VALUE=${DEFAULT}
  else
    NEW_VALUE=${KEYINPUT}
  fi

  # replace value on file conf
  sed -i "s/\${$variable\}/$NEW_VALUE/" $TMP_PATH/$DWH_TABLE.conf

done

_logger "-------------------------------" $LOG_FILE
# display contect in fiel temp
cat $TMP_PATH/$DWH_TABLE.conf
_logger "-------------------------------" $LOG_FILE

KEYCONFIRM="N/A"
until [ "$KEYCONFIRM" = "Y" -o  "$KEYCONFIRM" = "y" -o "$KEYCONFIRM" = "N" -o  "$KEYCONFIRM" = "n" -o "$FORCE_CONFIRM" == "YES" ]
do
  echo "-------------------------------"
  read -p 'Confrim Generate Deploy/Config file? (Y/N) ' KEYCONFIRM < /dev/tty
  echo "-------------------------------"
done

# When use key 'Y' or 'y'
if [ "$KEYCONFIRM" = "Y" -o "$KEYCONFIRM" = "y" -o "$FORCE_CONFIRM" == "YES" ]
then  
  _logger "Create File DDL : $DWH_FILE_DDL" $LOG_FILE
  _logger "Create File CONFIG : $CONF_FILE" $LOG_FILE
    
  # move temp to deploy and config script stg
  mv $TMP_PATH/$DWH_TABLE.sql  $DWH_FILE_DDL
  mv $TMP_PATH/$DWH_TABLE.conf $CONF_FILE

  _logger "Create Table by script : \$HOME_ETL/shell/1_deploy/deploy_dwh.sh -f $DWH_TABLE.sql" $LOG_FILE

else
  _logger "Delete Temp File DDL : $TMP_PATH/$FILENAME.sql" $LOG_FILE
  _logger "Delete Temp File CONFIG STG Load : $TMP_PATH/$FILENAME.conf" $LOG_FILE

  # delete temp files
  rm $TMP_PATH/$DWH_TABLE.sql
  rm $TMP_PATH/$DWH_TABLE.conf
fi

#--------------------------------------
# Summary
#--------------------------------------
ELAPSED=$(_calcElapsedTime $START_TIME)

_logger "Elapsed $ELAPSED sec." $LOG_FILE
_logger "STATUS=$ST_CODE $ST_NAME" $LOG_FILE
_logger "END" $LOG_FILE
echo " " >> $LOG_FILE

#--------------------------------------
# Verify result command for support Job Schduler
#--------------------------------------
#for support Job Schduler
if [ $ST_CODE -eq $ST_RECONCILE_REC_ERROR -o $ST_CODE -eq $ST_RECONCILE_SUM_ERROR ]
then
  exit $ST_COMPLETE
else
  exit $ST_CODE
fi

