/*
#---------------------------------------------------------------------------
#
#              Copyright C - 2010 All rights reserved
#                        G-Able Thailand.
#
#       No part of this program may be reproduced or adapted in any
#             form or by any means, electronic or mechanical,
#       without permission from G-Able Thailand.
#          This program is confidential and may not be disclosed,
#          decompiled or reverse engineered without permission in
#             writing from G-Able Thailand.
#
#--------------------------------------------------------------------
# DESCRIPTION:
#    transform source file input to framework
#
#--------------------------------------------------------------------
# Revision   Date        CR No.   Author         Description
#====================================================================
#  0.2       08/03/2020  XXX      Suebsakul.A    Project Push to SEE, Your DATA
#
#
#
#
*/

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <yaml.h>

#include <ctype.h>

#if __linux__ 	
	#include <sys/time.h>
	#include <sys/types.h>
	#include <sys/uio.h>
#endif

#include "getopt.h"
#include "yaml_conf.h"

#define	MAXBLK 1024
#define	MAXFIELD 255
#define iY2K(year)      (pTm->tm_year<2000 ?1900+pTm->tm_year :pTm->tm_year)
#define TRACE			0	//This use for open trance log
#define VERIFY_OUTPUT	0	//This use verify output automatic but effect performance of script

#define ST_CODE_SUCCESS         0
#define ST_CODE_FALSE           1


#define ST_CODE_ERR_PARAMETER  	11
#define ST_CODE_ERR_ACCESS_FILE	15
#define ST_CODE_ERR_CONVERT_OUTPUT	19


#define PATH_LND  "/root/Framework/input/lnd"
#define PATH_TMP  "/root/Framework/output/tmp/lnd"
#define PATH_OUT  "/root/Framework/input/stg"



struct gable_dynamic_field {
    char value[MAXBLK/4];
};

struct gable_record_structure {
    struct gable_dynamic_field **field;
};

// struct gable_arr_field {
//     char name[256];
//     char type[256];
// 	char description[256];

// 	int offset;
// };

// struct gable_conf {
//     char filename[256];
//     char md5sum[256];
// 	char original_header[MAXBLK];	
//     char line_no[256];
// 	int record_length;
// 	int field_no;
//     struct gable_arr_field data[MAXFIELD];
// };

//This vartiable for verify statu and prove output for converted hex double to double
int  show_verbose=1;

int print_usage(int argc,char *argv[])
{
	fprintf(stderr,"Invalid parameters: %s -f <FILE> -c <FILE.control> [ -v -s ]\n",argv[0]);
	fprintf(stderr,"\t -f FILE DATA\t\t: Input file\n");
	fprintf(stderr,"\t -c FILE CONTROL\t: input file.control\n");
	fprintf(stderr,"\t -v Verbose for show stat to standard error\n");
	fprintf(stderr,"\t -s Show Summery but only field's type double\n");
	fprintf(stderr,"e.g. %s -f Employee.csv -c Employee.control\n",argv[0]);
	exit(ST_CODE_ERR_PARAMETER);
}

/***********************************************************************
* Function : GetTokenStr(char  *strIn,char *cDelim,int iPos,char  *strOut)
* Argument : strIn  = String Input
* 	   : cDelim = Delimiter
* 	   : iPos   = Position
* 	   : strOut = String Output
* Description : Use for Get Token String 
* Updated : 15/01/2003
************************************************************************/
int  GetTokenStr(char  *strIn,char *cDelim,int iPos,char  *strOut)
{
	char szBuff[2048];
	char szBuffTok[2048];
	char *szTok=szBuffTok;
	int  i;

	memset(szBuffTok,0,256);
	strcpy(szBuff,strIn);

	if(iPos==1)
	{
		szTok=strtok(szBuff,cDelim);
	}
	else
	{
		szTok=strtok(szBuff,cDelim);

		for(i=1;i<iPos;i++)
		{
			szTok=strtok(NULL,cDelim);
		}
	}
	
	if (szTok != NULL)
		strcpy(strOut,szTok);
	else
		strOut[0] = '\0';


	return(ST_CODE_SUCCESS);

}

int iWriteLogProgram(char *szLogPath,char *szProgram,char *szMesg)
{
//	FILE *fp;
	struct tm *pTm;
	struct timeval tv;
	struct timezone tz;
//	time_t tt;
//	int ret;
	char  szBuffMesg[1024];
	//char  szLogFile[128];
	//char  szFileDate[128];
	char  szMesgDate[128];

	gettimeofday(&tv, &tz);

	//time(&tt);
	gettimeofday(&tv, &tz);
	pTm=localtime(&tv.tv_sec);

	//sprintf( szFileDate, "%04u%02u%02u",iY2K(pTm->tm_year),pTm->tm_mon+1,pTm->tm_mday);

	//sprintf(szLogFile,"%s/%s.%s",szLogPath,szProgram,szFileDate);

	gettimeofday(&tv, &tz);
	pTm=localtime(&tv.tv_sec);
	sprintf( szMesgDate, "%02u/%02u/%04u %02u:%02u:%02u.%06ld",
									pTm->tm_mday,pTm->tm_mon+1,iY2K(pTm->tm_year), 
									pTm->tm_hour,pTm->tm_min,pTm->tm_sec,tv.tv_usec);

	//memset(szBuffMesg,0x0,sizeof(szBuffMesg));
	sprintf(szBuffMesg,"%s,%s\n",szMesgDate,szMesg);
	
	//printf("FOPEN_MSX [%d]\n",FOPEN_MAX);

	// printf("Log : %s",szBuffMesg);
	fprintf(stdout,"Log : %s",szBuffMesg);

//	if ((fp=fopen(szLogFile,"ab+")) ==NULL)
//		return -1;
//	ret= fwrite(szBuffMesg,1,strlen(szBuffMesg),fp);
//	fclose(fp);
//
//	if (ret!=strlen(szBuffMesg))
//	{
//		printf("Error write log file please check disk space\n");
//		exit(0);
//	}

	//return ( ret>0 ?0 :-1 );
	return (ST_CODE_SUCCESS);
}

/***********************************************************************
* Function : char* read_file (const char* filename, size_t* length)
* Argument : char  = String filename
* 	       : char* = data of file in memory
* Description : Read all content data to memory
* Updated : 22/06/2012
************************************************************************/
//char* read_file (const char* filename, size_t* length)
//int read_file (const char* filename, size_t* length)
int read_file (const char* filename, off_t* length)
{
	int fd;
	struct stat file_info;
	char* buffer;

	/* Open the file. */
	fd = open (filename, O_RDONLY);

	/* Get information about the file. */
	fstatat (fd, filename, &file_info,0);
	*length = (off_t)file_info.st_size;

	//off64_t file_size = (off64_t)file_info.st_size; 
	//off64_t temp_buf_size = (off64_t) BUF_SIZE; //software 


	

	/* Make sure the file is an ordinary file. */
	if (!S_ISREG (file_info.st_mode))
	{
		/* It’s not, so give up. */
		close (fd);
		//return NULL;
		return 1;
	}


	/* Allocate a buffer large enough to hold the file’s contents. */
	//buffer = (char*) malloc (*length);
	//if (buffer==NULL)
	//{
	//	fprintf(stderr,"Error your system can't reseved memory [%d] byte\n",(*length));
	//	exit (1);
	//}



	/* Read the file into the buffer. */
	//read (fd, buffer, *length);

	/* Finish up. */
	close (fd);

	//return buffer;
	return (ST_CODE_SUCCESS);
}

int main(int argc, char *argv[])
{
	//Declare variable
	int c; 
	int summary=0;

	// double sum_bignumber[256];

	FILE *fp;
	FILE *fp_out;

	struct gable_conf Control_yaml;
	struct gable_dynamic_field *P_field;

	char szData_File[256];
	char szRecordSchema[256];

	char szFullPathInpFile[MAXBLK/2];
	char szFullPathCntFile[MAXBLK/2];

	char szFullPathTmpFile[MAXBLK/2];
	char szFullPathOutFile[MAXBLK/2];

	char szBuff[MAXBLK*2];

	char szRecord[MAXBLK*2];
	char szHeader[MAXBLK*2];
	
	char szLogMesg[MAXBLK/4];

	char szRegRule[MAXBLK/4];

	off_t file_size = 0;
	int record_size = 30; 				//hard code and wait Test
	int record_file_size = 30;	//hard code and wait Test
	int all_record_calcurate;	

	int iRet;
	int i;

	

    // Check invalid argment
    if (argc <= 3 || argc > 6)
	{
		print_usage(argc,argv);
	}


    while((c =  getopt(argc, argv, "f:c:vs")) != EOF) 
    { 
        switch (c) 
        { 
			case 'f': 
				//cout << optarg << endl; 

				sprintf(szFullPathInpFile,"%s/%s",PATH_LND,optarg);
				sprintf(szFullPathTmpFile,"%s/%s.tmp",PATH_TMP,optarg);
				sprintf(szFullPathOutFile,"%s/%s.cnv",PATH_OUT,optarg);
				

				if(access(szFullPathInpFile,R_OK) !=0 ) //Can't read file file
				{
					fprintf(stderr,"Can't read/access data file!!!\n");
					fprintf(stderr,"FILE_DATA : [%s]\n",szFullPathInpFile);
					exit(ST_CODE_ERR_PARAMETER);
				}

				// strcpy(szData_File,optarg);
				break; 
			case 'c': 
                 //cout << optarg << endl; 		

				sprintf(szFullPathCntFile,"%s/%s",PATH_LND,optarg);
				if(access(szFullPathCntFile,R_OK) !=0 ) //Can't read file file
				{
					fprintf(stderr,"Can't read/access control file!!!\n");
					fprintf(stderr,"FILE_CONTROL : [%s]\n",szFullPathCntFile);
					exit(ST_CODE_ERR_PARAMETER);
				}

				// strcpy(szRecordSchema,optarg);
				break; 
			case 'v':
				show_verbose=1;
				break;
			//case 's':
			//	summary=1;
			//	memset(sum_bignumber,0x0,sizeof(sum_bignumber));
			//	break;
			// case ':':
			// case '?': 
            //      //cerr << "Missing option." << endl; 
			// 	if (optopt == 'f')
			// 		fprintf(stderr,"-%c filename (input file)\n",optopt);
		    //     else if (isprint (optopt))
			// 		fprintf (stderr, "Unknown option `-%c'.\n", optopt);
			// 	else
			// 		fprintf(stderr,"Missing option. [-%c]\n",optopt);

			// 	exit(ST_CODE_ERR_PARAMETER); 

			// 	break; 
				
        } 
    }

	// initial variable from control file
	strcpy(szRegRule,"\\(([0-9]*)\\)");

	memset(&Control_yaml,0x0,sizeof(struct gable_conf));
	yaml_decode_file(szFullPathCntFile,&Control_yaml);

	memset(szHeader,0x0,sizeof(szHeader));
	for(int j=0,pre_offset=0;j<Control_yaml.field_no;j++)
	{
		//Sum all lenght of record
		Control_yaml.record_length += Control_yaml.data[j].offset;

		strcat(szHeader,Control_yaml.data[j].name);
		strcat(szHeader,"|");
	}
	szHeader[strlen(szHeader)-1]='\0';
	strcat(szHeader,"\n");

	#ifdef DEBUG
	diplay_record_structure(&Control_yaml);
	#endif

	//Create record structure
	P_field = (struct gable_dynamic_field *)malloc(Control_yaml.field_no * sizeof(struct gable_dynamic_field));
	

	// strcpy((P_field + 0)->value,"AAAAAA");
	// printf("Test [%s]\n",(P_field + 0)->value);

	#ifdef DEBUG
	printf("--------------------------------------------------------------------\n");
	printf("OK\n");
	printf("--------------------------------------------------------------------\n");
	#else
	sprintf(szLogMesg,"Start");
	iWriteLogProgram(NULL,"Main",szLogMesg);	
	#endif

	read_file (szFullPathInpFile,&file_size);
	if (show_verbose)
	{
		all_record_calcurate=(file_size/Control_yaml.record_length);
				
		sprintf(szLogMesg,"read file [%s]'s size [%ld],size of record [%d] which should be [%d] records",szFullPathInpFile
		                                                                                                ,file_size
																								    	,Control_yaml.record_length
																									    ,all_record_calcurate);
		iWriteLogProgram(NULL,"debug vertica",szLogMesg);
	}

	if ((fp = fopen(szFullPathInpFile, "rb")) == NULL) //Open file by pointer
	{
		fprintf(stderr,"Can't open data file!!!\n");
		fprintf(stderr,"DATA_FILE : [%s]\n",szData_File);
		fclose(fp);
		exit(ST_CODE_ERR_ACCESS_FILE);
	}

	if ((fp_out = fopen(szFullPathTmpFile, "w")) == NULL) //Open file by pointer
	{
		fprintf(stderr,"Can't open data file!!!\n");
		fprintf(stderr,"DATA_FILE : [%s]\n",szFullPathTmpFile);
		fclose(fp);
		exit(ST_CODE_ERR_ACCESS_FILE);
	}	

	i = 1;
	do {

		memset(szBuff,0x0,sizeof(szBuff));

		//read data on file input
		iRet=fread(szBuff,Control_yaml.record_length,1,fp);

		//check return code of read file
		#ifdef DEBUG
		if (iRet == 0 && i > 3)
		#else
		if (iRet == 0)
		#endif
		{
			sprintf(szLogMesg,"Error rec_no[%d] result fread %d <> %d record size"	,i,iRet
															,Control_yaml.record_length);
			iWriteLogProgram(NULL,"main",szLogMesg);			

			record_size=strlen(szBuff);

			if (record_size > 0)
			{	
				sprintf(szLogMesg,"Error found lasted data less than [%s](%d) lenght < (%d)",szBuff
				                                                                            ,record_size
				                                                                            ,Control_yaml.record_length);
				iWriteLogProgram(NULL,"main",szLogMesg);
				//close pointer file
				fclose(fp);
				fclose(fp_out);
				exit(ST_CODE_ERR_CONVERT_OUTPUT);
			}
			//  else {
			// 	printf("szBuff [%s]\n",szBuff);
			// }

			//break loop while (1);
			break;
		}

		//keep value from file		
		#ifdef DEBUG
		printf("szRec[% 5d] : [%s]\n",i,szBuff);

		#elif DTEST
		if ((i%15000) == 0)
		{
			sprintf(szLogMesg,"szRec[% 5d]",i);
			printf("  szRecord [% 2d] : [%s]\n",i,szRecord);
			iWriteLogProgram(NULL,"process record no",szLogMesg);
		}
		#endif
		
		memset(szRecord,0x0,sizeof(szRecord));


		for(int j=0,pre_offset=0;j<Control_yaml.field_no;j++)
		{
			memset((P_field+j),0x0,sizeof(struct gable_dynamic_field));
			pre_offset = (j == 0 ? 0 : Control_yaml.data[j-1].offset);

			strncpy((P_field+j)->value,szBuff+pre_offset,Control_yaml.data[j].offset);
			strcat(szRecord,(P_field + j)->value);
			strcat(szRecord,"|");

			#ifdef DEBUG
			printf("  Field [% 2d] : [%s]\n",j+1,(P_field + j)->value);
			#endif
		}
		szRecord[strlen(szRecord)-1]='\0';
		strcat(szRecord,"\n");

		//Write header record file output only first header
		if (i == 1)
		{
			fwrite(szHeader ,1 ,strlen(szHeader) ,fp_out);
		}

		//Write detail record file output
		fwrite(szRecord ,1 ,strlen(szRecord) ,fp_out);

		//Count next record
		i++;

	} while (1);//while (!feof(fp));

	//close pointer file
	fclose(fp);
	fclose(fp_out);

	//Rename to Output & Statistic
	sprintf(szLogMesg,"Rename tmp file %s -> %s, Write %d records(s)"
	                                                 ,szFullPathTmpFile
													 ,szFullPathOutFile
													 ,i);
	iWriteLogProgram(NULL,"Rename Output file",szLogMesg);	
	rename(szFullPathTmpFile,szFullPathOutFile);

	sprintf(szLogMesg,"Successfully.");
	iWriteLogProgram(NULL,"Main",szLogMesg);	

    return(ST_CODE_SUCCESS);    
}
