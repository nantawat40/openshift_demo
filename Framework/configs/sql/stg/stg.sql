SQL_SELECT_DATABASE="USE ${DATABASE_NAME}_STG;"
SQL_COUNT_ROW="SELECT 'TOTAL_RECORD=',COUNT(*) FROM ${DATABASE_NAME}_STG.$STG_TABLE_NAME;"
SQL_COUNT_COLUMNS="SELECT 'TOTAL_COLUMNS=',COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '${DATABASE_NAME}_STG' AND TABLE_NAME = '${STG_TABLE_NAME}';"
SQL_LIST_COLUMNS="SELECT ORDINAL_POSITION, COLUMN_NAME, COLUMN_TYPE, COLUMN_DEFAULT FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '${DATABASE_NAME}_STG' AND TABLE_NAME = '${STG_TABLE_NAME}'  ORDER BY ORDINAL_POSITION;"
SQL_TRUNCATE="TRUNCATE TABLE ${DATABASE_NAME}_STG.$STG_TABLE_NAME;"
SQL_COPY_DIRECT="
                  LOAD DATA LOCAL INFILE '${FULL_INPUT_FILE}' 
                            INTO TABLE ${DATABASE_NAME}_STG.$STG_TABLE_NAME 
                            CHARACTER SET UTF8
                            FIELDS TERMINATED BY '${INPUT_FIELD_SEPARATOR}'
                            OPTIONALLY ENCLOSED BY '\"'
                            ESCAPED BY ''
                            LINES TERMINATED BY '\n' 
                            IGNORE 1 LINES 
                            SET 
                               STG_SOURCE_DT = '$FILE_DATE',
                               STG_SOURCE_FILE = '${STG_SOURCE_FILE}',
                               STG_LOAD_DT = CURRENT_TIMESTAMP;"
SQL_COUNT_ROW_STG_SOURCE_FILE="SELECT 'TOTAL_RECORD=',COUNT(*) FROM ${DATABASE_NAME}_STG.$STG_TABLE_NAME WHERE STG_SOURCE_FILE = '${STG_SOURCE_FILE}';"

SQL_VERIFY_COUNT_ROW_STG="SELECT STG_SOURCE_FILE, COUNT(*) FROM ${DATABASE_NAME}_STG.$STG_TABLE_NAME GROUP BY STG_SOURCE_FILE;"
SQL_VERIFY_VALUE_NULL="SELECT * FROM ${DATABASE_NAME}_STG.$STG_TABLE_NAME WHERE $CND_WHERE;"
SQL_VERIFY_DUP_ROW_STG="SELECT COUNT(*) AS RECNO_DUP, $STG_ALL_FIELDS FROM ${DATABASE_NAME}_STG.$STG_TABLE_NAME A GROUP BY $STG_ALL_FIELDS HAVING COUNT(*) > 1;"
SQL_GET_ALL_FIELDS="SELECT GROUP_CONCAT(DISTINCT COLUMN_NAME SEPARATOR ',') AS ALL_FIELDS 
                    FROM INFORMATION_SCHEMA.COLUMNS 
                    WHERE 
                          TABLE_SCHEMA = '${DATABASE_NAME}_STG' AND 
                          TABLE_NAME = '$STG_TABLE_NAME' AND 
                          COLUMN_NAME NOT LIKE 'STG_%';"

# SQL_SET_SEARCH_PATH="SET SEARCH_PATH TO $STG_SCHEMA"
# SQL_SELECT="SELECT :1 FROM $STG_SCHEMA.$STG_TABLE_NAME;"
# SQL_TRUNCATE="TRUNCATE TABLE $STG_SCHEMA.$STG_TABLE_NAME;"
# SQL_DELETE="DELETE FROM $STG_SCHEMA.$STG_TABLE_NAME; COMMIT;"
# SQL_CHK_SUM="SELECT CASE SUM(:1) WHEN :2 THEN 'TRUE' ELSE 'FALSE' END, SUM(:1) FROM $STG_SCHEMA.$STG_TABLE_NAME;"
# SQL_CAT_COPY="COPY $STG_SCHEMA.$STG_TABLE_NAME ($STG_FIELDS) FROM STDIN DELIMITER '$INPUT_FIELD_SEPARATOR' NULL '' ;"
# SQL_COPY_STDIN="COPY $STG_SCHEMA.$STG_TABLE_NAME ($STG_FIELDS) FROM STDIN DELIMITER '$INPUT_FIELD_SEPARATOR' NULL '' DIRECT ;"
# SQL_COPY_STDIN="COPY $STG_SCHEMA.$STG_TABLE_NAME ($STG_FIELDS) FROM STDIN DELIMITER '$INPUT_FIELD_SEPARATOR' NULL '' REJECTED DATA '$REJECTED_FILE' DIRECT ;"
# For type binary
# With no reject and exception files
# SQL_COPY_STDIN_DIRECT="COPY $STG_SCHEMA.$STG_TABLE_NAME ($STG_FIELDS) FROM LOCAL STDIN DELIMITER '$INPUT_FIELD_SEPARATOR' NULL '' DIRECT ;"
# With reject and exception files
# SQL_COPY_STDIN_DIRECT="COPY $STG_SCHEMA.$STG_TABLE_NAME ($STG_FIELDS) FROM LOCAL STDIN DELIMITER '$INPUT_FIELD_SEPARATOR' NULL '' REJECTED DATA '$REJECTED_FILE' EXCEPTIONS '$EXCEPTION_FILE' DIRECT ;"
# SQL_COPY_STDIN_DIRECT="COPY $STG_SCHEMA.$STG_TABLE_NAME ($STG_FIELDS,source_dt as to_date('$FILE_DATE','YYYYMMDD')) FROM LOCAL '$DATA_FILE_CNV' DELIMITER '$INPUT_FIELD_SEPARATOR' NULL '' REJECTED DATA '$REJECTED_FILE' EXCEPTIONS '$EXCEPTION_FILE' DIRECT ;"
# For type text
# SQL_COPY_STDIN_AUTO="COPY   $STG_SCHEMA.$STG_TABLE_NAME ($STG_FIELDS,source_dt as to_date('$FILE_DATE','YYYYMMDD')) FROM LOCAL '$DATA_FILE' DELIMITER '$INPUT_FIELD_SEPARATOR' NULL '' REJECTED DATA '$REJECTED_FILE' EXCEPTIONS '$EXCEPTION_FILE';"
# SQL_COPY_STDIN_DIRECT="COPY $STG_SCHEMA.$STG_TABLE_NAME ($STG_FIELDS) FROM LOCAL STDIN DELIMITER '$INPUT_FIELD_SEPARATOR' NULL '' DIRECT;"
# SQL_UPDATE_SOURCE_DT="UPDATE $STG_SCHEMA.$STG_TABLE_NAME SET $FIELD_SOURCE_DT = TO_DATE('<DATE>', 'YYYYMMDD');"
# SQL_SUM="SELECT $STG_FORMULA_SUM_FIELDS FROM $STG_SCHEMA.$STG_TABLE_NAME;";
SQL_COMMIT="COMMIT;"
