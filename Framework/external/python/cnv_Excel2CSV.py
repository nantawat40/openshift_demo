import pandas as pd
import numpy as np
import os
import glob
import shutil
import logging

log = logging.getLogger() # 'root' Logger

console = logging.StreamHandler()

format_str = '%(asctime)s\t%(levelname)8s -- %(processName)s %(filename)s:%(lineno)4s -- %(message)s'
console.setFormatter(logging.Formatter(format_str))

log.addHandler(console) # prints to console.
# log.setLevel(logging.ERROR) # anything ERROR or above
# log.setLevel(logging.DEBUG) # anything DEBUG
log.setLevel(logging.INFO) # anything INFO

log.info('Show example log level info')
log.warn('Show example log level warning')
log.critical('Show example log level critical')
log.error('Show example log level error')
log.debug('Show example log level debug')

source=os.environ['HOME_ETL']+"/configs/dwh/"
dest=os.environ['HOME_ETL']+"/landing/"
staging=os.environ['HOME_ETL']+"/input/stg/"
os.chdir(source)

for file in glob.glob('[A-Za-z]*.xlsx'):
# for file in glob.glob("dwh_mapping_stg_V1.xlsx"):
    
    try:
        # filename = os.path.basename(file)
        
        filename, file_extension = os.path.splitext(file)

        log.info("-------------------------------")
        log.info('Found filename : %s' % (file))
        log.info("-------------------------------")        



        xlsx = pd.ExcelFile(file)
        movies_sheets = []

        # list sheet on file excel
        for sheet in xlsx.sheet_names:
            log.info('Sheet name : ' + sheet)

            # get sheel 'DWH to DataFrame
            df = pd.read_excel(file,sheet_name = sheet)            
        
            log.info("-------------------------------")
            log.info('filename : %s on Sheet: %s = %d record(s)' % (file, sheet, len(df)))
            log.info("-------------------------------")

            # assign's 0 because have +1 before assign value
            DWH_No=0
            DWH_Table='N/A'

            # len(df) is value actual record remove header line
            for i in range(0, len(df)):
                
                # iterating the columns
                for col in df.columns: 
                    # log.info(col)  
                    
                    if (df[col][i] is np.nan) and (col == 'Group_Name' or col == 'DWH_Type' or col == 'DWH_Table') :

                        # row_index = df.Group_Name == i
                        # df.loc modify value on DataFrame fixed problem warning SettingWithCopyWarning
                        df.loc[i, col] = df[col][i-1]
                    elif col == 'DWH_Table' and DWH_Table != df[col][i] :
                        
                        if DWH_Table != 'N/A' :
                            DWH_No = 1

                            df.loc[i, 'DWH_No'] = DWH_No

                        DWH_Table = df[col][i]
                        

                    elif col == 'DWH_No' :
                        # increment value for next record
                        DWH_No = DWH_No + 1

                        df.loc[i, col] = DWH_No

            
            if sheet != 'DWH' :
                filename = "REPO_"+sheet
            else:
                filename = 'dwh_mapping_stg_V1'

            CSV_File=dest+filename+'.csv'

            log.info("write into file: %s" % (CSV_File))
            df.to_csv(CSV_File, encoding='utf-8', index=False)

            stagingTable="STG_"+filename.upper()
            # stagingFile=stagingFile.replace("_V1","")
            # log.info(stagingFile)

            #shutil.copy2(CSV_File, stagingFile)
            log.info("-------------------------------")
            log.info("    Convert xlsx file : %s to csv file %s" % (file,CSV_File))
            log.info("    Stage table : %s" % (stagingTable))

            log.info("-------------------------------")
            log.info("    $HOME/Framework/shell/stg/stg_load.sh -t %s -d 19990101" % (stagingTable))
            log.info("-------------------------------")

        # close variable
        xlsx.close()

        # return success
        pass

    except:
        log.info("An exception occurred")

        # return fail
        raise
