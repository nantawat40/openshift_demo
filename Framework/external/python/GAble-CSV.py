# not working beacuse we should not modify source file

import csv

import os
import glob

import re

# import codecs

# import pandas
# import unicodecsv

# encode uft8 require LC_ALL=en_US.UTF-8 (setup from .profile of user)


landing_path=os.environ['HOME_ETL']+"/landing"

# myfile = open(landing_path+'/'+'NTL_devices.csv')
# data = pandas.read_csv(myfile, encoding='utf-8', quotechar='"', delimiter=';')
# print(data.values)


# myfile = open(landing_path+'/'+'NTL_devices.csv')
# data = unicodecsv.reader(myfile, encoding='utf-8', delimiter=';')
# for row in data:                                                 
#     print(row)

# delimiter = ','
# reader = codecs.open(landing_path+'/'+'NTL_devices.csv', 'r', encoding='utf-8')
# for line in reader:
#     row = line.split(delimiter)
#     print(row)

# print(landing_path+'/'+'NTL_devices.csv')

def assign_type_field(Value) :

    newValue=Value

    if re.match('^([0-9]*)/([0-9]*)/([0-9][0-9][0-9][0-9]) ([0-9]*:[0-9]*)$',Value):

        matchObj = re.match('^([0-9]*)/([0-9]*)/([0-9][0-9][0-9][0-9]) ([0-9]*:[0-9]*)$',Value)

        newValue = matchObj.group(3)+"/"+matchObj.group(1)+"/"+matchObj.group(2)+" "+matchObj.group(4)+":00"
        # print("Date format dd/mm/YYYY HH:MM -> %s" % (newValue))

    # else:
        # print("Unknow")
        
    return(newValue)


with open (landing_path+'/'+'NTL_devices.csv',encoding='utf8') as csv_file:
    reader = csv.reader(csv_file, delimiter=",")

    next(reader) # skip first row 
    
    for row in reader:                
        print('Line : %d' %(reader.line_num))
        # print(row)

        # getting length of list 
        length = len(row) 
        for i in range(length): 
            FieldValue = row[i]

            FieldValue = assign_type_field(FieldValue)

            row[i] = FieldValue

            print('No. %d %s' % (i, FieldValue))

        print(row)
        # Force terminate
        exit(1)

    

    csv_file.seek(0) # goto first row
    row_count = list(reader)
    print(landing_path+'/'+'NTL_devices.csv %d line(s)' % (len(row_count)))        