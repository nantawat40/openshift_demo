#!/bin/bash

#--------------------------------------------------------------------
#              Copyright C - 2012 All rights reserved
#                        G-Able Thailand.
#
#       No part of this program may be reproduced or adapted in any
#             form or by any means, electronic or mechanical,
#       without permission from G-Able Thailand Thailand.
#          This program is confidential and may not be disclosed,
#          decompiled or reverse engineered without permission in
#             writing from G-Able Thailand Thailand.
#
#--------------------------------------------------------------------
# DESCRIPTION:
#    summerize log to table
#
#--------------------------------------------------------------------
# Revision   Date        CR No.   Author         Description
#====================================================================
#  0.2       02/04/2020  XXX      Suebsakul.A    Project Push to SEE, Your DATA
#
#
#
#


# Parameters
# -s [Initial | Staging | Warehouse] : stage for unit test PUSH to SEE, build unit test on all Stage

#Force run profile for support Job Schduler
. $HOME/.profile

# Load global variables and function library
GLOBAL_LIB_CONFIG=$HOME/Framework/configs/lib/global.inc
. $GLOBAL_LIB_CONFIG

EXEC_NAME="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")"
EXEC_PARA='[-d <BUSINESS DATE> -b <BATCH NO> -T <TEST MODE>]'
BUILD_NUM='on_demand'
STAGE='report'
TEST_MODE='No'
# UNIT_TEST_MODE='N/A'
TABLE_CONDITION=""
AREA='report'

BUSINESS_DATE=''

set -e
set -o pipefail

# Load global variables and function library
. $MAIN_CONFIG

#Check number of argment
if [ $# -ne 0 -a $# -ne 2 -a $# -ne 3 -a $# -ne 5 ]
then
  _usage
else
  let i=1
  for p in $*;
  do
    case "$p" in
      "-d")
        BUSINESS_DATE=${@:$((i+1)):1}
      ;;      
      "-b")
        BUILD_NUM=${@:$((i+1)):1}
      ;;               
      "-T")
        TEST_MODE='Yes'
      ;;      
      *)
        if [ $((i%2)) -eq 1 ]
        then
          _usage
        fi
      ;;      
    esac
    let i=i+1
  done;

  if [ -z $BUSINESS_DATE ]
  then
    # _usage
    BUSINESS_DATE=`date +%Y%m%d`
  fi

  if [ -z $BUILD_NUM ]
  then
    BUILD_NUM='on_demand'
  fi
  
fi


# initial variable
ROOT_LOG_PATH=$LOG_PATH
LOG_PATH=$LOG_PATH/$STAGE
# ARCHIVE_PATH=$ARCHIVE_PATH/$STAGE
TEST_PATH=$TEST_PATH/$STAGE/$BUILD_NUM
CONF_COMMON=$CONF_PATH/common
TEST_PATH=$TEST_PATH/$STAGE/$BUILD_NUM
INPUT_PATH=$INPUT_PATH/lnd/etl_logs

if [ ! -d $LOG_PATH ]
then
  mkdir -p $LOG_PATH
fi

if [ ! -d $ARCHIVE_PATH ]
then
  mkdir -p $ARCHIVE_PATH
fi

if [ ! -d $TEST_PATH ]
then
  mkdir -p $TEST_PATH
fi

# CONF_FILE=$CONF_PATH/$STAGE/$AREA.conf
LOG_FILE=$LOG_PATH/$AREA.log
RESULT_FILE=$LOG_PATH/$AREA.out
ERROR_FILE=$LOG_PATH/$AREA.err
SQL_FILE=$CONF_PATH/sql/$STAGE/unit_test.sql
TEST_FILE=$TEST_PATH/$AREA.result
PROC_FILE=$LOG_PATH/$AREA.proc

#--------------------------------------
# STEP 1. Check running process
#--------------------------------------
_preventMultipleInstance "$EXEC_NAME.*\ $AREA"  $PROC_FILE $LOG_FILE

#Define start time
START_TIME="$(date +%s%N)"

#--------------------------------------
# STEP 2. Loading Configuration
#--------------------------------------
_logger "Started ROOT LOG Path=$ROOT_LOG_PATH,BUSINESS DATE=$BUSINESS_DATE,Test Mode=${TEST_MODE}" $LOG_FILE

_logger "Clear previous rsult file : $RESULT_FILE" $LOG_FILE
rm -f $RESULT_FILE

_logger "List file on folder=$ROOT_LOG_PATH" $LOG_FILE
find $ROOT_LOG_PATH -type f -name *.log -newermt $BUSINESS_DATE | while read file_log
do    
    _logger "Found log file:$file_log" $LOG_FILE
    # cat ${file_log} | grep 'Started\|Elapsed\|STATUS\|END'
    # grep --with-filename 'Started\|Elapsed\|STATUS\|Table STG\|END' ${file_log}
    # cat ${file_log} >> $RESULT_FILE
    grep ^'[A-Za-z0-9]' ${file_log} | grep -v "\-------------------------------" >> $RESULT_FILE
done

RECORD="$(wc -l $RESULT_FILE | awk '{print $1}')"
MD5SUM="$(md5sum $RESULT_FILE | awk '{print $1}')"


FULLPATH_TEMPLATE_LND_CONTROL=$CREATE_PATH/LND_template.control

# create temp file
cp $FULLPATH_TEMPLATE_LND_CONTROL $TMP_PATH/etl_logs.control

# replace value on file conf
sed -i "s/\${LND_FILENAME}/etl_logs.log/" $TMP_PATH/etl_logs.control
sed -i "s/\${LND_MD5SUM}/$MD5SUM/"        $TMP_PATH/etl_logs.control
sed -i "s/\${ORI_HEADER}/N\/A/"           $TMP_PATH/etl_logs.control
sed -i "s/\${LND_RECNO}/$RECORD/"         $TMP_PATH/etl_logs.control

# Add more field structure
# 1
echo "        - name : datetime" >> $TMP_PATH/etl_logs.control
echo "          type : regular((20[0-9][0-9]-[01][0-9]-[0123][0-9] [012][0-9]:[0-5][0-9]:[0-5][0-9]) )" >> $TMP_PATH/etl_logs.control
echo "          description : xxx" >> $TMP_PATH/etl_logs.control
# 2
echo "        - name : area" >> $TMP_PATH/etl_logs.control
echo "          type : regular(([a-zA-Z0-9_]*.sh):)" >> $TMP_PATH/etl_logs.control
echo "          description : xxx" >> $TMP_PATH/etl_logs.control
# 3
echo "        - name : process_id" >> $TMP_PATH/etl_logs.control
echo "          type : regular( ([0-9]*):)" >> $TMP_PATH/etl_logs.control
echo "          description : xxx" >> $TMP_PATH/etl_logs.control
# 4
echo "        - name : message" >> $TMP_PATH/etl_logs.control
echo "          type : regular( (.*$))" >> $TMP_PATH/etl_logs.control
echo "          description : xxx" >> $TMP_PATH/etl_logs.control

cat $TMP_PATH/etl_logs.control

_logger "Found log: $RECORD record(s)" $LOG_FILE
_logger "Copy File: $RESULT_FILE to Input Path: $INPUT_PATH/etl_logs.log" $LOG_FILE
_logger "Copy File: $TMP_PATH/etl_logs.control to Input Path: $INPUT_PATH/etl_logs.control" $LOG_FILE

mv $RESULT_FILE $INPUT_PATH/etl_logs.log
mv $TMP_PATH/etl_logs.control $INPUT_PATH/etl_logs.control

#--------------------------------------
# Summary
#--------------------------------------
ELAPSED=$(_calcElapsedTime $START_TIME)

_logger "Elapsed $ELAPSED sec." $LOG_FILE
_logger "STATUS=$ST_CODE $ST_NAME" $LOG_FILE
_logger "END" $LOG_FILE
echo " " >> $LOG_FILE

