#!/bin/bash

#--------------------------------------------------------------------
#              Copyright C - 2012 All rights reserved
#                        G-Able Thailand.
#
#       No part of this program may be reproduced or adapted in any
#             form or by any means, electronic or mechanical,
#       without permission from G-Able Thailand Thailand.
#          This program is confidential and may not be disclosed,
#          decompiled or reverse engineered without permission in
#             writing from G-Able Thailand Thailand.
#
#--------------------------------------------------------------------
# DESCRIPTION:
#    create table/view/object on database on area staging
#
#--------------------------------------------------------------------
# Revision   Date        CR No.   Author    Description
#====================================================================
#
#
#
#
#
#

while true
do

    cd $HOME_ETL/external/c/

    dos2ux *.c *.h makefile
    echo "--------------------------------------------------------------------"
    echo "make clean"
    echo "--------------------------------------------------------------------"
    make clean

    echo "--------------------------------------------------------------------"
    echo "make"
    echo "--------------------------------------------------------------------"
    make

    echo "--------------------------------------------------------------------"
    echo "run ./gable-transform.x"
    echo "--------------------------------------------------------------------"
    ./gable-transform.x -f LND_Testfixlenght.dat -c LND_Testfixlenght.control

    RESULT=$?

    if [ $RESULT -ne 0 ]
    then
        echo -e "\n"
        echo "Terminate error return code [$RESULT]"
        exit 1
    fi

    date
    sleep 5
done