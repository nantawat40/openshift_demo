
select * from EMPLOYEE_TESTING_DWH.JOB_GRADE_MASTER

UPDATE
	JOB_GRADE_MASTER AS DWH_DES
INNER JOIN (
	SELECT
		JOB_GRADE_ID,
		JOB_GRADE_NAME,
		DESCRIPTION,
		STG_SOURCE_DT,
		STG_SOURCE_FILE,
		STG_LOAD_DT
	FROM
		JOB_GRADE_MASTER_CLASSIFICATION A
	WHERE
		!( (ERR_IS_PK_DUP
		OR ERR_IS_UK_DUP
		OR ERR_IS_NULL_VALUE
		OR ERR_IS_BLANK_VALUE
		OR ERR_LENGTH_OVERFLOW
		OR ERR_SOURCE_DT_MT_CURRENT_DATE)
		OR (1 = 0) )
		AND EXISTS (
		SELECT
			1
		FROM
			JOB_GRADE_MASTER B
		WHERE
			((B.JOB_GRADE_ID = A.JOB_GRADE_ID)
			AND (B.JOB_GRADE_NAME <> A.JOB_GRADE_NAME)
			AND (B.STG_SOURCE_DT <= A.STG_SOURCE_DT)
			AND (B.STG_SOURCE_FILE < A.STG_SOURCE_FILE) ) ) ) AS STG ON
	STG.JOB_GRADE_ID = DWH_DES.JOB_GRADE_ID SET
	DWH_DES.JOB_GRADE_NAME = STG.JOB_GRADE_NAME,
	DWH_DES.DESCRIPTION = STG.DESCRIPTION,
	DWH_DES.STG_SOURCE_DT = STG.STG_SOURCE_DT,
	DWH_DES.STG_SOURCE_FILE = STG.STG_SOURCE_FILE,
	DWH_DES.STG_LOAD_DT = STG.STG_LOAD_DT,
	DWH_DES.LAST_MODIFIED_DT = current_timestamp();