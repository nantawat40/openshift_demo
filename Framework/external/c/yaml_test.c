#include <stdio.h>
#include <yaml.h>

#define	MAXBLK 1024
#define	MAXFIELD 255

struct gable_arr_field {
    char name[MAXBLK/4];
    char type[MAXBLK/4];
	  char description[MAXBLK/4];
	  int offset;
};

struct gable_conf {
  char filename[MAXBLK/4];
  char md5sum[MAXBLK/4];
  char original_header[MAXBLK*2];
  char line[MAXBLK/4];
  struct gable_arr_field data[MAXFIELD];

  int field_no;
};

// enum YAML_field {filename = 0, md5sum = 0}; 

void level(int level)
{
  while ((--level)>0)
  {
    printf("-");
    // printf("\t",level);
  } 
}

void diplay_record_structure(struct gable_conf *YAML_Conf)
{
  printf("------------------------------------------------------------------------\n");
  printf("filename : [%s]\n",YAML_Conf->filename);
  printf("md5sum : [%s]\n",YAML_Conf->md5sum);
  printf("original_header : [%s]\n",YAML_Conf->original_header);
  printf("line : [%s]\n",YAML_Conf->line);  

  printf("field_no : [%d]\n",YAML_Conf->field_no);

  for(int i=0;i<YAML_Conf->field_no;i++)
  {
    printf("field_no : [%d], name: [%s], type: [%s], description: [%s]\n",i,YAML_Conf->data[i].name
                                                        ,YAML_Conf->data[i].type
                                                        ,YAML_Conf->data[i].description);
  }

  printf("------------------------------------------------------------------------\n");
}

int yaml_decode_file(char *FILE_YAML,struct gable_conf *YAML_Conf)
{
  FILE *fh = fopen(FILE_YAML, "r");
  yaml_parser_t parser;
  yaml_token_t  token;   /* new variable */
  

  // int level;
  int indent = 1;
  int previous_toktype;

  char previous_key[255];

  /* Initialize parser */
  if(!yaml_parser_initialize(&parser))
    fputs("Failed to initialize parser!\n", stderr);
  if(fh == NULL)
    fputs("Failed to open file!\n", stderr);

  /* Set input file */
  yaml_parser_set_input_file(&parser, fh);

  /* BEGIN new code */
  do {
    yaml_parser_scan(&parser, &token);
    switch(token.type)
    {
        /* Stream start/end */
        case YAML_STREAM_START_TOKEN: level(indent); indent++; puts("STREAM START"); previous_toktype=token.type; break;
        case YAML_STREAM_END_TOKEN:   indent--; level(indent); puts("STREAM END");   previous_toktype=token.type; break;

        /* Token types (read before actual token) */
        case YAML_KEY_TOKEN:    level(indent); printf("(Key token)   "); previous_toktype=token.type; break;
        case YAML_VALUE_TOKEN:  level(indent); printf("(Value token) "); previous_toktype=token.type; indent++; break;

        /* Block delimeters */
        case YAML_BLOCK_SEQUENCE_START_TOKEN: level(indent); puts("<b>Start Block (Sequence)</b>"); previous_toktype=token.type; break;
        case YAML_BLOCK_ENTRY_TOKEN:          level(indent); puts("<b>Start Block (Entry)</b>");    previous_toktype=token.type; break;
        case YAML_BLOCK_END_TOKEN:
                                              level(indent);
                                              indent--;
                                              puts("<b>End block</b>");
                                              previous_toktype=token.type;
                                              printf("\n");                                              
                                              break;

        /* Data */
        case YAML_BLOCK_MAPPING_START_TOKEN:  indent++; level(indent); puts("[Block mapping]"); previous_toktype=token.type; break;
        case YAML_SCALAR_TOKEN: 

          if (previous_toktype == YAML_KEY_TOKEN)
            strcpy(previous_key,(char *)token.data.scalar.value);

          //First Level
          if (indent == 6)
          {
            if (strcmp(previous_key,"filename") == 0)
            {
              strcpy(YAML_Conf->filename,(char *)token.data.scalar.value);
            }else if (strcmp(previous_key,"md5sum") == 0)
            {
              strcpy(YAML_Conf->md5sum,(char *)token.data.scalar.value);
            }else if (strcmp(previous_key,"original_header") == 0)
            {
              strcpy(YAML_Conf->original_header,(char *)token.data.scalar.value);
            }else if (strcmp(previous_key,"line") == 0)
            {
              strcpy(YAML_Conf->line,(char *)token.data.scalar.value);
            }


          }else if (indent == 8) //Level 2
          {
            
            if (strcmp(previous_key,"name") == 0)
            {
              strcpy(YAML_Conf->data[YAML_Conf->field_no].name,(char *)token.data.scalar.value);
            }else if (strcmp(previous_key,"type") == 0)
            {
              strcpy(YAML_Conf->data[YAML_Conf->field_no].type,(char *)token.data.scalar.value);
            }else if (strcmp(previous_key,"description") == 0)
            {
              strcpy(YAML_Conf->data[YAML_Conf->field_no].description,(char *)token.data.scalar.value);
            }

            if (
                (strlen(YAML_Conf->data[YAML_Conf->field_no].name) > 0) &&
                (strlen(YAML_Conf->data[YAML_Conf->field_no].type) > 0) &&
                (strlen(YAML_Conf->data[YAML_Conf->field_no].description) > 0)
              )
                YAML_Conf->field_no++;
          }

          
          printf("scalar %d : %s \n", indent, token.data.scalar.value);
          if (previous_toktype == YAML_VALUE_TOKEN)
          {
            printf("\n");
            indent--;
          }
        break;

        /* Others */
        default:
            printf("Got token of type %d\n", token.type);
    }

    if(token.type != YAML_STREAM_END_TOKEN)
        yaml_token_delete(&token);

  } while(token.type != YAML_STREAM_END_TOKEN);
  yaml_token_delete(&token);
  /* END new code */

  /* Cleanup */
  yaml_parser_delete(&parser);
  fclose(fh);

  return 0;
}


int main(void)
{
  struct gable_conf YAML_Conf;
  
  memset(&YAML_Conf,0x0,sizeof(struct gable_conf));
  yaml_decode_file("fruit.yaml",&YAML_Conf);

  diplay_record_structure(&YAML_Conf);  
  return 0;
}