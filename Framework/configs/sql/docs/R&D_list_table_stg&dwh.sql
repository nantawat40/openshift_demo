select
	t.TABLE_SCHEMA,
	sdmsv.STG_TABLE,
	t.TABLE_NAME
from information_schema.TABLES t
left join (select DWH_TABLE, STG_TABLE 
           from DATA_TESTING_STG.STG_DWH_MAPPING_STG_V1 
		   group by DWH_TABLE, STG_TABLE 
		   having length(trim(STG_TABLE)) > 0 ) sdmsv 
  on sdmsv.DWH_TABLE = t.TABLE_NAME
where 
	t.TABLE_NAME NOT like '%_ERR' and 
	t.TABLE_NAME NOT like '%_WRN%' and
	t.TABLE_TYPE = 'BASE TABLE' and
	t.TABLE_SCHEMA <> 'mysql' and
	t.TABLE_SCHEMA <> 'performance_schema' and
	t.TABLE_SCHEMA like 'DATA_TESTING_%'
order by t.TABLE_SCHEMA DESC, sdmsv.STG_TABLE;
