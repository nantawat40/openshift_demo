#!/bin/bash

#--------------------------------------------------------------------
#              Copyright C - 2012 All rights reserved
#                        G-Able Thailand.
#
#       No part of this program may be reproduced or adapted in any
#             form or by any means, electronic or mechanical,
#       without permission from G-Able Thailand Thailand.
#          This program is confidential and may not be disclosed,
#          decompiled or reverse engineered without permission in
#             writing from G-Able Thailand Thailand.
#
#--------------------------------------------------------------------
# DESCRIPTION:
#    summerize log to table
#
#--------------------------------------------------------------------
# Revision   Date        CR No.   Author         Description
#====================================================================
#  0.2       02/04/2020  XXX      Suebsakul.A    Project Push to SEE, Your DATA
#
#
#
#


# Parameters
# -s [Initial | Staging | Warehouse] : stage for unit test PUSH to SEE, build unit test on all Stage

#Force run profile for support Job Schduler
. $HOME/.profile

# Load global variables and function library
GLOBAL_LIB_CONFIG=$HOME/Framework/configs/lib/global.inc
. $GLOBAL_LIB_CONFIG

EXEC_NAME="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")"
EXEC_PARA='[-s < Initial_stg | Initial_dwh | Staging | Warehouse | All >] [-d <FILE DATE> -b <BATCH NO> -T <TEST MODE>]'
BUILD_NUM='on_demand'
STAGE='report'
TEST_MODE='No'
# UNIT_TEST_MODE='N/A'
TABLE_CONDITION=""
# AREA='unit_test'

TABLE_TYPE=''
TABLE_NAME=''
P_BUSINESS_DATE=''

set -e
set -o pipefail

# Load global variables and function library
. $MAIN_CONFIG

#Check number of argment
if [ $# -ne 2 -a $# -ne 3 -a $# -ne 4 -a $# -ne 5 -a $# -ne 7 ]
then
  _usage
else
  let i=1
  for p in $*;
  do
    case "$p" in
      "-s")
        AREA=${@:$((i+1)):1}        
      ;;   
      "-d")
        FILE_DATE=${@:$((i+1)):1}
      ;;      
      "-b")
        BUILD_NUM=${@:$((i+1)):1}
      ;;               
      "-T")
        TEST_MODE='Yes'
      ;;      
      *)
        if [ $((i%2)) -eq 1 ]
        then
          _usage
        fi
      ;;      
    esac
    let i=i+1
  done;

  if [ -z $STAGE ]
  then
    _usage
  fi

  if [ -z $BUILD_NUM ]
  then
    BUILD_NUM='on_demand'
  fi
  
fi


# initial variable
ROOT_LOG_PATH=$LOG_PATH
LOG_PATH=$LOG_PATH/$STAGE
# ARCHIVE_PATH=$ARCHIVE_PATH/$STAGE
TEST_PATH=$TEST_PATH/$STAGE/$BUILD_NUM
CONF_COMMON=$CONF_PATH/common

if [ ! -d $LOG_PATH ]
then
  mkdir -p $LOG_PATH
fi

if [ ! -d $ARCHIVE_PATH ]
then
  mkdir -p $ARCHIVE_PATH
fi

if [ ! -d $TEST_PATH ]
then
  mkdir -p $TEST_PATH
fi

# CONF_FILE=$CONF_PATH/$STAGE/$AREA.conf
LOG_FILE=$LOG_PATH/$AREA.log
RESULT_FILE=$LOG_PATH/$AREA.out
ERROR_FILE=$LOG_PATH/$AREA.err
SQL_FILE=$CONF_PATH/sql/$STAGE/unit_test.sql
TEST_FILE=$TEST_PATH/$AREA.result
PROC_FILE=$LOG_PATH/$AREA.proc

#--------------------------------------
# STEP 1. Check running process
#--------------------------------------
_preventMultipleInstance "$EXEC_NAME.*\ $AREA"  $PROC_FILE $LOG_FILE

#Define start time
START_TIME="$(date +%s%N)"

#--------------------------------------
# STEP 2. Loading Configuration
#--------------------------------------
_logger "Started ROOT LOG Path=$ROOT_LOG_PATH ,Test Mode=${TEST_MODE}" $LOG_FILE

_logger "List file on folder=$ROOT_LOG_PATH" $LOG_FILE
find $ROOT_LOG_PATH -name *.log | while read file_log
do
    echo "${file_log}"
    # cat ${file_log} | grep 'Started\|Elapsed\|STATUS\|END'
    grep --with-filename 'Started\|Elapsed\|STATUS\|Table STG\|END' ${file_log}
done

#--------------------------------------
# Summary
#--------------------------------------
ELAPSED=$(_calcElapsedTime $START_TIME)

_logger "Elapsed $ELAPSED sec." $LOG_FILE
_logger "STATUS=$ST_CODE $ST_NAME" $LOG_FILE
_logger "END" $LOG_FILE
echo " " >> $LOG_FILE

