package com.gable.main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDSimpleFont;
import org.apache.pdfbox.text.PDFTextStripper;


public class PDFEncoding {
	
	public static void main(String[] args) throws Throwable {	

//		PDDocument doc = PDDocument.load("C:/mydoc3.pdf");
//		PDDocument doc = new PDFDocument("C:\\Users\\Admin\\Desktop\\CSC\\SSO\\SSO\\ADC\\01_SSO.pdf", null);
		File PDFFile = new File("C:\\Users\\Admin\\Desktop\\CSC\\SSO\\SSO\\ADC\\01_SSO.pdf");			
		PDDocument document = PDDocument.load(PDFFile);
		for (int i = 0; i < document.getNumberOfPages(); ++i)
		{
		    PDPage page = document.getPage(i);
		    PDResources res = page.getResources();
		    for (COSName fontName : res.getFontNames())
		    {
		        // do stuff with the font
                PDFont font = res.getFont(fontName);
                System.out.println("FONT :: "+ font);		        
		    }
		    
		    PDFTextStripper textStripper=new PDFTextStripper();
		    System.out.println(textStripper.getText(document));
//		    textStripper.getFonts();		    
		}		
	}
}