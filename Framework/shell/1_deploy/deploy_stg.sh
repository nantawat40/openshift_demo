#!/bin/bash

#--------------------------------------------------------------------
#              Copyright C - 2012 All rights reserved
#                        G-Able Thailand.
#
#       No part of this program may be reproduced or adapted in any
#             form or by any means, electronic or mechanical,
#       without permission from G-Able Thailand Thailand.
#          This program is confidential and may not be disclosed,
#          decompiled or reverse engineered without permission in
#             writing from G-Able Thailand Thailand.
#
#--------------------------------------------------------------------
# DESCRIPTION:
#    create table/view/object on database on area staging
#
#--------------------------------------------------------------------
# Revision   Date        CR No.   Author         Description
#====================================================================
#  0.2       16/01/2020  XXX      Suebsakul.A    Project Push to SEE, Your DATA
#
#
#
#


# Parameters
# -f [Filename.sql] : new initial database, tables and object for data processing

#Force run profile for support Job Schduler
. $HOME/.profile

# Load global variables and function library
GLOBAL_LIB_CONFIG=$HOME/Framework/configs/lib/global.inc
. $GLOBAL_LIB_CONFIG

EXEC_NAME="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")"
EXEC_PARA='-f <FILE DDL> [-T <TEST MODE>]'
INITIAL='deploy'
STAGE='stg'
TEST_MODE='No'

# Load global variables and function library
. $MAIN_CONFIG

#Check number of argment
if [ $# -ne 2 -a $# -ne 3 ]
then
  _usage
else
  let i=1
  for p in $*;
  do
    case "$p" in
      "-f")
        FILE_DDL=${@:$((i+1)):1}
      ;;
      "-T")
        TEST_MODE='Yes'
      ;;      
      *)
        if [ $((i%2)) -eq 1 ]
        then
          _usage
        fi
      ;;
    esac
    let i=i+1
  done;

  if [ -z $FILE_DDL ]
  then
    _usage
  fi
  
fi

# initial variable
LOG_PATH=$LOG_PATH/$INITIAL
TEST_PATH=$TEST_PATH/$STAGE/$INITIAL

if [ ! -d $LOG_PATH ]
then
  mkdir -p $LOG_PATH
fi

if [ ! -d $TEST_PATH ]
then
  mkdir -p $TEST_PATH
fi

FULLPATH_FILE_DDL=$DEPLOY_PATH/stg/$FILE_DDL
#TABLENAME=$(_getFilename $FULLPATH_FILE_DDL)
STG_TABLE_NAME=$(_getFilename $FULLPATH_FILE_DDL)

LOG_FILE=$LOG_PATH/$STG_TABLE_NAME.log
RESULT_FILE=$LOG_PATH/$STG_TABLE_NAME.out
ERROR_FILE=$LOG_PATH/$STG_TABLE_NAME.err
SQL_FILE=$CONF_PATH/sql/$STAGE/stg.sql
TEST_FILE=$TEST_PATH/$STG_TABLE_NAME.result
# echo "$LOG_FILE"
# echo "$RESULT_FILE"
# echo "$ERROR_FILE"

#Define start time
START_TIME="$(date +%s%N)"

_logger "Started FILE_DDL=${FILE_DDL}" $LOG_FILE

if [ ! -f $FULLPATH_FILE_DDL ]
then    
  _logger "Unable to access file: $FULLPATH_FILE_DDL" $LOG_FILE
  exit $ST_UNEXPECTED_ERROR

# else
  # clear old temp file
#   rm -f $TMP_PATH/create_table_stg_out.sql
fi

# 1.2 SQL Configure
if [ -s $SQL_FILE ]
then
  . $SQL_FILE
else
  _logger "Unable to load SQL Configuration file: $SQL_FILE" $LOG_FILE
  _logger "STATUS=$ST_UNEXPECTED_ERROR $ST_UNEXPECTED_ERROR_NAME" $LOG_FILE
  exit $ST_UNEXPECTED_ERROR
fi

#--------------------------------------
# STEP 1. Execute Deploy on STG
#--------------------------------------
# NOW=$(date '+%F %T')

FULL_TMP_PATH=$TMP_PATH/stg_$FILE_DDL

_logger "Create table/view on File DDL : $FILE_DDL" $LOG_FILE

${DATABASE_CLIENT_COMMAND} --user ${DATABASE_STG_USERNAME} \
                           --password=${DATABASE_STG_PASSWORD} <$FULLPATH_FILE_DDL \
                           >$RESULT_FILE 2>$ERROR_FILE

#--------------------------------------
# STEP 2. Verify Return Code
#--------------------------------------
RESULT=$?

if [ $RESULT -ne 0 -o -s $ERROR_FILE ]
then
    #_logError $ERROR_FILE $LOG_FILE
    #ERROR=$(_readFile $ERROR_FILE 1)
    ERROR=$(_readFile $ERROR_FILE 1)

    ST_CODE=$ST_ERR_SQL_EXECUTE
    ST_NAME="$ST_ERR_SQL_EXECUTE_NAME $ERROR"
fi

#--------------------------------------
# STEP 2. Verify Object on DB
#--------------------------------------

if [ "$TEST_MODE" = "Yes" ]
then
    _logger "-------------------------------" $LOG_FILE
    _logger "Verify Table : $STG_TABLE_NAME" $LOG_FILE
    _logger "-------------------------------" $LOG_FILE

    #_logger "Execute SQL : $SQL_COUNT" $LOG_FILE

    ${DATABASE_CLIENT_COMMAND} --user ${DATABASE_STG_USERNAME} \
                            --password=${DATABASE_STG_PASSWORD} \
                            >$RESULT_FILE 2>$ERROR_FILE \
                            --unbuffered --silent --skip-column-names \
    <<EOF                           
    $SQL_SELECT_DATABASE
    \! echo "------------------------"
    $SQL_COUNT_ROW
    \! echo "------------------------"
    $SQL_LIST_COLUMNS
    \! echo "------------------------"
    $SQL_COUNT_COLUMNS
    \! echo "------------------------"
    \q
EOF

    # remove charecter [space, tab]
    sed 's/=[\t]/ = /g' <$RESULT_FILE >$RESULT_FILE.tmp
    mv $RESULT_FILE.tmp $RESULT_FILE
    #. $RESULT_FILE
    # cat $RESULT_FILE


    #_fileToLogger $RESULT_FILE

    _fileToLogger $RESULT_FILE $LOG_FILE

    #_logger "TOTAL_RECORD = $TOTAL_RECORD" $LOG_FILE
    #_logger "TOTAL_COLUMNS = $TOTAL_COLUMNS" $LOG_FILE
    _logger "-------------------------------" $LOG_FILE

    _logger "Copy File Out $RESULT_FILE to Test File : $TEST_FILE" $LOG_FILE
    cp $RESULT_FILE $TEST_FILE

fi
#--------------------------------------
# Summary
#--------------------------------------
ELAPSED=$(_calcElapsedTime $START_TIME)

_logger "Elapsed $ELAPSED sec." $LOG_FILE
_logger "STATUS=$ST_CODE $ST_NAME" $LOG_FILE
_logger "Load data to Table by script : \$HOME_ETL/shell/stg/stg_load.sh -t $STG_TABLE_NAME -d 19990101" $LOG_FILE
_logger "END" $LOG_FILE
echo " " >> $LOG_FILE

#--------------------------------------
# Verify result command for support Job Schduler
#--------------------------------------
#Force run profile for support Job Schduler
if [ $ST_CODE -eq $ST_RECONCILE_REC_ERROR -o $ST_CODE -eq $ST_RECONCILE_SUM_ERROR ]
then
  exit $ST_COMPLETE
else
  exit $ST_CODE
fi
