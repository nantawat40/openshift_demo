-- THIS FILE Generate by G-Able Framework
-- COMMENT Generate by G-Able Framework @2020-01-22 14:03:53

-- SELECTED DATABASE
USE EMPLOYEE_TESTING_DWH;

-- DROP TABLE IF FOUND
DROP TABLE IF EXISTS `EMPLOYEE_MASTER`;

-- CREATE TABLE DWH
CREATE TABLE `EMPLOYEE_MASTER` (


    -- Standard Field by G-Able Framework
    DWH_LOAD_DT      DATETIME       DEFAULT NOW() COMMENT 'Generate this field by G-Able Framework for DateTime Stage load',
    LAST_MODIFIED_DT DATETIME       DEFAULT NOW() COMMENT 'Generate this field by G-Able Framework for DateTime Stage load'
);
