#!/bin/bash

#--------------------------------------------------------------------
#              Copyright C - 2012 All rights reserved
#                        G-Able Thailand.
#
#       No part of this program may be reproduced or adapted in any
#             form or by any means, electronic or mechanical,
#       without permission from G-Able Thailand Thailand.
#          This program is confidential and may not be disclosed,
#          decompiled or reverse engineered without permission in
#             writing from G-Able Thailand Thailand.
#
#--------------------------------------------------------------------
# DESCRIPTION:
#    create table/view/object on database on area staging
#
#--------------------------------------------------------------------
# Revision   Date        CR No.   Author    Description
#====================================================================
#
#
#
#
#


# Parameters
# -f [Filename.sql] : new initial database, tables and object for data processing

#Force run profile for support Job Schduler
. $HOME/.profile

# Load global variables and function library
GLOBAL_LIB_CONFIG=$HOME/Framework/configs/lib/global.inc
. $GLOBAL_LIB_CONFIG

EXEC_NAME="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")"
EXEC_PARA='-s <SOURCE LND> [-d <FILE DATE> -T <TEST MODE>]'
INITIAL='lnd'
STAGE='lnd'
TEST_MODE='No'
MAX_RETRY_POLLING=5
WAIT_SEC_RETRY=10

# Load global variables and function library
. $MAIN_CONFIG

#Check number of argment
if [ $# -ne 2 -a $# -ne 3 -a $# -ne 4 -a $# -ne 5 ]
then
  _usage
else
  let i=1
  for p in $*;
  do
    case "$p" in
      "-s")
        SOURCE_LND=${@:$((i+1)):1}
      ;;
      "-d")
        FILE_DATE=${@:$((i+1)):1}
      ;;      
      "-T")
        TEST_MODE='Yes'
      ;;      
      *)
        if [ $((i%2)) -eq 1 ]
        then
          _usage
        fi
      ;;
    esac
    let i=i+1
  done;

  if [ -z $SOURCE_LND ]
  then
    _usage
  fi
  
fi

# Pass Default Date to Variable
if [ -z "$FILE_DATE" ]
then
  FILE_DATE=`date +%Y%m%d`

else
  _chkDate $FILE_DATE

fi

# initial variable
LOG_PATH=$LOG_PATH/$STAGE
OUTPUT_PATH=$INPUT_PATH/stg
INPUT_PATH=$INPUT_PATH/$STAGE
ARCHIVE_PATH=$ARCHIVE_PATH/$STAGE
TEST_PATH=$TEST_PATH/$STAGE

if [ ! -d $LOG_PATH ]
then
  mkdir -p $LOG_PATH
fi

if [ ! -d $ARCHIVE_PATH ]
then
  mkdir -p $ARCHIVE_PATH
fi

if [ ! -d $TEST_PATH ]
then
  mkdir -p $TEST_PATH
fi

CONF_FILE=$CONF_PATH/$STAGE/$SOURCE_LND.conf
LOG_FILE=$LOG_PATH/$SOURCE_LND.log
RESULT_FILE=$LOG_PATH/$SOURCE_LND.out
ERROR_FILE=$LOG_PATH/$SOURCE_LND.err
# SQL_FILE=$CONF_PATH/sql/$STAGE/stg.sql
TEST_FILE=$TEST_PATH/$SOURCE_LND.result
PROC_FILE=$LOG_PATH/$SOURCE_LND.proc

# echo "$CONF_FILE"
# echo "$LOG_FILE"
# echo "$RESULT_FILE"
# echo "$ERROR_FILE"
# echo "$TEST_FILE"
# echo "$PROC_FILE"


#--------------------------------------
# STEP 1. Check running process
#--------------------------------------
_preventMultipleInstance "$EXEC_NAME.*\ $SOURCE_LND"  $PROC_FILE $LOG_FILE

#Define start time
START_TIME="$(date +%s%N)"

#--------------------------------------
# STEP 2. Loading Configuration
#--------------------------------------
_logger "Started SOURCE_LND=${SOURCE_LND}, FILE_DATE=${FILE_DATE}" $LOG_FILE

if [ ! -f $CONF_FILE ]
then    
  _logger "Unable to access file: $CONF_FILE" $LOG_FILE
  exit $ST_UNEXPECTED_ERROR

else
  # clear old temp file
  # rm -f $TMP_PATH/create_table_stg_out.sql
  . $CONF_FILE
fi

TMP_PATH=$TMP_PATH/$STAGE/$SUB_PATH


if [ ! -d $TMP_PATH ]
then
  mkdir -p $TMP_PATH
fi

OUTPUT_PATH=$OUTPUT_PATH/$SUB_PATH

if [ ! -d $OUTPUT_PATH ]
then
  mkdir -p $OUTPUT_PATH
fi

# echo "$TMP_PATH"
# echo "$OUTPUT_PATH"

# 1.2 SQL Configure
# if [ -s $SQL_FILE ]
# then
#   . $SQL_FILE
# else
#   _logger "Unable to load SQL Configuration file: $SQL_FILE" $LOG_FILE
#   _logger "STATUS=$ST_UNEXPECTED_ERROR $ST_UNEXPECTED_ERROR_NAME" $LOG_FILE
#   exit $ST_UNEXPECTED_ERROR
# fi


# NOW=$(date '+%F %T')

POLLING_PATH=$INPUT_PATH/$SUB_PATH

#--------------------------------------
# STEP 3. List Input File
#--------------------------------------
_logger "Group Name : $GROUP_NAME" $LOG_FILE

INPUT_FILE=$(_replace "$INPUT_FILE" "YYYYMMDD" "$FILE_DATE")
INPUT_FILE=$(_replace "$INPUT_FILE" "HHMMSS" ".*")

SHOW_MAX_RETRY_POLLING=$(expr $MAX_RETRY_POLLING - 1)
_logger "Input Type : $INPUT_TYPE" $LOG_FILE
_logger "Polling PATH : $POLLING_PATH" $LOG_FILE
_logger "Pattern Filename : $INPUT_FILE" $LOG_FILE
_logger "Retry Polling : $SHOW_MAX_RETRY_POLLING" $LOG_FILE

RETRY=$MAX_RETRY_POLLING
while [ $RETRY -gt 1 ]
do
  
  # switch off Exit immediately if a command exits with a non-zero status
  set +e

  LIST_FILE_ARRAY=($(find $POLLING_PATH -type f | sort | egrep "${INPUT_FILE}$"))

  # switch on Exit immediately if a command exits with a non-zero status
  set -e

  NUMBER_OF_INPUT_VERIFY=${#LIST_FILE_ARRAY[@]}

  if [ $NUMBER_OF_INPUT_VERIFY -gt 0 ]
  then
    _logger "Found : $NUMBER_OF_INPUT_VERIFY file(s)" $LOG_FILE
    RETRY=0
  else
    RETRY=$(expr $RETRY - 1)

    _logger "Retry Polling PATH : $POLLING_PATH" $LOG_FILE
    _logger "Wait : $WAIT_SEC_RETRY($RETRY)" $LOG_FILE
    sleep $WAIT_SEC_RETRY
  fi

done


# Not found file on Input Path
if [ $NUMBER_OF_INPUT_VERIFY -eq 0 ]
then
  _logger "$ST_NO_INPUT_NAME: $INPUT_FILE" $LOG_FILE
  ST_CODE=$ST_NO_INPUT
  ST_NAME=$ST_NO_INPUT_NAME
fi


#--------------------------------------
# STEP 3. Convert file to CSV file
#--------------------------------------
if [ $ST_CODE -eq $ST_COMPLETE ]
then

  #Truncate Table
  _logger "Convert to file CSV file..." $LOG_FILE

  # switch off Exit immediately if a command exits with a non-zero status
  # set +e

#   ${DATABASE_CLIENT_COMMAND} --user ${DATABASE_ETL_USERNAME} \
#                          --password=${DATABASE_ETL_PASSWORD} -e "$SQL_TRUNCATE" \
#                            >$RESULT_FILE 2>$ERROR_FILE

  # RESULT=$?

  # switch on Exit immediately if a command exits with a non-zero status
  # set -e
  
  count_input_file=0
  for DATA_FILE in ${LIST_FILE_ARRAY[@]}
  do
    
    FULL_INPUT_FILE=$DATA_FILE
    
    FILENAME=$(_getFilename $DATA_FILE)
    FILE_EXT=$(_getExtension $DATA_FILE)
        
#     STG_SOURCE_FILE=$FILENAME.$FILE_EXT

    # command because found error on option set -e
    # ((count_input_file++))
    count_input_file=$(( $count_input_file + 1 ))

    _logger "Convert File[$count_input_file] : $FULL_INPUT_FILE" $LOG_FILE

    #Verify control file
    chk_sum=$(_getValueYAML $POLLING_PATH/$FILENAME.control LND md5sum)
    _logger "Get check sum of control file : $chk_sum" $LOG_FILE

    file_sum=$(md5sum $POLLING_PATH/$FILENAME.$FILE_EXT | awk '{print $1}')
    _logger "Get check sum of input file : $file_sum" $LOG_FILE    

    # echo "[$chk_sum]"
    # echo "$file_sum"

    if [ "$chk_sum" != "$file_sum" ]
    then
      _logger "Check Sum Control File [$chk_sum] <> [$file_sum] input File" $LOG_FILE    
      ST_CODE=$ST_CONVERSION_ERROR
      ST_NAME="$ST_CONVERSION_ERROR_NAME $ERROR"      
    else
      _logger "Check Sum Control File [$chk_sum] = [$file_sum] input File" $LOG_FILE          
    fi

    # exit 1

    _logger "Execute External command line : $EXTERNAL_CLI_TRANSFORMATION -f $SUB_PATH/$FILENAME.$FILE_EXT -c $SUB_PATH/$FILENAME.control" $LOG_FILE
    # switch off Exit immediately if a command exits with a non-zero status
    set +e

    $EXTERNAL_CLI_TRANSFORMATION -f $SUB_PATH/$FILENAME.$FILE_EXT -c $SUB_PATH/$FILENAME.control >$RESULT_FILE 2>$ERROR_FILE

    RESULT=$?

    # switch on Exit immediately if a command exits with a non-zero status
    set -e


    
    # cat $ERROR_FILE    

    if [ $RESULT -ne 0 ]
    then
        _logError $ERROR_FILE $LOG_FILE
        ERROR=$(_readFile $ERROR_FILE 1)
        # ERROR=$(_readFile $ERROR_FILE 1)

        ST_CODE=$ST_CONVERSION_ERROR
        ST_NAME="$ST_CONVERSION_ERROR_NAME $ERROR"
    else

      #--------------------------------------
      # STEP X. Verify Record Number
      #--------------------------------------
      cat $RESULT_FILE

      LND_SOURCE_CONTROL=$POLLING_PATH/$FILENAME.control

      _logger "Cnvert Input File: $FILENAME.$FILE_EXT to $OUTPUT_PATH/$FILENAME.$FILE_EXT.cnv" $LOG_FILE
      _logger "Copy Control File: $FILENAME.$FILE_EXT.control to $OUTPUT_PATH/$FILENAME.$FILE_EXT.control" $LOG_FILE

      STG_SOURCE_CONTROL=$OUTPUT_PATH/$FILENAME.$FILE_EXT.control

      cp $LND_SOURCE_CONTROL $STG_SOURCE_CONTROL

      RECORD="$(wc -l  $OUTPUT_PATH/$FILENAME.$FILE_EXT.cnv | awk '{print $1}')"
      MD5SUM="$(md5sum $OUTPUT_PATH/$FILENAME.$FILE_EXT.cnv | awk '{print $1}')"

      # generate control file
      echo "- STG: " >> $STG_SOURCE_CONTROL
      echo "    filename: $FILENAME.$FILE_EXT.cnv" >> $STG_SOURCE_CONTROL
      echo "    md5sum: $MD5SUM"                   >> $STG_SOURCE_CONTROL
      echo "    original_header: datetime|area|message"  >> $STG_SOURCE_CONTROL
      echo "    recommand_header: DATETIME,AREA,MESSAGE" >> $STG_SOURCE_CONTROL
      echo "    line: $RECORD" >> $STG_SOURCE_CONTROL

      

#       # minus - 1 because ignore line Header
#       FILE_REC_NO=$(expr $FILE_REC_NO - 1)

#       ${DATABASE_CLIENT_COMMAND} --user ${DATABASE_ETL_USERNAME} \
#                             --password=${DATABASE_ETL_PASSWORD} -e "$SQL_COUNT_ROW_STG_SOURCE_FILE" \
#                             --unbuffered --silent --skip-column-names \
#                               >$RESULT_FILE 2>$ERROR_FILE      

#       sed 's/=[\t]/=/g' <$RESULT_FILE >$RESULT_FILE.tmp
#       mv $RESULT_FILE.tmp $RESULT_FILE

#       . $RESULT_FILE

#       FILE_REC_NO=$(_format $FILE_REC_NO)
#       TOTAL_RECORD=$(_format $TOTAL_RECORD)

#       if [ "$FILE_REC_NO" !=  "$TOTAL_RECORD" ]
#       then
#         _logger "Error Record Number between FILE Control [$FILE_REC_NO]  <> [$TOTAL_RECORD] Table STG" $LOG_FILE
#         ST_CODE=$ST_RECONCILE_REC_ERROR
#         ST_NAME="$ST_RECONCILE_REC_ERROR_NAME $ERROR"        
#       else
#         _logger "Verify Record Number between FILE Control [$FILE_REC_NO]  = [$TOTAL_RECORD] Table STG" $LOG_FILE
#       fi            
    fi
  done 
  # for DATA_FILE in ${LIST_F

fi

# #--------------------------------------
# # STEP X. Verify Object on DB
# #--------------------------------------

# if [ "$TEST_MODE" = "Yes" ]
# then
#     _logger "-------------------------------" $LOG_FILE
#     _logger "Verify Table : $STG_TABLE_NAME" $LOG_FILE
#     _logger "-------------------------------" $LOG_FILE

#     . $SQL_FILE

#     ${DATABASE_CLIENT_COMMAND} --user ${DATABASE_ETL_USERNAME} \
#                            --password=${DATABASE_ETL_PASSWORD} \
#                             >$RESULT_FILE 2>$ERROR_FILE \
#                             --unbuffered --silent --skip-column-names \
#                             -e "$SQL_GET_ALL_FIELDS"

#     STG_ALL_FIELDS=$(cat $RESULT_FILE)
#     CND_WHERE_IS_NULL=$(echo "$STG_ALL_FIELDS" | sed -e 's/,/ IS NULL or /g' -e 's/$/ IS NULL/g')
#     CND_WHERE_IS_BLANK=$(echo "$STG_ALL_FIELDS" | sed -e 's/^/(/g' -e 's/,/),(/g' -e 's/$/)/g' | sed -e 's/(/LENGTH(/g' | sed -e 's/),/) = 0 or /g' -e 's/$/ = 0/')

#     CND_WHERE="$CND_WHERE_IS_NULL or "$CND_WHERE_IS_BLANK

#     . $SQL_FILE

#     ${DATABASE_CLIENT_COMMAND} --user ${DATABASE_ETL_USERNAME} \
#                            --password=${DATABASE_ETL_PASSWORD} \
#                             >$RESULT_FILE 2>$ERROR_FILE \
#                             --unbuffered -t \
#     <<EOF                           
#     $SQL_SELECT_DATABASE
#     \! echo "------------------------"
#     \! echo "NUMBER COLUMNS"
#     \! echo "------------------------"
#     $SQL_COUNT_COLUMNS
#     \! echo "------------------------"
#     $SQL_COUNT_ROW
#     \! echo "------------------------"
#     \! echo "SHOW RECORD PER SOURCE"
#     \! echo "------------------------"    
#     $SQL_VERIFY_COUNT_ROW_STG
#     \! echo "------------------------"    
#     \! echo "RECORD NULL or BLANK"
#     \! echo "------------------------"        
#     $SQL_VERIFY_VALUE_NULL
#     \! echo "------------------------"
#     \! echo "ALL ROW DUPLICATE"
#     \! echo "------------------------"    
#     $SQL_VERIFY_DUP_ROW_STG
#     \q
# EOF

#     cat $RESULT_FILE

#     # remove charecter [space, tab]
#     sed 's/=[\t]/ = /g' <$RESULT_FILE >$RESULT_FILE.tmp
#     mv $RESULT_FILE.tmp $RESULT_FILE
    
#     #. $RESULT_FILE
#     # cat $RESULT_FILE
#     #_fileToLogger $RESULT_FILE

#     _fileToLogger $RESULT_FILE $LOG_FILE

#     #_logger "TOTAL_RECORD = $TOTAL_RECORD" $LOG_FILE
#     #_logger "TOTAL_COLUMNS = $TOTAL_COLUMNS" $LOG_FILE
#     _logger "-------------------------------" $LOG_FILE

#     _logger "Copy File Out $RESULT_FILE to Test File : $TEST_FILE" $LOG_FILE
#     cp $RESULT_FILE $TEST_FILE

# fi

#--------------------------------------
# Summary
#--------------------------------------
ELAPSED=$(_calcElapsedTime $START_TIME)

_logger "Elapsed $ELAPSED sec." $LOG_FILE
_logger "STATUS=$ST_CODE $ST_NAME" $LOG_FILE
_logger "END" $LOG_FILE
echo " " >> $LOG_FILE

#--------------------------------------
# Verify result command for support Job Schduler
#--------------------------------------
#Force run profile for support Job Schduler
# if [ $ST_CODE -eq $ST_RECONCILE_REC_ERROR -o $ST_CODE -eq $ST_RECONCILE_SUM_ERROR ]
# then
#   exit $ST_COMPLETE
# else
#   exit $ST_CODE
# fi

exit $ST_CODE
