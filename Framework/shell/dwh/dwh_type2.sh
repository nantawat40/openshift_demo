#!/bin/bash

#--------------------------------------------------------------------
#              Copyright C - 2012 All rights reserved
#                        G-Able Thailand.
#
#       No part of this program may be reproduced or adapted in any
#             form or by any means, electronic or mechanical,
#       without permission from G-Able Thailand Thailand.
#          This program is confidential and may not be disclosed,
#          decompiled or reverse engineered without permission in
#             writing from G-Able Thailand Thailand.
#
#--------------------------------------------------------------------
# DESCRIPTION:
#    create table/view/object on database on area staging
#
#--------------------------------------------------------------------
# Revision   Date        CR No.   Author         Description
#====================================================================
#  0.2       16/01/2020  XXX      Suebsakul.A    Project Push to SEE, Your DATA
#
#
#
#


# Parameters
# -t [Table Staging] : Table Staging, build table dimension on Data Warehouse

#Force run profile for support Job Schduler
. $HOME/.profile

# Load global variables and function library
GLOBAL_LIB_CONFIG=$HOME/Framework/configs/lib/global.inc
. $GLOBAL_LIB_CONFIG

EXEC_NAME="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")"
EXEC_PARA='-t <TABLE> [-T <TEST MODE>]'
INITIAL='dwh'
STAGE='dwh'
TEST_MODE='No'

# Load global variables and function library
. $MAIN_CONFIG

#Check number of argment
if [ $# -ne 2 -a $# -ne 3 -a $# -ne 4 ]
then
  _usage
else
  let i=1
  for p in $*;
  do
    case "$p" in
      "-t")
        DWH_TABLE_NAME=${@:$((i+1)):1}
        TNAME=$DWH_TABLE_NAME
      ;;   
      "-T")
        TEST_MODE='Yes'
      ;;      
      *)
        if [ $((i%2)) -eq 1 ]
        then
          _usage
        fi
      ;;
    esac
    let i=i+1
  done;

  if [ -z $DWH_TABLE_NAME ]
  then
    _usage
  fi
  
fi



# initial variable
LOG_PATH=$LOG_PATH/$STAGE
INPUT_PATH=$INPUT_PATH/$STAGE
ARCHIVE_PATH=$ARCHIVE_PATH/$STAGE
TEST_PATH=$TEST_PATH/$STAGE/$INITIAL

if [ ! -d $LOG_PATH ]
then
  mkdir -p $LOG_PATH
fi

if [ ! -d $ARCHIVE_PATH ]
then
  mkdir -p $ARCHIVE_PATH
fi

if [ ! -d $TEST_PATH ]
then
  mkdir -p $TEST_PATH
fi

CONF_FILE=$CONF_PATH/$STAGE/$DWH_TABLE_NAME.conf
LOG_FILE=$LOG_PATH/$DWH_TABLE_NAME.log
RESULT_FILE=$LOG_PATH/$DWH_TABLE_NAME.out
ERROR_FILE=$LOG_PATH/$DWH_TABLE_NAME.err
SQL_FILE=$CONF_PATH/sql/$STAGE/dwh_type_2.sql
TEST_FILE=$TEST_PATH/$DWH_TABLE_NAME.result
PROC_FILE=$LOG_PATH/$TNAME.proc

# echo "$LOG_FILE"
# echo "$RESULT_FILE"
# echo "$ERROR_FILE"

#--------------------------------------
# STEP 1. Check running process
#--------------------------------------
_preventMultipleInstance "$EXEC_NAME.*\ $DWH_TABLE_NAME"  $PROC_FILE $LOG_FILE

#Define start time
START_TIME="$(date +%s%N)"

#--------------------------------------
# STEP 2. Loading Configuration
#--------------------------------------
_logger "Started DWH_TABLE_NAME=${DWH_TABLE_NAME}" $LOG_FILE



if [ ! -f $CONF_FILE ]
then    
  _logger "Unable to access file: $CONF_FILE" $LOG_FILE
  exit $ST_UNEXPECTED_ERROR

else
  # clear old temp file
  # rm -f $TMP_PATH/create_table_stg_out.sql
  . $CONF_FILE
fi

# 1.2 SQL Configure
if [ -s $SQL_FILE ]
then
  . $SQL_FILE
else
  _logger "Unable to load SQL Configuration file: $SQL_FILE" $LOG_FILE
  _logger "STATUS=$ST_UNEXPECTED_ERROR $ST_UNEXPECTED_ERROR_NAME" $LOG_FILE
  exit $ST_UNEXPECTED_ERROR
fi


# NOW=$(date '+%F %T')

# FULL_TMP_PATH=$TMP_PATH/stg_$FILE_DDL

#--------------------------------------
# STEP 3. List Input File
#--------------------------------------
_logger "Group Name : $GROUP_NAME" $LOG_FILE

_logger "Build Dynamic SQL" $LOG_FILE

_logger "Get Reult from DB:SQL_GET_LIST_FIELD_ERR" $LOG_FILE
SQL_QUERY_GET_RESULT=$(_removeCR "$SQL_GET_LIST_FIELD_ERR")
LIST_FLAG_ERR=$(_getDBresult "$SQL_QUERY_GET_RESULT")

_logger "Get Reult from DB:SQL_GET_LIST_FIELD_WRN" $LOG_FILE
SQL_QUERY_GET_RESULT=$(_removeCR "$SQL_GET_LIST_FIELD_WRN")
LIST_FLAG_WRN=$(_getDBresult "$SQL_QUERY_GET_RESULT")

# IF none WRN Flag
if [ "$LIST_FLAG_WRN" == "NULL" ]
then

  LIST_FLAG_WRN="1 = 0"
fi

_logger "Get Reult from DB:SQL_LIST_CND_NOT_IN" $LOG_FILE
SQL_QUERY_GET_RESULT=$(_removeCR "$SQL_LIST_CND_NOT_IN")
LIST_CND_NOT_IN=$(_getDBresult "$SQL_QUERY_GET_RESULT")

_logger "Get Reult from DB:SQL_LIST_CND_NOT_IN_PK" $LOG_FILE
SQL_QUERY_GET_RESULT=$(_removeCR "$SQL_LIST_CND_NOT_IN_PK")
LIST_CND_NOT_IN_PK=$(_getDBresult "$SQL_QUERY_GET_RESULT")

# For condition field exists
LIST_CND_EXISTS_PK=$(echo "$LIST_CND_NOT_IN_PK" | sed "s/B\./T./g")



# _logger "Get Reult from DB:SQL_FOR_INSERT (2/2)" $LOG_FILE
# SQL_QUERY_GET_RESULT=$(_removeCR "$SQL_GET_LIST_UK")
# DB_RESULT=$(_getDBresult "$SQL_QUERY_GET_RESULT")

_logger "Get Reult from DB:SQL_FOR_UPDATE_LAST (1/3)" $LOG_FILE
SQL_QUERY_GET_RESULT=$(_removeCR "$SQL_CND_UPDATE_TYPE2")
LIST_CND_UPDATE_TYPE2=$(_getDBresult "$SQL_QUERY_GET_RESULT")

# replace string Number when user not define CDC Field
LIST_CND_UPDATE_TYPE2=${LIST_CND_UPDATE_TYPE2/NULL(/(}

_logger "Get Reult from DB:SQL_FOR_UPDATE_LAST (2/3)" $LOG_FILE
SQL_QUERY_GET_RESULT=$(_removeCR "$SQL_JOIN_UPDATE_TYPE2")
LIST_JOIN_UPDATE_PK=$(_getDBresult "$SQL_QUERY_GET_RESULT")



_logger "Get Reult from DB:SQL_FOR_INSERT_NEW (1/3)" $LOG_FILE
SQL_QUERY_GET_RESULT=$(_removeCR "$SQL_LIST_FIELD_INSERT_STG")
LIST_FIELD_SOURCE_STG=$(_getDBresult "$SQL_QUERY_GET_RESULT")

_logger "Get Reult from DB:SQL_FOR_INSERT_NEW (2/3)" $LOG_FILE
SQL_QUERY_GET_RESULT=$(_removeCR "$SQL_LIST_FIELD_INSERT_DWH")
LIST_FIELD_SOURCE_DWH=$(_getDBresult "$SQL_QUERY_GET_RESULT")

_logger "Get Reult from DB:SQL_FOR_INSERT_NEW (3/3)" $LOG_FILE
SQL_GET_LIST_PK=$(_removeCR "$SQL_GET_LIST_PK")
LIST_FIELD_PK=$(_getDBresult "$SQL_GET_LIST_PK")

_logger "Get Reult from DB:SQL_FOR_INSERT_CHG (1/4)" $LOG_FILE
SQL_QUERY_GET_RESULT=$(_removeCR "$SQL_GET_HIST_FIELDS_NAME")
FIELDS_HIST_NAME=$(_getDBresult "$SQL_QUERY_GET_RESULT")

_logger "Get Reult from DB:SQL_FOR_INSERT_CHG (2/4)" $LOG_FILE
SQL_QUERY_GET_RESULT=$(_removeCR "$SQL_GET_HIST_FIELDS_VALUE")
FIELDS_HIST_VALUE=$(_getDBresult "$SQL_QUERY_GET_RESULT")

_logger "Get Reult from DB:SQL_FOR_INSERT_CHG (3/4)" $LOG_FILE
SQL_QUERY_GET_RESULT=$(_removeCR "$SQL_JOIN_LAST_RECORD")
LIST_ONJOIN_FIELD_LAST_RECORD=$(_getDBresult "$SQL_QUERY_GET_RESULT")

_logger "Get Reult from DB:SQL_FOR_INSERT_CHG (4/4)" $LOG_FILE
SQL_QUERY_GET_RESULT=$(_removeCR "$SQL_GET_LIST_SEQ")
GET_FIELD_SEQ=$(_getDBresult "$SQL_QUERY_GET_RESULT")

# Get field History END DATE
for word in $FIELDS_HIST_VALUE
do 
  
  # if echo "$word" | grep 'END_DATE'; then
  if [[ "$word" == *"END_DATE" ]]; then
    FIELD_END_DATE=$word
  fi

done

. $SQL_FILE

_logger "Get Reult from DB:SQL_FOR_UPDATE_LAST (3/3)" $LOG_FILE
SQL_QUERY_GET_RESULT=$(_removeCR "$SQL_UPDATE_TYPE2_VALUE")
FIELDS_UPDATE_TYPE2_VALUE=$(_getDBresult "$SQL_QUERY_GET_RESULT")

# echo "$SQL_UPDATE_TYPE2_VALUE"
# echo "$FIELD_END_DATE"

# Repalce space charecter by function trim all fields
LIST_FIELD_SOURCE_STG_TRANSFORM=$(echo "TRIM($LIST_FIELD_SOURCE_STG)" | sed 's/, /),'\\\n\\\t\\\t\\\t'TRIM(/g' | sed 's/TRIM(\(STG_[A-Za-z1-9\_]*\))/\1/g')

. $SQL_FILE

# echo "$DATABASE_NAME"
# echo "$SQL_LIST_FIELD_INSERT_STG"
# echo "[$LIST_FIELD_SOURCE_STG]"
# echo "$SQL_DELETE_WRN_LAST"
# echo "$SQL_INSERT_WRN_LAST"
# echo "$SQL_DELETE_WRN"
# echo "$SQL_INSERT_ERR"
# echo "$SQL_INSERT_WRN"

# 1
# echo "$SQL_UPDATE_TYPE2_END_DATE_LAST_RECORD"

# 2
# echo "$SQL_INSERT_NEW"

# 3
# echo "$SQL_LAST_RECORD"
# echo "$SQL_INSERT_CNG_RECORD"

# exit 1

_logger "Execute SQL for DWH Type 2" $LOG_FILE

# switch off Exit immediately if a command exits with a non-zero status
set +e

${DATABASE_CLIENT_COMMAND} --user ${DATABASE_ETL_USERNAME} \
                       --password=${DATABASE_ETL_PASSWORD} \
                        >$RESULT_FILE 2>$ERROR_FILE \
                        --unbuffered -q \
<<EOF
$SQL_SELECT_DATABASE
\! echo "DELETE_WARN_LAST_RECORD="  | tr -d '\n'
$SQL_DELETE_WRN_LAST
SELECT ROW_COUNT();
\! echo "INSERT_WARN_LAST_RECORD="  | tr -d '\n'
$SQL_INSERT_WRN_LAST
SELECT ROW_COUNT();
\! echo "DELETE_WARN_RECORD="  | tr -d '\n'
$SQL_DELETE_WRN
SELECT ROW_COUNT();
\! echo "INSERT_ERROR_RECORD="  | tr -d '\n'
$SQL_INSERT_ERR
SELECT ROW_COUNT();
\! echo "INSERT_WARN_RECORD="  | tr -d '\n'
$SQL_INSERT_WRN
SELECT ROW_COUNT();
\! echo "UPDATE_LAST_RECORD="  | tr -d '\n'
$SQL_UPDATE_TYPE2_END_DATE_LAST_RECORD
SELECT ROW_COUNT();
\! echo "INSERT_NEW_RECORD="  | tr -d '\n'
$SQL_INSERT_NEW
SELECT ROW_COUNT();
\! echo "INSERT_CHG_RECORD="  | tr -d '\n'
$SQL_INSERT_CNG_RECORD
SELECT ROW_COUNT();
\q
EOF

RESULT=$?

# switch on Exit immediately if a command exits with a non-zero status
set -e

if [ $RESULT -ne 0 -o -s $ERROR_FILE ]
then
    ERROR=$(_readFile $ERROR_FILE 1)

    ST_CODE=$ST_ERR_SQL_EXECUTE
    ST_NAME="$ST_ERR_SQL_EXECUTE_NAME $ERROR"

else
  sed -i 's/ROW_COUNT()//g'    $RESULT_FILE
  sed -i ':a;N;$!ba;s/=\n/=/g' $RESULT_FILE
  . $RESULT_FILE

  echo "DELETE_WARN_LAST_RECORD=$DELETE_WARN_LAST_RECORD"
  echo "INSERT_WARN_LAST_RECORD=$INSERT_WARN_LAST_RECORD"
  echo "DELETE_WARN_RECORD=$DELETE_WARN_RECORD"
  echo "INSERT_ERROR_RECORD=$INSERT_ERROR_RECORD"
  echo "INSERT_WARN_RECORD=$INSERT_WARN_RECORD"
  echo "UPDATE_LAST_RECORD=$UPDATE_LAST_RECORD"
  echo "INSERT_NEW_RECORD=$INSERT_NEW_RECORD"
  echo "INSERT_CHG_RECORD=$INSERT_CHG_RECORD"
  
fi

#--------------------------------------
# STEP X. Verify Object on DB
#--------------------------------------

if [ "$TEST_MODE" = "Yes" ]
then
    _logger "-------------------------------" $LOG_FILE
    _logger "Verify Table : $DWH_TABLE_NAME" $LOG_FILE
    _logger "-------------------------------" $LOG_FILE

#     . $SQL_FILE

#     ${DATABASE_CLIENT_COMMAND} --user ${DATABASE_ETL_USERNAME} \
#                            --password=${DATABASE_ETL_PASSWORD} \
#                             >$RESULT_FILE 2>$ERROR_FILE \
#                             --unbuffered --silent --skip-column-names \
#                             -e "$SQL_GET_ALL_FIELDS"

#     STG_ALL_FIELDS=$(cat $RESULT_FILE)
#     CND_WHERE_IS_NULL=$(echo "$STG_ALL_FIELDS" | sed -e 's/,/ IS NULL or /g' -e 's/$/ IS NULL/g')
#     CND_WHERE_IS_BLANK=$(echo "$STG_ALL_FIELDS" | sed -e 's/^/(/g' -e 's/,/),(/g' -e 's/$/)/g' | sed -e 's/(/LENGTH(/g' | sed -e 's/),/) = 0 or /g' -e 's/$/ = 0/')

#     CND_WHERE="$CND_WHERE_IS_NULL or "$CND_WHERE_IS_BLANK

#     . $SQL_FILE

#     ${DATABASE_CLIENT_COMMAND} --user ${DATABASE_ETL_USERNAME} \
#                            --password=${DATABASE_ETL_PASSWORD} \
#                             >$RESULT_FILE 2>$ERROR_FILE \
#                             --unbuffered -t \
#     <<EOF                           
#     $SQL_SELECT_DATABASE
#     \! echo "------------------------"
#     \! echo "NUMBER COLUMNS"
#     \! echo "------------------------"
#     $SQL_COUNT_COLUMNS
#     \! echo "------------------------"
#     $SQL_COUNT_ROW
#     \! echo "------------------------"
#     \! echo "SHOW RECORD PER SOURCE"
#     \! echo "------------------------"    
#     $SQL_VERIFY_COUNT_ROW_STG
#     \! echo "------------------------"    
#     \! echo "RECORD NULL or BLANK"
#     \! echo "------------------------"        
#     $SQL_VERIFY_VALUE_NULL
#     \! echo "------------------------"
#     \! echo "ALL ROW DUPLICATE"
#     \! echo "------------------------"    
#     $SQL_VERIFY_DUP_ROW_STG
#     \q
# EOF

#     cat $RESULT_FILE

#     # remove charecter [space, tab]
#     sed 's/=[\t]/ = /g' <$RESULT_FILE >$RESULT_FILE.tmp
#     mv $RESULT_FILE.tmp $RESULT_FILE
    
#     #. $RESULT_FILE
#     # cat $RESULT_FILE
#     #_fileToLogger $RESULT_FILE

#     _fileToLogger $RESULT_FILE $LOG_FILE

#     #_logger "TOTAL_RECORD = $TOTAL_RECORD" $LOG_FILE
#     #_logger "TOTAL_COLUMNS = $TOTAL_COLUMNS" $LOG_FILE
#     _logger "-------------------------------" $LOG_FILE

#     _logger "Copy File Out $RESULT_FILE to Test File : $TEST_FILE" $LOG_FILE
#     cp $RESULT_FILE $TEST_FILE

fi

#--------------------------------------
# Summary
#--------------------------------------
ELAPSED=$(_calcElapsedTime $START_TIME)

_logger "Elapsed $ELAPSED sec." $LOG_FILE
_logger "STATUS=$ST_CODE $ST_NAME" $LOG_FILE
_logger "END" $LOG_FILE
echo " " >> $LOG_FILE

#--------------------------------------
# Verify result command for support Job Schduler
#--------------------------------------
#Force run profile for support Job Schduler
# if [ $ST_CODE -eq $ST_RECONCILE_REC_ERROR -o $ST_CODE -eq $ST_RECONCILE_SUM_ERROR ]
# then
#   exit $ST_COMPLETE
# else
#   exit $ST_CODE
# fi

exit $ST_CODE
