package com.gable.main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;


public class SLIP {
	
	
	
	
	public static void main(String[] args) throws Throwable {	
		
		int setnum_index=0;
		
		List<String> myList = new ArrayList<>();
		HashMap<String, String> dataHashMap = new HashMap<String,String>();
		String[] PDFInputPathData = new String[] {
				
				"D:\\Workspaces\\PDF_Converter\\Pay_Slip"
		};
		 
		String[] PDFInputFileNameData = new String[] {				
//				"HL-EPayslip_may19_20190522.pdf",
//				"HL-EPayslip_Jun19_V1_20190620.pdf",
//				"HL-EPayslip_Jul19_20190712.pdf",
//				"HL-EPayslip_Aug19_20190815.pdf",
//				"HL-EPayslip_Sep19_339Emps_20190913.pdf",
//				"HL-EPayslip_Oct19_336Emps_20191024.pdf",
//				"HL-EPayslip_Nov19_332Emps_20191124.pdf",
//				"HL-EPayslip_Dec19_20191224.pdf"
				
				"HL-payslip_may19_20190522.pdf",
				"HL-payslip_may19_3emp_20190524.pdf",
				"HL-payslip_may19_69emp_OT3_20190524.pdf",
				"HL-payslip_may19_69emp_OT3_20190524_v2.pdf",
				"HL-payslip_may19_69emp_OT3_20190524_v3.pdf",
				"HL-payslip_may19_73emp_OT3_20190524_v3.pdf",
				"HL-EPayslip_Jun19_V1_20190620.pdf",
				"HL-EPayslip_340Emp_Jul19_20190712.pdf",
				"HL-EPayslip_340Emp_Aug19_20190815.pdf",
				"HL-Payslip_09_Sep19_339Emps_20190913.pdf",
				"HL-Payslip_10_Oct19_336Emps_20191024.pdf",
				"HL-Payslip_6Emps_10_Oct19-P2_20191031.pdf",
				"HL-Payslip_11_Nov19_332Emps_20191124.pdf",				
				"HL-EPayslip_Dec19_20191224.pdf",
				"ADC-Payslip_4Emps_Expat_SF_12_Dec19_20191224.pdf",
				"Payslip_10960017_201912.pdf",
				"ADC-Payslip_3Emps_Expat_NoSF_12_Dec19_20191224.pdf",				
				"ADC-Payslip_11Emps_Contract_NoSF_12_Dec19_20191224.pdf",
				"ADC-Payslip_704Emps_12_Dec19_20191224.pdf",
				"Payslip_10962044_201912.pdf"
		};
		
		String[] FileMonthData = new String[] {				
//				"HL-EPayslip_may19_20190522.pdf",
//				"HL-EPayslip_Jun19_V1_20190620.pdf",
//				"HL-EPayslip_Jul19_20190712.pdf",
//				"HL-EPayslip_Aug19_20190815.pdf",
//				"HL-EPayslip_Sep19_339Emps_20190913.pdf",
//				"HL-EPayslip_Oct19_336Emps_20191024.pdf",
//				"HL-EPayslip_Nov19_332Emps_20191124.pdf",
//				"HL-EPayslip_Dec19_20191224.pdf"
				
				"05",
				"05",
				"05",
				"05",
				"05",
				"05",
				"06",
				"07",
				"08",
				"09",
				"10",
				"10",
				"11",				
				"12",
				"12",
				"12",
				"12",				
				"12",
				"12",
				"12"
				
		};
		

		
		String[] CompanyData = new String[] {
//				"����ѷ ͹ѹ�� ������ͻ��鹷� ",	
				"���ԡ�� ", 
				
//		
		};
		
		
		String key_str1="��������";
		String key_str1_en="Total Income";
		
		String key_str2="�������ѡ";
		String key_str2_en="Total Deduction"; 
		
		String key_str3 ="����-���ʡ��";
		String key_str3_en = "Employee Name"; 
		
		String key_str4="�Թ���ط��";
		String key_str4_en="Net Income";
		
		String key_str5="�����ѡ � ������";
		String key_str5_en="Tax";
		
		int tot_process_rec_num=0;
		
		int tot_output_rec_num=0; 
		
//		HashMap<String, String> empTHashMap = new HashMap<String,String>();
		
		for (String PDFInputFileName : PDFInputFileNameData) {		
			
			String PDFInputFile = PDFInputPathData[0]+"\\"+PDFInputFileName;
			
			System.out.println(setnum_index+"=> Processing for file =>"+PDFInputFile);
			int process_rec_num=0;
			File PDFFile = new File(PDFInputFile);			
			if (PDFFile.exists()) {
				PDDocument document = PDDocument.load(PDFFile);
				PDFTextStripper textReader = new PDFTextStripper();
				
	//			System.out.println("" + document.getNumberOfPages());
				int FilePageNum = document.getNumberOfPages();
	//			FilePath = PDFInputFile;
				
	//			if(FilePageNum >= csvData.getRecordNum()) {
					int datRec = 0;
					
					for (int i=1;i<=FilePageNum;i++) {
						PDDocument tempDocument = new PDDocument();
						datRec++;
						tempDocument.addPage(document.getPage(i - 1));
						
//						String PayDate = PayDateData[setnum_index];
							
							String pageText = textReader.getText(tempDocument);
							
							tempDocument.close();
//							System.out.println(pageText);
							String[] pageTextArrayed = pageText.split("\r\n");
							int strNum=0;						
							for (String str : pageTextArrayed) {							
	//							System.out.println(strNum+"=>["+str.replace("\\n","").replace("\\r", "").replace("[\\u0E00-\\u0E7F]","")+"]");
//								System.out.println(strNum+"=>["+str+"]");
//								if (str.length()>=1 && str.length()<=4) {
								if(str.contains(key_str3) || str.equalsIgnoreCase(key_str3_en)) {
									
									
	//								String regex = ".*([0-9]).*";
	//								System.out.println("str=>"+str+"=>"+str.matches(regex));
//									if (isNumeric(pageTextArrayed[strNum+1])) {
										String emp_dat = pageTextArrayed[strNum+8];
										String[] emp_dat_arr = emp_dat.split(" ");
										String seq_no = emp_dat_arr[0];
										String key_dat = seq_no+"_"+FileMonthData[setnum_index];
//										if(seq_no.equals("10962015"))
//											System.out.println(pageText);										
										String emp_name = pageTextArrayed[strNum+3];
										String income_mny = "";
										String deduct_mny = "";
										String total_mny = "";
										String tax_mny = "";
										boolean complete_chk = false;
										for(int info_ind=1;info_ind<(pageTextArrayed.length-strNum);info_ind++) {
											
											if(pageTextArrayed[strNum+info_ind].contains(key_str1) || pageTextArrayed[strNum+info_ind].equalsIgnoreCase(key_str1_en)) {												
												for(int data_ind=2;data_ind<(pageTextArrayed.length-strNum-info_ind);data_ind++) {													
													if(!(isNumeric(pageTextArrayed[strNum+info_ind+data_ind].replace(",", "")))) {
														income_mny = pageTextArrayed[strNum+info_ind+data_ind-1].replace(",", "");
//														complete_chk=true;
														break;
													}
												}
											}
											
											if(pageTextArrayed[strNum+info_ind].contains(key_str2) || pageTextArrayed[strNum+info_ind].equalsIgnoreCase(key_str2_en)) {												
												for(int data_ind=2;data_ind<(pageTextArrayed.length-strNum-info_ind);data_ind++) {													
													if(!(isNumeric(pageTextArrayed[strNum+info_ind+data_ind].replace(",", "")))) {
														deduct_mny = pageTextArrayed[strNum+info_ind+data_ind-2].replace(",", "");
//														complete_chk=true;
														break;
													}
												}
											}
											
											if(pageTextArrayed[strNum+info_ind].contains(key_str4) || pageTextArrayed[strNum+info_ind].equalsIgnoreCase(key_str4_en)) {												
												for(int data_ind=1;data_ind<(pageTextArrayed.length-strNum-info_ind);data_ind++) {													
													if(!(isNumeric(pageTextArrayed[strNum+info_ind+data_ind].replace(",", "")))) {
														total_mny = pageTextArrayed[strNum+info_ind+data_ind-1].replace(",", "").replace("�Թ���ط�� ","");
														complete_chk=true;
														break;
													}
												}
											}										
											
											if (complete_chk) break;
											
										}
										

//										String dat_str = PDFInputFileName+","+CompanyData[0]+","+seq_no+","+emp_name+","+income_mny+","+deduct_mny+","+total_mny;
										String dat_str = key_dat+","+FileMonthData[setnum_index]+","+PDFInputFileName+","+seq_no+","+emp_name+","+income_mny+","+deduct_mny+","+total_mny;
										System.out.println(dat_str);
//										myList.add(dat_str);
										
										if(!(dataHashMap.containsKey(key_dat))) {
											dataHashMap.put(key_dat, dat_str);
											myList.add(dat_str);
											
											tot_output_rec_num++;
								        }
										else {
											int output_dat_ind = 0;
											for (String output_dat : myList) {
												String[] output_dat_arr = output_dat.split(",");
												if(output_dat_arr[0].equals(key_dat)) {
													myList.set(output_dat_ind, dat_str);
													break;
												}
												output_dat_ind++;
											}
										}
										tot_process_rec_num++;
										process_rec_num++;
										
//									}
								}					
								strNum++;
							}	
					}// end for
				document.close();
				
				System.out.println(setnum_index+"=> Process done total record=>"+process_rec_num+", FilePageNum=>"+FilePageNum);
			}
			else {
				System.err.println("File not Found !");
			}
						
			setnum_index++;
		}//end PDFInputFileData
		
		System.out.println("Generating Result...");
		GenerateResult(myList);
		System.out.println("Generating Result Done");
		System.out.println("Total File =>"+setnum_index);
		System.out.println("Total Record =>"+tot_output_rec_num);
		System.out.println("Processed Record =>"+tot_process_rec_num);
		
	}
	
	public static boolean isNumeric(String strNum) {
		
		if (strNum == null)
			return false;
			
		try {
			
//			Double d = Double.parseDouble(strNum);
			
//			String txt="XXX_$(GetDateValue:d)_YYY";
			//
					String re1=".*([0-9]).*";	// Non-greedy match on filler
//				    String re2="(\\$\\(.*\\))";	// Variable Name 1
			//		^\d{1,}(,\d+)?\.\d+$
			//
				    String regDat = re1;
//				    System.out.println(regDat);
				    Pattern p = Pattern.compile(regDat,Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
				    Matcher m = p.matcher(strNum);
				    if (m.find())
				    {
				        String var1=m.group(1);				  
//				        System.out.println(var1);
				        return true;
				  
//				        String var2=m.group(3);
//				        String var3=m.group(4);
//				        System.out.print("("+word1.toString()+")"+"("+var1.toString()+")"+"("+var2.toString()+")"+"("+var3.toString()+")"+"\n");
				    }
				    
			
		}
		catch(Exception ex) {
			return false;
		}
		
		return false;
	}
	
	public static void GenerateResult(List<String> dataList) throws IOException, Exception {		
		
		String CSVOutputFile = "D:\\Workspaces\\PDF_Converter\\SLIP.txt";
		int datRec = 0;		
		BufferedWriter bw = null;
		FileWriter fw = null;
		
		try {
			fw = new FileWriter(CSVOutputFile);
			bw = new BufferedWriter(fw);	
//			bw.write(outputHeader);
//			bw.newLine();
			for (String dat : dataList) {				
				datRec++;
				bw.write(dat);
				bw.newLine();
			}// end for
			
//			for (String dat : dataList) {				
//				datRec++;
//				String val = dataHashMap.get(dat);
//				bw.write(val);
//				bw.newLine();
//			}// end for

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (bw != null)
				bw.close();
			if (fw != null)
				fw.close();
		}		
		
		
	}
	


}
