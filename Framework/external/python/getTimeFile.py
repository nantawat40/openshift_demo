import os
import platform
from datetime import datetime

def creation_date(path_to_file):
    if platform.system() == 'Windows':
        time = os.path.getmtime(path_to_file)
        return datetime.fromtimestamp(time).strftime('%Y-%m-%d %H:%M:%S')  
    else:
        stat = os.stat(path_to_file)
        try:
            return stat.st_birthtime
        except AttributeError:
            # We're probably on Linux. No easy way to get creation dates here,
            # so we'll settle for when its content was last modified.
            return stat.st_mtime 
