#!/bin/bash

#--------------------------------------------------------------------
#              Copyright C - 2012 All rights reserved
#                    Hewlett Package Thailand.
#
#       No part of this program may be reproduced or adapted in any
#             form or by any means, electronic or mechanical,
#       without permission from Hewlett Package Thailand.
#          This program is confidential and may not be disclosed,
#          decompiled or reverse engineered without permission in
#             writing from Hewlett Package Thailand.
#
#--------------------------------------------------------------------
# DESCRIPTION:
# Transform Staging to Data Warehouse (STG -> DWH)
# Monthly Period Dimension
#--------------------------------------------------------------------
# Revision   Date        CR No.   Author         Description
#====================================================================
#  0.2       16/01/2020  XXX      Suebsakul.A    Project Push to SEE, Your DATA
#
#--------------------------------------------------------------------

# Parameters
# -s <YYYYMM> : Start of period
# -e <YYYYMM> : End of period

#ETL_HOME=/opt/dwa/dev/apps
CONF_PATH=$ETL_HOME/config
EXEC_NAME='dwhmonthlyperiod.sh'
EXEC_PARA='-s <YYYYMM> -e <YYYYMM>'
STAGE='dwh'
DEST_TABLE="MONTHLY_PERIOD_DIM"

# Load global variables and function library
GLOBAL_LIB_CONFIG=$HOME/Framework/configs/lib/global.inc
. $GLOBAL_LIB_CONFIG

# Load global variables and function library
. $MAIN_CONFIG

if [ $# -ne 4 ]
then
  _usage
else
  let i=1
  for p in $*;
  do
    case "$p" in
      "-s")
        START_DATE=${@:$((i+1)):1}
      ;;
      "-e")
        END_DATE=${@:$((i+1)):1}
      ;;
      *)
        if [ $((i%2)) -eq 1 ]
        then
          _usage
        fi
      ;;
    esac
    let i=i+1
  done;
fi

# Validate Start Date & End Date
if [ -z $START_DATE -o -z $END_DATE ]
then
  _usage
fi

_chkDateMonth "$START_DATE"
_chkDateMonth "$END_DATE"

# Ensure that End Date is greater or equal than Start Date
if [ $START_DATE -gt $END_DATE ]
then
  echo "Invalid date: End date is less than start date"
  exit $ST_UNEXPECTED_ERROR
fi

# Additional Status
ST_OVERLAP_PERIOD=1
ST_OVERLAP_PERIOD_NAME='Error overlap period'

set -o pipefail

LOG_PATH=$LOG_PATH/$STAGE

if [ ! -d $LOG_PATH ]
then
  mkdir $LOG_PATH
fi

LOG_FILE="$LOG_PATH/dwhmonthlyperiod.log"

START_TIME="$(date +%s%N)"
#START_TIME="${SECONDS}"
FORM_START_TIME=`date +"%b %d %T"`

_logger "Started" $LOG_FILE

# Declarations
SQL_FILE=$CONF_PATH/sql/$STAGE/dwhperiod.sql
RESULT_FILE=$LOG_PATH/$START_DATE$END_DATE.out
ERROR_FILE=$LOG_PATH/$START_DATE$END_DATE.err
#EMAIL_FILE=$LOG_PATH/$START_DATE$END_DATE.mail

#--------------------------------------
# STEP 1. Load Configuration
#--------------------------------------

# 1.2 SQL Configure
if [ -s $SQL_FILE ]
then
  . $SQL_FILE
else
  _logger "Unable to load SQL Configuration file: $SQL_FILE" $LOG_FILE
  exit $ST_UNEXPECTED_ERROR
fi

#--------------------------------------
# STEP 2. Verify overlap period
#--------------------------------------
# ${DATABASE_CLIENT_COMMAND} --user ${DATABASE_ETL_USERNAME} \
#                        --password=${DATABASE_ETL_PASSWORD} \
#                         >$RESULT_FILE 2>$ERROR_FILE \
#                         --unbuffered -q \
# <<EOF
# \!echo "OVERLAP_RECORD="  | tr -d '\n'
# $SQL_CHK_OVERLAP_MNTLY
# SELECT ROW_COUNT();
# \q
# EOF

# if [ $? -ne 0 ]
# then
#   _logError $ERROR_FILE $LOG_FILE
#   exit $ST_UNEXPECTED_ERROR
# else
  
#     perl -p -e 's/=\n/=/g' $RESULT_FILE > file.tmp
#     mv file.tmp $RESULT_FILE

#   . $RESULT_FILE
#   _logger "Overlap Record: $OVERLAP_RECORD record(s)" $LOG_FILE

#   if [ $OVERLAP_RECORD -gt 0 ]
#   then
#     # When found overlap record
#     ST_CODE=$ST_OVERLAP_PERIOD
#     ST_NAME=$ST_OVERLAP_PERIOD_NAME
#   fi

# fi


#--------------------------------------
# STEP 3. Generate Period
#--------------------------------------

if [ $ST_CODE -eq $ST_COMPLETE ]
then

  let TOTAL_GENERATE_PERIOD=0
  TMP_SQL_FILE="$LOG_PATH/monthlyperiod.sql"

  echo "$SQL_INSERT_PERIOD_MNTLY_DEF" > $TMP_SQL_FILE

  _logger "Generating Monthly Period from $START_DATE to $END_DATE..." $LOG_FILE

  START_YYYY=${START_DATE:0:4}
  START_MM=${START_DATE:4:2}
  END_YYYY=${END_DATE:0:4}
  END_MM=${END_DATE:4:2}
  TMP_START_MM=$START_MM

  for (( y=$START_YYYY; y<=$END_YYYY; y++)) #>
  do

    if [ $y -eq $END_YYYY ]
    then
      TMP_END_MM=$END_MM
    else
      TMP_END_MM=12
    fi
  
    for (( m=$TMP_START_MM; m<=$TMP_END_MM; m++)) #>
    do

	if [ $m -lt 10 ]
        then 
          mm="0$m"
        else
          mm=$m
        fi
       
        #echo "$y-$mm"
        SQL=$(_replace "$SQL_INSERT_PERIOD_MNTLY_BODY" ":1" "$y$mm")
        echo "$SQL" >> $TMP_SQL_FILE
      
        if [ $END_DATE != "$y$mm" ]
        then
          echo "UNION ALL" >> $TMP_SQL_FILE
        else
          echo ";" >> $TMP_SQL_FILE
          # echo "$SQL_COMMIT" >> $TMP_SQL_FILE
        fi

    done # End of loop months

    TMP_START_MM=1

  done # End of loop years


  # cat $TMP_SQL_FILE

  # Generate Monthly Period to DB
  # $VSQL_PATH -h $DB_HOST -U $DB_USER -w $DB_PASS -Atq << EOF >$RESULT_FILE 2>$ERROR_FILE
  # \!echo "REC_CNT="
  # \i $TMP_SQL_FILE
  # \q
  # EOF

  SQL_INSERT_RECORD=$(cat $TMP_SQL_FILE)

  # switch off Exit immediately if a command exits with a non-zero status
  set +e

  ${DATABASE_CLIENT_COMMAND} --user ${DATABASE_ETL_USERNAME} \
                        --password=${DATABASE_ETL_PASSWORD} \
                          >$RESULT_FILE 2>$ERROR_FILE \
                          --unbuffered -q \
  <<EOF
  $SQL_SELECT_DATABASE
  $SQL_TRUNCATE_PERIOD_MNTLY
  \! echo "INSERT_NEW_RECORD=" | tr -d '\n'
  $SQL_INSERT_RECORD
  SELECT ROW_COUNT();
  \q
EOF

  RESULT=$?

  # switch on Exit immediately if a command exits with a non-zero status
  set -e

  if [ $RESULT -eq 0 ]
  then
    # Remove string
    sed -i 's/ROW_COUNT()//g'    $RESULT_FILE

    # Remove newline
    sed -i ':a;N;$!ba;s/=\n/=/g' $RESULT_FILE

    # perl -p -e 's/=\n/=/g' $RESULT_FILE > file.tmp
    # mv file.tmp $RESULT_FILE

    . $RESULT_FILE
    _logger "Total generated period: $INSERT_NEW_RECORD month(s)" $LOG_FILE
  else
    _logError $ERROR_FILE $LOG_FILE
    ST_CODE=$ST_UNEXPECTED_ERROR
    ST_NAME=$ST_UNEXPECTED_ERROR_NAME
  fi

fi


#--------------------------------------
# Summary
#--------------------------------------
ELAPSED=$(_calcElapsedTime $START_TIME)

_logger "Elapsed $ELAPSED sec." $LOG_FILE
_logger "STATUS=$ST_CODE $ST_NAME" $LOG_FILE
_logger "END" $LOG_FILE
echo " " >> $LOG_FILE

exit $ST_CODE

