SELECT
*
FROM DATA_TESTING_STG.STG_DWH_MAPPING_STG_V1
WHERE DWH_TABLE = 'PEOPLE_HIST_MASTER' AND KEYTYPE IN ('PK','UK')

select PEOPLE_HIST_PK,EMPLOYEE_ID, PEOPLE_HIST_START_DATE, PEOPLE_HIST_END_DATE from DATA_TESTING_DWH.PEOPLE_HIST_MASTER

insert into DATA_TESTING_DWH.tttt (EMPLOYEE_ID,NAME)
select 'AAAAA1','BBBBB'
union all
select 'AAAAA2','BBBBB'


delete from DATA_TESTING_DWH.PEOPLE_HIST_MASTER;

select * from  DATA_TESTING_STG.STG_PEOPLE;

select * from  DATA_TESTING_DWH.PEOPLE_HIST_MASTER;

update DATA_TESTING_DWH.PEOPLE_HIST_MASTER set PEOPLE_HIST_END_DATE = '9999-12-31 23:59:59' where EMPLOYEE_ID='EM0001';

select * from DATA_TESTING_DWH.PEOPLE_HIST_MASTER order by 2;

-- 1 update end date = today in case have new record
UPDATE DATA_TESTING_DWH.PEOPLE_HIST_MASTER AS DWH_DES
INNER JOIN (
			select 
				A.EMPLOYEE_ID,
				A.PEOPLE_HIST_END_DATE, 
				B.STG_SOURCE_DT 
			from DATA_TESTING_DWH.PEOPLE_HIST_MASTER A
			JOIN DATA_TESTING_DWH.PEOPLE_HIST_MASTER_CLASSIFICATION B on B.EMPLOYEE_ID = A.EMPLOYEE_ID
			where EXISTS(
					select 1
					from
					(
						select
							EMPLOYEE_ID
						from DATA_TESTING_DWH.PEOPLE_HIST_MASTER_CLASSIFICATION A
						where EXISTS(
								select 1 from  DATA_TESTING_DWH.PEOPLE_HIST_MASTER B
								where 	B.EMPLOYEE_ID = A.EMPLOYEE_ID AND			   
										A.STG_SOURCE_DT > B.STG_SOURCE_DT AND
									   (
									   		-- Not same on CDC but Cast by follow as DataType
									   		B.DATE_OF_BIRTH <> CAST(A.DATE_OF_BIRTH AS DATE) OR
									    	B.DATE_JOINED_COMPANY <> CAST(A.DATE_JOINED_COMPANY AS DATE) OR
									    	B.DATE_LEFT_COMPANY   <> CAST(A.DATE_LEFT_COMPANY AS DATE) OR
									    	B.GENDER_MFU <> A.GENDER_MFU OR
									    	B.FIRST_NAME <> A.FIRST_NAME OR
									    	B.MIDDLE_NAME <> A.MIDDLE_NAME OR
									    	B.LAST_NAME <> A.LAST_NAME
									   )
								)
					) T
					WHERE T.EMPLOYEE_ID = A.EMPLOYEE_ID AND
					      A.PEOPLE_HIST_END_DATE > CURRENT_DATE
			)
) AS STG ON STG.EMPLOYEE_ID = DWH_DES.EMPLOYEE_ID and STG.PEOPLE_HIST_END_DATE = DWH_DES.PEOPLE_HIST_END_DATE
SET
	-- Need modify value assign is STG_SOURCE_DT of new record
	DWH_DES.PEOPLE_HIST_END_DATE = DATE_ADD(CAST(STG.STG_SOURCE_DT AS DATE), INTERVAL -1 SECOND),
	DWH_DES.LAST_MODIFIED_DT     = current_timestamp();


-- 2 insert record new
insert into DATA_TESTING_DWH.PEOPLE_HIST_MASTER
(EMPLOYEE_ID, DATE_OF_BIRTH, DATE_JOINED_COMPANY, DATE_LEFT_COMPANY, GENDER_MFU, JOB_TITLE, FIRST_NAME, MIDDLE_NAME, LAST_NAME, OTHER_DETAILS, PEOPLE_HIST_START_DATE, PEOPLE_HIST_END_DATE, STG_SOURCE_DT, STG_SOURCE_FILE, STG_LOAD_DT)
select
	EMPLOYEE_ID,
	DATE_OF_BIRTH,
	DATE_JOINED_COMPANY,
	DATE_LEFT_COMPANY,
	GENDER_MFU,
	JOB_TITLE,
	FIRST_NAME,
	MIDDLE_NAME,
	LAST_NAME,
	OTHER_DETAILS,
	CONCAT(CAST(STG_SOURCE_DT as DATE),' 00:00:00') PEOPLE_HIST_START_DATE,
	'9999-12-31 23:59:59' PEOPLE_HIST_END_DATE,
	STG_SOURCE_DT,
	STG_SOURCE_FILE,
	STG_LOAD_DT
from DATA_TESTING_DWH.PEOPLE_HIST_MASTER_CLASSIFICATION A
where EXISTS(
		select 1 from  DATA_TESTING_DWH.PEOPLE_HIST_MASTER B
		where 	B.EMPLOYEE_ID = A.EMPLOYEE_ID AND			   
				A.STG_SOURCE_DT > B.STG_SOURCE_DT AND
			   (
			   		-- Not same on CDC but Cast by follow as DataType
			   		B.DATE_OF_BIRTH <> CAST(A.DATE_OF_BIRTH AS DATE) OR
			    	B.DATE_JOINED_COMPANY <> CAST(A.DATE_JOINED_COMPANY AS DATE) OR
			    	B.DATE_LEFT_COMPANY   <> CAST(A.DATE_LEFT_COMPANY AS DATE) OR
			    	B.GENDER_MFU <> A.GENDER_MFU OR
			    	B.FIRST_NAME <> A.FIRST_NAME OR
			    	B.MIDDLE_NAME <> A.MIDDLE_NAME OR
			    	B.LAST_NAME <> A.LAST_NAME
			   )
		);
		
	