-- THIS FILE Generate by G-Able Framework
-- COMMENT Generate by G-Able Framework @2020-01-29 12:07:50

-- SELECTED DATABASE
USE EMPLOYEE_TESTING_STG;

-- DROP TABLE IF FOUND
DROP TABLE IF EXISTS `STG_DWH_MAPPING_STG_V1`;

-- CREATE TABLE STG
CREATE TABLE `STG_DWH_MAPPING_STG_V1` (
	GROUP_NAME 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',
	DWH_TYPE 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',
	DWH_NO 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',
	DWH_TABLE 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',
	DWH_FIELDS 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',
	KEYTYPE 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',
	IDENTIFY 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',
	DEFAULT_VALUE 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',
	DATATYPE 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',
	STG_TABLE 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',
	STG_FIELDS 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',
	REF_TABLE 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',
	REG_FIELDS 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',
	DESCRIPTION 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',


    -- Standard Field by G-Able Framework
    STG_SOURCE_DT       VARCHAR(128)   DEFAULT NULL  COMMENT 'Generate this field by G-Able Framework for DateTime on Filename of Source',
    STG_SOURCE_FILE     VARCHAR(128)   DEFAULT NULL  COMMENT 'Generate this field by G-Able Framework for Filename of Source',
    STG_LOAD_DT     DATETIME       DEFAULT NOW() COMMENT 'Generate this field by G-Able Framework for DateTime Stage load'
) 
CHARACTER SET UTF8 
COLLATE UTF8_ICELANDIC_CI;
