#include <stdio.h>
#include <regex.h>
#include <yaml.h>
#include <ctype.h>

#include "yaml_conf.h"


// enum YAML_field {filename = 0, md5sum = 0}; 

void level(int level)
{
  while ((--level)>0)
  {
    #ifdef DEBUG
    printf("-");
    #endif
    // printf("\t",level);
  } 
}

_Bool IsNumeric(char* String)
{
   char *ptr = String;
 
   while(*ptr && isdigit(*ptr))
      ptr++;
 
   return (*ptr ? ST_CODE_FALSE : ST_CODE_SUCCESS);
}

int pattern_regular(const char *reg_rule, int maxGroups, char *szInput, char *szre_subMatch)
{
	regex_t regex;
	regmatch_t match[10];

	char msgbuf[100];
	int start;
  int finish;

	int iret;
  int istatus = REG_NOMATCH;

	/* Compile regular expression */
	// reti = regcomp(&regex, "^a[[:alnum:]]", 0);
	iret = regcomp(&regex, reg_rule, REG_EXTENDED);
	if (iret) {
		fprintf(stderr, "Could not compile regex\n");
		exit(EXIT_FAILURE);
	}

	while (1) {
		/* Execute regular expression */
		iret = regexec(&regex, szInput, maxGroups, match, 0);
		if (!iret) 
    {

      #ifdef DEBUG
      printf("Match\n");			
      #endif
			
			for (int g = 0; g < maxGroups; g++)
			{
				if (match[g].rm_so == -1) {
					break;
				}

				start  = match[g].rm_so + (szInput - szInput);
            	finish = match[g].rm_eo + (szInput - szInput);

				// printf("[%d] re_nsub [%ld] sub match [%d]->[%d] (bytes %d:%d)\n"
				//                                                  ,g
				//                                                  ,regex.re_nsub
				// 												 ,match[g].rm_so
				// 												 ,match[g].rm_eo-match[g].rm_so
				// 												 ,start
				// 												 ,finish);

				#ifdef DEBUG
				printf ("Match [%.*s]\n",(finish - start),szInput + start);				
				#endif

				sprintf(szre_subMatch,"%.*s",(finish - start),szInput + start);

        istatus = ST_CODE_SUCCESS;

				// printf ("nsub(%d) '%.*s' (bytes %d:%d)\n",g
				//                                 ,(finish - start)
				//                                 ,szInput + start
				// 								,start
				// 								,finish);
			}
		}
		else if (iret == REG_NOMATCH) {
			#ifdef DEBUG
			printf("No match\n");
			#endif
			break;
		}
		else {
			regerror(iret, &regex, msgbuf, sizeof(msgbuf));
			fprintf(stderr, "Regex match failed: %s\n", msgbuf);
			exit(EXIT_FAILURE);
		}
		szInput += match[0].rm_eo;
	}

	/* Free memory allocated to the pattern buffer by regcomp() */
	regfree(&regex);

	return(istatus == REG_NOMATCH ? REG_NOMATCH : ST_CODE_SUCCESS);
}

/***********************************************************************
* Function : int Str2Int(char *buf)
* Argument : buf  = String Integer
* 	   :     Out = Integer Output
* Description : Use converted string double hex to vartiable double 
* Updated : 25/05/2012
************************************************************************/
int Str2Int(char *szbuf,int *out)
{
	int temp;

	if (IsNumeric(szbuf) == ST_CODE_FALSE) {
    fprintf(stderr,"Error: szbuf = %s isn't number\n",szbuf);
		exit(ST_CODE_FALSE);
	} 
	else{
		temp = (int)atoi(szbuf);
		*out = temp;

		#ifdef DEBUG
		//show value of pointer
		printf("out [%d]\n",*out);
		#endif

		return ST_CODE_SUCCESS;
	}

	return ST_CODE_FALSE;
}

void diplay_record_structure(struct gable_conf *YAML_Conf)
{
  printf("------------------------------------------------------------------------\n");
  printf("filename : %s\n",YAML_Conf->filename);
  printf("md5sum : %s\n",YAML_Conf->md5sum);
  printf("original_header : %s\n",YAML_Conf->original_header);
  printf("line : %s\n",YAML_Conf->line);  

  printf("number of field : %d\n",YAML_Conf->field_no);
  printf("------------------------------------------------------------------------\n");
  printf("record_length : %d\n",YAML_Conf->record_length);
  printf("------------------------------------------------------------------------\n");
  for(int i=0;i<YAML_Conf->field_no;i++)
  {
    printf("No : %3d, Name: %20s, Type: [%20s], Offset: [%5d], Description: %s\n",i+1
                                                        ,YAML_Conf->data[i].name
                                                        ,YAML_Conf->data[i].type
                                                        ,YAML_Conf->data[i].offset
                                                        ,YAML_Conf->data[i].description);
    // sleep(1);
  }

  printf("------------------------------------------------------------------------\n");
}

int trim(char * strIn, char  *strOut)
{
    int i_strIn = strlen(strIn);
    int i_first = 0;
    int i_last = 0;

    // memset(strOut,0x0,sizeof(strOut));
    for (int i=0;i<i_strIn;i++)
    {
      if (strIn[i] != ' ')
      {
        i_first=i;
        break;
      }
    }

    for (int i=i_strIn;i>0;i--)
    {
      if (strIn[i] != ' ')
      {
        i_last=i;      
        break;
      }        
    }

    
    strncpy(strOut,strIn+i_first,i_last);
    strOut[i_last]='\0';

    #ifdef DEBUG
    printf("[strIn=%s\n",strIn);
    printf("[strOu=%s, %d -> %d\n",strOut,i_first,i_last);
    #endif


    return(0);
}

int yaml_decode_file(char *FILE_YAML,struct gable_conf *YAML_Conf)
{
  FILE *fh = fopen(FILE_YAML, "r");
  yaml_parser_t parser;
  yaml_token_t  token;   /* new variable */
  

  // int level;
  int indent = 1;
  int previous_toktype;
  int iret;

  char previous_key[MAXBLK/4];
  char szbuff[MAXBLK/4];

  const char* szRegRule = "\\(([0-9]*)\\)";

  const int max_Loop = 500;
  int iLoop = 0;

  /* Initialize parser */
  if(!yaml_parser_initialize(&parser))
    fputs("Failed to initialize parser!\n", stderr);
  if(fh == NULL)
    fputs("Failed to open file!\n", stderr);

  /* Set input file */
  yaml_parser_set_input_file(&parser, fh);

  /* BEGIN new code */
  do {
    yaml_parser_scan(&parser, &token);
    switch(token.type)
    {
        /* Stream start/end */
        case YAML_STREAM_START_TOKEN: level(indent); 
                                      indent++; 
                                      #ifdef DEBUG
                                      puts("STREAM START"); 
                                      #endif
                                      previous_toktype=token.type; 
                                      break;
        case YAML_STREAM_END_TOKEN:   indent--; 
                                      level(indent); 
                                      #ifdef DEBUG
                                      puts("STREAM END");   
                                      #endif
                                      previous_toktype=token.type; 
                                      break;

        /* Token types (read before actual token) */
        case YAML_KEY_TOKEN:    level(indent); 
                                #ifdef DEBUG
                                printf("(Key token)   "); 
                                #endif
                                previous_toktype=token.type; 
                                break;
        case YAML_VALUE_TOKEN:  level(indent); 
                                #ifdef DEBUG
                                printf("(Value token) "); 
                                #endif
                                previous_toktype=token.type; 
                                indent++; 
                                break;

        /* Block delimeters */
        case YAML_BLOCK_SEQUENCE_START_TOKEN: level(indent); 
                                              #ifdef DEBUG
                                              puts("<b>Start Block (Sequence)</b>"); 
                                              #endif
                                              previous_toktype=token.type; 
                                              break;

        case YAML_BLOCK_ENTRY_TOKEN:          level(indent); 
                                              #ifdef DEBUG
                                              puts("<b>Start Block (Entry)</b>");
                                              #endif
                                              previous_toktype=token.type; 
                                              break;
        case YAML_BLOCK_END_TOKEN:
                                              level(indent);
                                              indent--;
                                              #ifdef DEBUG
                                              puts("<b>End block</b>");
                                              printf("\n");            
                                              #endif
                                              previous_toktype=token.type;
                                              break;

        /* Data */
        case YAML_BLOCK_MAPPING_START_TOKEN:  indent++; 
                                              level(indent); 
                                              #ifdef DEBUG
                                              puts("[Block mapping]");
                                              #endif
                                              previous_toktype=token.type; 
                                              break;
        case YAML_SCALAR_TOKEN: 

          if (previous_toktype == YAML_KEY_TOKEN)
            strcpy(previous_key,(char *)token.data.scalar.value);

          //First Level
          if (indent == 6)
          {
            if (strcmp(previous_key,"filename") == 0)
            {
              strcpy(YAML_Conf->filename,(char *)token.data.scalar.value);
            }else if (strcmp(previous_key,"md5sum") == 0)
            {
              strcpy(YAML_Conf->md5sum,(char *)token.data.scalar.value);
            }else if (strcmp(previous_key,"original_header") == 0)
            {
              strcpy(YAML_Conf->original_header,(char *)token.data.scalar.value);
            }else if (strcmp(previous_key,"line") == 0)
            {
              strcpy(YAML_Conf->line,(char *)token.data.scalar.value);
            }


          }else if (indent == 8) //Level 2
          {
            
            if (strcmp(previous_key,"name") == 0)
            {
              strcpy(YAML_Conf->data[YAML_Conf->field_no].name,(char *)token.data.scalar.value);
            }else if (strcmp(previous_key,"type") == 0)
            {
              
              // strcpy(YAML_Conf->data[YAML_Conf->field_no].type,(char *)token.data.scalar.value);
              
              trim((char *)token.data.scalar.value ,szbuff);
              // printf("szbuff [%s]\n",szbuff);
              strcpy(YAML_Conf->data[YAML_Conf->field_no].type,szbuff);

            }else if (strcmp(previous_key,"description") == 0)
            {
              strcpy(YAML_Conf->data[YAML_Conf->field_no].description,(char *)token.data.scalar.value);
            }

            if (
                (strlen(YAML_Conf->data[YAML_Conf->field_no].name) > 0) &&
                (strlen(YAML_Conf->data[YAML_Conf->field_no].type) > 0) &&
                (strlen(YAML_Conf->data[YAML_Conf->field_no].description) > 0)
              )
                YAML_Conf->field_no++;
          }

          #ifdef DEBUG
          printf("scalar %d : %s \n", indent, token.data.scalar.value);
          #endif          
          if (previous_toktype == YAML_VALUE_TOKEN)
          {
            #ifdef DEBUG
            printf("\n");
            #endif
            indent--;
          }
        break;

        /* Others */
        #ifdef DEBUG
        default:            
            printf("Got token of type %d (%d)\n", token.type, iLoop);
        #endif
    }

    if(token.type != YAML_STREAM_END_TOKEN)
        yaml_token_delete(&token);

  } while(token.type != YAML_STREAM_END_TOKEN && iLoop++ < max_Loop);
  yaml_token_delete(&token);
  /* END new code */

  /* Cleanup */
  yaml_parser_delete(&parser);
  fclose(fh);

  if (iLoop > max_Loop)
    fputs("Error process max limit loop\n", stderr);
  
  #ifdef DEBUG
  printf("Decode by regular..."); 
  #endif
	for(int j=0,pre_offset=0;j<YAML_Conf->field_no;j++)
	{
	  iret=pattern_regular(szRegRule,2,YAML_Conf->data[j].type,szbuff);

    // printf("iret = %d\n",iret);

    if (iret == ST_CODE_SUCCESS)
	    Str2Int(szbuff,&YAML_Conf->data[j].offset);
    else
      YAML_Conf->data[j].offset = 0;
  }

  return 0;
}


// int main(void)
// {
//   struct gable_conf YAML_Conf;
  
//   memset(&YAML_Conf,0x0,sizeof(struct gable_conf));
//   yaml_decode_file("fruit.yaml",&YAML_Conf);

//   diplay_record_structure(&YAML_Conf);  
//   return 0;
// }