/*
#---------------------------------------------------------------------------
#
#              Copyright C - 2010 All rights reserved
#                        G-Able Thailand.
#
#       No part of this program may be reproduced or adapted in any
#             form or by any means, electronic or mechanical,
#       without permission from G-Able Thailand.
#          This program is confidential and may not be disclosed,
#          decompiled or reverse engineered without permission in
#             writing from G-Able Thailand.
#
#--------------------------------------------------------------------
# DESCRIPTION:
#    transform source file input to framework
#
#--------------------------------------------------------------------
# Revision   Date        CR No.   Author         Description
#====================================================================
#  0.2       08/03/2020  XXX      Suebsakul.A    Project Push to SEE, Your DATA
#
#
#
#
*/

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <yaml.h>
#include <regex.h>

#include <ctype.h>

#if __linux__ 	
	#include <sys/time.h>
	#include <sys/types.h>
	#include <sys/uio.h>
#endif

#include "getopt.h"
#include "yaml_conf.h"

#define	MAXBLK 1024
#define	MAXFIELD 255
#define iY2K(year)      (pTm->tm_year<2000 ?1900+pTm->tm_year :pTm->tm_year)
#define TRACE			0	//This use for open trance log
#define VERIFY_OUTPUT	0	//This use verify output automatic but effect performance of script

#define True        			0
#define ST_CODE_SUCCESS         0
#define ST_CODE_FALSE           1


#define ST_CODE_ERR_PARAMETER  	11
#define ST_CODE_ERR_ACCESS_FILE	15
#define ST_CODE_ERR_CONVERT_OUTPUT	19


#define PATH_LND  "/root/Framework/input/lnd"
#define PATH_TMP  "/root/Framework/output/tmp/lnd"
#define PATH_OUT  "/root/Framework/input/stg"



struct gable_dynamic_field {
    char value[MAXBLK/4];
};

struct gable_record_structure {
    struct gable_dynamic_field **field;
};

//This vartiable for verify statu and prove output for converted hex double to double
int  show_verbose=1;

int print_usage(int argc,char *argv[])
{
	fprintf(stderr,"Invalid parameters: %s -f <FILE> -c <FILE.control> [ -v -s ]\n",argv[0]);
	fprintf(stderr,"\t -f FILE DATA\t\t: Input file\n");
	fprintf(stderr,"\t -c FILE CONTROL\t: input file.control\n");
	fprintf(stderr,"\t -v Verbose for show stat to standard error\n");
	fprintf(stderr,"\t -s Show Summery but only field's type double\n");
	fprintf(stderr,"e.g. %s -f Employee.csv -c Employee.control\n",argv[0]);
	exit(ST_CODE_ERR_PARAMETER);
}

/***********************************************************************
* Function : GetTokenStr(char  *strIn,char *cDelim,int iPos,char  *strOut)
* Argument : strIn  = String Input
* 	   : cDelim = Delimiter
* 	   : iPos   = Position
* 	   : strOut = String Output
* Description : Use for Get Token String 
* Updated : 15/01/2003
************************************************************************/
int  GetTokenStr(char  *strIn,char *cDelim,int iPos,char  *strOut)
{
	char szBuff[2048];
	char szBuffTok[2048];
	char *szTok=szBuffTok;
	int  i;

	memset(szBuffTok,0,256);
	strcpy(szBuff,strIn);

	if(iPos==1)
	{
		szTok=strtok(szBuff,cDelim);
	}
	else
	{
		szTok=strtok(szBuff,cDelim);

		for(i=1;i<iPos;i++)
		{
			szTok=strtok(NULL,cDelim);
		}
	}
	
	if (szTok != NULL)
		strcpy(strOut,szTok);
	else
		strOut[0] = '\0';


	return(ST_CODE_SUCCESS);

}

int iWriteLogProgram(char *szLogPath,char *szProgram,char *szMesg)
{
	struct tm *pTm;
	struct timeval tv;
	struct timezone tz;
	char  szBuffMesg[1024];
	char  szMesgDate[128];

	gettimeofday(&tv, &tz);

	//time(&tt);
	gettimeofday(&tv, &tz);
	pTm=localtime(&tv.tv_sec);

	gettimeofday(&tv, &tz);
	pTm=localtime(&tv.tv_sec);
	sprintf( szMesgDate, "%02u/%02u/%04u %02u:%02u:%02u.%06ld",
									pTm->tm_mday,pTm->tm_mon+1,iY2K(pTm->tm_year), 
									pTm->tm_hour,pTm->tm_min,pTm->tm_sec,tv.tv_usec);

	sprintf(szBuffMesg,"%s,%s\n",szMesgDate,szMesg);
	
	fprintf(stdout,"Log : %s",szBuffMesg);


	return (ST_CODE_SUCCESS);
}

/***********************************************************************
* Function : char* read_file (const char* filename, size_t* length)
* Argument : char  = String filename
* 	       : char* = data of file in memory
* Description : Read all content data to memory
* Updated : 22/06/2012
************************************************************************/
int read_file (const char* filename, off_t* length)
{
	int fd;
	struct stat file_info;
	char* buffer;

	/* Open the file. */
	fd = open (filename, O_RDONLY);

	/* Get information about the file. */
	fstatat (fd, filename, &file_info,0);
	*length = (off_t)file_info.st_size;

	/* Make sure the file is an ordinary file. */
	if (!S_ISREG (file_info.st_mode))
	{
		/* It’s not, so give up. */
		close (fd);
		//return NULL;
		return 1;
	}

	/* Finish up. */
	close (fd);

	//return buffer;
	return (ST_CODE_SUCCESS);
}

/***********************************************************************
* Function : int remvoe_newline(const char *str_in, char find_char,char *str_in)
* Argument : str_in  = String input
* 	       : find_char = char remove
*          : str_out = String Output	
* Description : xxxxxxxxxxxxxxxxxxxx
* Updated : 22/04/2020
************************************************************************/
int remvoe_newline(const char *str_in,char *str_out)
{
	int str_in_length = strlen(str_in);
	int i,j;

	memset(str_out,0x0,sizeof(str_out));
	for(i=j=0;i<str_in_length;i++)
	{
		if (str_in[i] != '\n' && str_in[i] != '\r')
		{
			str_out[j++] = str_in[i];
		} 
		// else
		// 	printf("str_in : [%c]\n",str_in[i]);
	}

	str_out[j] = '\0';

	// printf("str_in : %s(%d)\n",str_in,str_in_length);
	// printf("str_ou : %s\n\n",str_out);

	return(0);
}

/***********************************************************************
* Function : int mapping_regular(const char *reg_rule, int maxGroups, char *szInput, char *szre_subMatch)
* Argument : reg_rule  = regular rule for mapping all line
* 	       : szInput = string input
*          : szre_subMatch = submat			
* Description : xxxxxxxxxxxxxxxxxxxx
* Updated : 22/04/2020
************************************************************************/
int mapping_regular(const char *reg_rule, int maxGroups, char *szInput, char *szre_subMatch)
{
	regex_t regex;
	regmatch_t match[10];

	char temp_subMatch[MAXBLK/2];
	char msgbuf[MAXBLK/2];
	int start;
	int finish;

	int iret;
	int istatus = REG_NOMATCH;

	/* Compile regular expression */
	// reti = regcomp(&regex, "^a[[:alnum:]]", 0);
	iret = regcomp(&regex, reg_rule, REG_EXTENDED);
	if (iret) {
		fprintf(stderr, "Could not compile regex\n");
		exit(EXIT_FAILURE);
	}

	while (1) 
	{
		/* Execute regular expression */
		iret = regexec(&regex, szInput, maxGroups, match, 0);
		if (!iret) 
		{

			#ifdef DEBUG
			printf("Match\n");			
			#endif
				
			for (int g = 0; g < maxGroups; g++)
			{
				if (match[g].rm_so == -1) {
					break;
				}

				start  = match[g].rm_so + (szInput - szInput);
				finish = match[g].rm_eo + (szInput - szInput);

				#ifdef DEBUG
				printf("[%d] re_nsub [%ld] sub match [%d]->[%d] (bytes %d:%d)\n"
																,g
																,regex.re_nsub
																,match[g].rm_so
																,match[g].rm_eo-match[g].rm_so
																,start
																,finish);

				printf ("Match [%.*s]\n",(finish - start),szInput + start);				
				#endif
				
				// Mapping value into line out
				if (g > 0)
				{
					sprintf(temp_subMatch,"%.*s",(finish - start),szInput + start);
					strcat(szre_subMatch,temp_subMatch);
					strcat(szre_subMatch,"|");
				}
				

				istatus = ST_CODE_SUCCESS;

			}
		}
		else if (iret == REG_NOMATCH) {
			#ifdef DEBUG
			printf("No match\n");
			#endif
			break;
		}
		else {
			regerror(iret, &regex, msgbuf, sizeof(msgbuf));
			fprintf(stderr, "Regex match failed: %s\n", msgbuf);
			exit(EXIT_FAILURE);
		}
		szInput += match[0].rm_eo;
	}

	/* Free memory allocated to the pattern buffer by regcomp() */
	regfree(&regex);

	return(istatus == REG_NOMATCH ? REG_NOMATCH : ST_CODE_SUCCESS);	
}

int main(int argc, char *argv[])
{
	//Declare variable
	int c; 
	int summary=0;

	// double sum_bignumber[256];

	FILE *fp;
	FILE *fp_out;

	struct gable_conf Control_yaml;
	struct gable_dynamic_field *P_field;

	char szData_File[MAXBLK/2];
	char szRecordSchema[MAXBLK/2];

	char szFullPathInpFile[MAXBLK/2];
	char szFullPathCntFile[MAXBLK/2];

	char szFullPathTmpFile[MAXBLK/2];
	char szFullPathOutFile[MAXBLK/2];

	char szBuff[MAXBLK*2];

	char szRecord[MAXBLK*2];
	char szHeader[MAXBLK*2];
	
	char szLogMesg[MAXBLK/4];
	char szRegRule[MAXBLK/4];

	off_t file_size = 0;
	int record_size = 30; 				//hard code and wait Test
	int record_file_size = 30;	//hard code and wait Test
	int all_record_calcurate;	

	int iRet;
	int i;

    // Check invalid argment
    if (argc <= 3 || argc > 6)
	{
		print_usage(argc,argv);
	}


    while((c =  getopt(argc, argv, "f:c:vs")) != EOF) 
    { 
        switch (c) 
        { 
			case 'f': 
				//cout << optarg << endl; 

				sprintf(szFullPathInpFile,"%s/%s",PATH_LND,optarg);
				sprintf(szFullPathTmpFile,"%s/%s.tmp",PATH_TMP,optarg);
				sprintf(szFullPathOutFile,"%s/%s.cnv",PATH_OUT,optarg);
				

				if(access(szFullPathInpFile,R_OK) !=0 ) //Can't read file file
				{
					fprintf(stderr,"Can't read/access data file!!!\n");
					fprintf(stderr,"FILE_DATA : [%s]\n",szFullPathInpFile);
					exit(ST_CODE_ERR_PARAMETER);
				}

				// strcpy(szData_File,optarg);
				break; 
			case 'c': 
                 //cout << optarg << endl; 		

				sprintf(szFullPathCntFile,"%s/%s",PATH_LND,optarg);
				if(access(szFullPathCntFile,R_OK) !=0 ) //Can't read file file
				{
					fprintf(stderr,"Can't read/access control file!!!\n");
					fprintf(stderr,"FILE_CONTROL : [%s]\n",szFullPathCntFile);
					exit(ST_CODE_ERR_PARAMETER);
				}

				break; 
			case 'v':
				show_verbose=1;
				break;				
        } 
    }

	// initial variable from control file
	// strcpy(szRegRule,"\\(([0-9]*)\\)");

	memset(szRegRule,0x0,sizeof(szRegRule));
	memset(&Control_yaml,0x0,sizeof(struct gable_conf));
	yaml_decode_file(szFullPathCntFile,&Control_yaml);

	memset(szHeader,0x0,sizeof(szHeader));
	for(int j=0,pre_offset=0;j<Control_yaml.field_no;j++)
	{
		//Sum all lenght of record
		Control_yaml.record_length += Control_yaml.data[j].offset;

		//Regular rules
		if (strncmp(Control_yaml.data[j].type,"regular(",8) == True)
		{
			strcat(szRegRule,Control_yaml.data[j].type+8);

			//remove charecter ')'
			if (szRegRule[strlen(szRegRule)-1] == ')')
				szRegRule[strlen(szRegRule)-1]='\0';
		} else
		{
			fprintf(stderr,"Error unknow support type [%s]!!!\n",Control_yaml.data[j].type);
		}

		
		strcat(szHeader,Control_yaml.data[j].name);
		// strcat(szRegRule,"|");
		strcat(szHeader,"|");
	}
	szHeader[strlen(szHeader)-1]='\0';
	strcat(szHeader,"\n");

	// szRegRule[strlen(szRegRule)-1]='\0';

	printf("szRegRule = [%s]\n",szRegRule);
	

	#ifdef DEBUG
	diplay_record_structure(&Control_yaml);
	#endif

	//Create record structure
	P_field = (struct gable_dynamic_field *)malloc(Control_yaml.field_no * sizeof(struct gable_dynamic_field));

	#ifdef DEBUG
	printf("--------------------------------------------------------------------\n");
	printf("OK\n");
	printf("--------------------------------------------------------------------\n");
	#else
	sprintf(szLogMesg,"Start");
	iWriteLogProgram(NULL,"Main",szLogMesg);	
	#endif

	read_file (szFullPathInpFile,&file_size);
	if (show_verbose)
	{
		//test
		//all_record_calcurate=(file_size/Control_yaml.record_length);
				
		sprintf(szLogMesg,"read file [%s]'s size [%ld]",szFullPathInpFile
		                                               ,file_size);
		iWriteLogProgram(NULL,"debug vertica",szLogMesg);
	}

	if ((fp = fopen(szFullPathInpFile, "rb")) == NULL) //Open file by pointer
	{
		fprintf(stderr,"Can't open data file!!!\n");
		fprintf(stderr,"DATA_FILE : [%s]\n",szData_File);
		fclose(fp);
		exit(ST_CODE_ERR_ACCESS_FILE);
	}

	if ((fp_out = fopen(szFullPathTmpFile, "w")) == NULL) //Open file by pointer
	{
		fprintf(stderr,"Can't open data file!!!\n");
		fprintf(stderr,"DATA_FILE : [%s]\n",szFullPathTmpFile);
		fclose(fp);
		exit(ST_CODE_ERR_ACCESS_FILE);
	}	

	i = 1;
	iRet = 0;
	do {

		//Write header record file output only first header
		if (i == 1)
		{
			fwrite(szHeader ,1 ,strlen(szHeader) ,fp_out);
		}		

		memset(szBuff,0x0,sizeof(szBuff));

		//read data on file input
		fgets(szBuff,sizeof(szBuff),fp);

		//Remove last charector
		if (szBuff[strlen(szBuff)-1] == '\n')
			szBuff[strlen(szBuff)-1]='\0';

		//check return code of read file
		// #ifdef DEBUG
		// if (iRet == 0 && i > 3)
		// #else
		// if (iRet == 0)
		// #endif
		// {
		// 	sprintf(szLogMesg,"Error rec_no[%d] result fread %d <> %d record size"	,i,iRet
		// 													,Control_yaml.record_length);
		// 	iWriteLogProgram(NULL,"main",szLogMesg);			

		// 	record_size=strlen(szBuff);

		// 	if (record_size > 0)
		// 	{	
		// 		sprintf(szLogMesg,"Error found lasted data less than [%s](%d) lenght < (%d)",szBuff
		// 		                                                                            ,record_size
		// 		                                                                            ,Control_yaml.record_length);
		// 		iWriteLogProgram(NULL,"main",szLogMesg);
		// 		//close pointer file
		// 		fclose(fp);
		// 		fclose(fp_out);
		// 		exit(ST_CODE_ERR_CONVERT_OUTPUT);
		// 	}
		// 	//  else {
		// 	// 	printf("szBuff [%s]\n",szBuff);
		// 	// }

		// 	//break loop while (1);
		// 	break;
		// }

		//keep value from file		
		#ifdef DEBUG
		printf("szRec[% 5d] : [%s]\n",i,szBuff);
		// sleep(1);
		//exit(1);

		#elif DTEST
		if ((i%15000) == 0)
		{
			sprintf(szLogMesg,"Processed : % 5d record(s)",i);
			iWriteLogProgram(NULL,"process record no",szLogMesg);
		}
		#endif
		
		memset(szRecord,0x0,sizeof(szRecord));

		//Mapping Fields
		iRet = mapping_regular(szRegRule, Control_yaml.field_no+1, szBuff, szRecord);
		if (iRet == ST_CODE_SUCCESS)
		{
			//remove last charecter's '|'
			szRecord[strlen(szRecord)-1]='\0';

			remvoe_newline(szRecord, szBuff);
			strcpy(szRecord,szBuff);

			strcat(szRecord,"\n");

			//Write detail record file output
			fwrite(szRecord ,1 ,strlen(szRecord) ,fp_out);			
		}

		//Count next record
		i++;

	} while (!feof(fp)); //while (!feof(fp));

	//close pointer file
	fclose(fp);
	fclose(fp_out);

	//Rename to Output & Statistic
	sprintf(szLogMesg,"Rename tmp file %s -> %s, Write %d records(s)"
	                                                 ,szFullPathTmpFile
													 ,szFullPathOutFile
													 ,i);
	iWriteLogProgram(NULL,"Rename Output file",szLogMesg);	
	rename(szFullPathTmpFile,szFullPathOutFile);

	sprintf(szLogMesg,"Successfully.");
	iWriteLogProgram(NULL,"Main",szLogMesg);	

    return(ST_CODE_SUCCESS);    
}
