#!/bin/bash

#--------------------------------------------------------------------
#              Copyright C - 2012 All rights reserved
#                        G-Able Thailand.
#
#       No part of this program may be reproduced or adapted in any
#             form or by any means, electronic or mechanical,
#       without permission from G-Able Thailand Thailand.
#          This program is confidential and may not be disclosed,
#          decompiled or reverse engineered without permission in
#             writing from G-Able Thailand Thailand.
#
#--------------------------------------------------------------------
# DESCRIPTION:
#    initial database and any object on platform data analysis (initiai_db)
#
#--------------------------------------------------------------------
# Revision   Date        CR No.   Author         Description
#====================================================================
#  0.2       16/01/2020  XXX      Suebsakul.A    Project Push to SEE, Your DATA
#
#
#
#

#Force run profile for support Job Schduler
. $HOME/.profile

#Get current script filename
current_Script="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")"
#echo $me

#Conver file Dos2Unix
cd ~/Framework/
find . -type f | grep -v .git | grep -v $current_Script | grep -v .metadata | grep -v output | grep -e .csv$ -e sh$ -e .conf$ -e .sql$ -e .config$ -e .inc$ -e .py$ -e .control$ | while read x
do
    dos2unix $x
    cat $x | tr '\r' '\n' > $x.tmp
    mv $x.tmp $x
done

# Change mode only file.sh
find . -name *.sh -exec chmod 755 {} \;
