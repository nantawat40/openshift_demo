#!/bin/bash

#--------------------------------------------------------------------
#              Copyright C - 2012 All rights reserved
#                        G-Able Thailand.
#
#       No part of this program may be reproduced or adapted in any
#             form or by any means, electronic or mechanical,
#       without permission from G-Able Thailand Thailand.
#          This program is confidential and may not be disclosed,
#          decompiled or reverse engineered without permission in
#             writing from G-Able Thailand Thailand.
#
#--------------------------------------------------------------------
# DESCRIPTION:
#    initial database and any object on platform data analysis (initiai_db)
#
#--------------------------------------------------------------------
# Revision   Date        CR No.   Author         Description
#====================================================================
#  0.2       16/01/2020  XXX      Suebsakul.A    Project Push to SEE, Your DATA
#
#
#
#

# Parameters
# -f [Filename.sql] : new initial database, tables and object for data processing

#Force run profile for support Job Schduler
. $HOME/.profile

# Load global variables and function library
GLOBAL_LIB_CONFIG=$HOME/Framework/configs/lib/global.inc
. $GLOBAL_LIB_CONFIG


EXEC_NAME="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")"
EXEC_PARA='-nono <NONONONO>'
INITIAL='initial'

# Load global variables and function library
. $MAIN_CONFIG


apt update
apt install -y default-jre
apt install -y nano 
apt install -y dos2unix
apt install -y wget

#md5sum
apt install -y ucommon-utils

#python
apt install -y python3.6
apt install -y python3.6-dev
apt install -y python3.6-venv
apt install -y python3-pip

wget https://bootstrap.pypa.io/get-pip.py
python3.6 get-pip.py

ln -s /usr/bin/python3.6 /usr/local/bin/python3
ln -s /usr/local/bin/pip /usr/local/bin/pip3
pip install pandas 
pip install xlrd 
python3 -V

#python excel teamplate
pip install xlsxwriter
pip install Image


#lib yaml
apt install -y libyaml-dev
apt install -y libglib2.0-dev

#locale
apt-get install -y locales locales-all
locale-gen en_US en_US.UTF-8
# dpkg-reconfigure locales

echo "LANG=en_US.UTF-8" >> /etc/default/locale
echo "LC_ALL=en_US.UTF-8" >> /etc/default/locale
echo "LANGUAGE=en_US.UTF-8" >> /etc/default/locale

cd
echo "export LC_ALL=en_US.UTF-8"   >> .profile
echo "export LANG=en_US.UTF-8"     >> .profile
echo "export LANGUAGE=en_US.UTF-8" >> .profile

echo "export HOME_ETL=$HOME/Framework" >> .profile

#reload profile
. ./.profile

# #Check number of argment
# if [ $# -ne 2 ]
# then
#   _usage
# else
#   let i=1
#   for p in $*;
#   do
#     case "$p" in
#       "-f")
#         FILE_DDL=${@:$((i+1)):1}
#       ;;
#       *)
#         if [ $((i%2)) -eq 1 ]
#         then
#           _usage
#         fi
#       ;;
#     esac
#     let i=i+1
#   done;

#   if [ -z $FILE_DDL ]
#   then
#     _usage
#   fi
# fi

# # echo $FILE_DDL

# LOG_PATH=$LOG_PATH/$INITIAL

# if [ ! -d $LOG_PATH ]
# then
#   mkdir $LOG_PATH
# fi

# LOG_FILE=$LOG_PATH/$FILE_DDL.log

# #Define start time
# START_TIME="$(date +%s%N)"

# _logger "Started FILE_DDL=${FILE_DDL}" $LOG_FILE

# #Add Full path
# FULLPATH_FILE_DDL=$CREATE_PATH/$FILE_DDL

# if [ ! -f $FULLPATH_FILE_DDL ]
# then    
#   _logger "Unable to access file: $FULLPATH_FILE_DDL" $LOG_FILE
#   exit $ST_UNEXPECTED_ERROR
# else
#   #clear old temp file
#   rm -f $TMP_PATH/$FILE_DDL $TMP_PATH/out.sql  
# fi

# # echo "------------------------------------------------------------"
# # echo "Show all databases"
# # echo "------------------------------------------------------------"
# # #$DATABASE_CLIENT_COMMAND
# # ${DATABASE_CLIENT_COMMAND} --user ${DATABASE_ROOT_USERNAME} --password=${DATABASE_ROOT_PASSWORD} <<EOFMYSQL
# # show databases
# # EOFMYSQL


# #--------------------------------------
# # STEP 1. xxxx xxxx
# #--------------------------------------
# #Replace variable on File.sql to Temp Path
# _logger "copy file $FILE_DDL to $TMP_PATH/$FILE_DDL" $LOG_FILE
# cp $FULLPATH_FILE_DDL $TMP_PATH/$FILE_DDL

# sed -e "s/\${DATABASE_NAME\}/$DATABASE_NAME/g" \
#     -e "s/\${DATABASE_STG_PASSWORD\}/$DATABASE_STG_PASSWORD/g" \
#     -e "s/\${DATABASE_DWH_PASSWORD\}/$DATABASE_DWH_PASSWORD/g" \
#     -e "s/\${DATABASE_DM_PASSWORD\}/$DATABASE_DM_PASSWORD/g"   \
#     -e "s/\${DATABASE_ETL_PASSWORD\}/$DATABASE_ETL_PASSWORD/g" $TMP_PATH/$FILE_DDL > $TMP_PATH/out.sql

# #echo "Transfer variabel to file : $TMP_PATH/out.sql"
# _logger "Replace variabel on $FILE_DDL to $TMP_PATH/out.sql" $LOG_FILE

# #echo "$AAAA"

# #--------------------------------------
# # STEP 1. xxxx xxxx
# #--------------------------------------
# _logger "Execute File DDL on file : $TMP_PATH/out.sql" $LOG_FILE

# # echo "------------------------------------------------------------"
# # echo "Execute File DDL on file:$TMP_PATH/out.sql"
# # echo "------------------------------------------------------------"
# ${DATABASE_CLIENT_COMMAND} --user ${DATABASE_ROOT_USERNAME} --password=${DATABASE_ROOT_PASSWORD} < $TMP_PATH/out.sql


# #--------------------------------------
# # STEP 1. Verify Error Code
# #--------------------------------------
# RESULT=$?

# if [ $RESULT -ne 0 ]
# then
#   #_logError $ERROR_FILE $LOG_FILE
#   #ERROR=$(_readFile $ERROR_FILE 1)
#   ST_CODE=$ST_UNKNOW_ERROR
#   ST_NAME="$ST_UNKNOW_ERROR_NAME $ERROR"
# fi


# #--------------------------------------
# # Summary
# #--------------------------------------
# ELAPSED=$(_calcElapsedTime $START_TIME)

# _logger "Elapsed $ELAPSED sec." $LOG_FILE
# _logger "STATUS=$ST_CODE $ST_NAME" $LOG_FILE
# _logger "END" $LOG_FILE
# echo " " >> $LOG_FILE

# #--------------------------------------
# # Verify result command for support Job Schduler
# #--------------------------------------
# #for support Job Schduler
# if [ $ST_CODE -eq $ST_RECONCILE_REC_ERROR -o $ST_CODE -eq $ST_RECONCILE_SUM_ERROR ]
# then
#   exit $ST_COMPLETE
# else
#   exit $ST_CODE
# fi
