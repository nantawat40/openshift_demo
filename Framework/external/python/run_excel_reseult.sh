#!/bin/bash

#--------------------------------------------------------------------
#              Copyright C - 2012 All rights reserved
#                        G-Able Thailand.
#
#       No part of this program may be reproduced or adapted in any
#             form or by any means, electronic or mechanical,
#       without permission from G-Able Thailand Thailand.
#          This program is confidential and may not be disclosed,
#          decompiled or reverse engineered without permission in
#             writing from G-Able Thailand Thailand.
#
#--------------------------------------------------------------------
# DESCRIPTION:
#    create table/view/object on database on area staging
#
#--------------------------------------------------------------------
# Revision   Date        CR No.   Author    Description
#====================================================================
#
#
#
#
#
#


cd $HOME_ETL/external/python/
python3.6 convert_result_to_excel.py -p $HOME_ETL/output/tests/unit_test/TABLE -b 60

RESULT=$?

if [ $RESULT -ne 0 ]
then

    echo "Terminate error return code [$RESULT]"
    exit 1
fi
