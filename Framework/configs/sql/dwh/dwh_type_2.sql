SQL_COMMIT="COMMIT;"
SQL_ROLLBACK="ROLLBACK;"
SQL_SELECT_DATABASE="USE ${DATABASE_NAME}_DWH;"
SQL_LIST_COLUMNS="
                    SELECT
                        DWH_FIELDS, 
                        CASE
                        	WHEN IDENTIFY = 'SEQUENCE' 	  THEN 'INT(1)'
                            WHEN DATATYPE LIKE 'String(%' THEN REPLACE(DATATYPE,'String','Varchar')
                            WHEN DATATYPE LIKE 'Direct'   THEN 'Varchar(128)'
                            ELSE DATATYPE
                        END 'DataType',
                        'NOT NULL' as 'NULL',
                        CASE
                        	WHEN IDENTIFY = 'SEQUENCE' 	  THEN CONCAT('DEFAULT nextval(','${DWH_TABLE_NAME}','_SEQ)')
                        	WHEN length(trim(Default_Value)) > 0 THEN CONCAT('DEFAULT ''',Default_Value,'''')
                        	ELSE ''
                        END 'DEFAULT',
                        'COMMENT \'Generate this field by G-Able Framework\',' as COMMENT
                    FROM ${DATABASE_NAME}_STG.STG_DWH_MAPPING_STG_V1
                    WHERE DWH_TABLE = '${DWH_TABLE_NAME}'
                    ORDER BY CAST(DWH_NO AS INT);"

SQL_GET_PK_UK_COLUMNS="
                    SELECT
                        CONCAT('PRIMARY KEY ${DWH_TABLE_NAME}_PK (' ,GROUP_CONCAT(DISTINCT DWH_Fields SEPARATOR ','), ')',',') AS PK	
                    FROM ${DATABASE_NAME}_STG.STG_DWH_MAPPING_STG_V1
                    WHERE DWH_TABLE = '${DWH_TABLE_NAME}' AND KEYTYPE = 'PK'
                    UNION
                    SELECT
                        CONCAT('UNIQUE KEY ${DWH_TABLE_NAME}_UK (' ,GROUP_CONCAT(DISTINCT DWH_Fields SEPARATOR ','), ')',',') AS UK
                    FROM ${DATABASE_NAME}_STG.STG_DWH_MAPPING_STG_V1
                    WHERE DWH_TABLE = '${DWH_TABLE_NAME}' AND KEYTYPE = 'UK';"

SQL_RULE_CHECK_ERR="
                    SELECT
                        LIST_RULES.*
                    FROM 
                    (
                        SELECT
                            CONCAT('CASE WHEN ' ,GROUP_CONCAT(DISTINCT CONCAT('PK_DUP.',DWH_Fields) SEPARATOR ' IS NOT NULL OR '), ' IS NOT NULL THEN TRUE ELSE FALSE END ERR_IS_PK_DUP,') AS RULES	
                        FROM ${DATABASE_NAME}_STG.STG_DWH_MAPPING_STG_V1
                        WHERE DWH_TABLE = '${DWH_TABLE_NAME}' AND DWH_TYPE = 'TYPE_2' AND IDENTIFY = 'STG_PK'
                        UNION
                        SELECT
                            CONCAT('CASE WHEN ' ,GROUP_CONCAT(DISTINCT CONCAT('M_SOURCE.',DWH_Fields) SEPARATOR ' IS NULL OR '), ' IS NULL THEN TRUE ELSE FALSE END ERR_IS_NULL_VALUE,') AS RULES
                        FROM ${DATABASE_NAME}_STG.STG_DWH_MAPPING_STG_V1
                        WHERE DWH_TABLE = '${DWH_TABLE_NAME}' 
                          AND DWH_Fields not like 'STG_%' 
                          AND STG_Fields not like '%.%'
                          AND LENGTH(TRIM(STG_FIELDS)) > 0
                        UNION
                        SELECT
                            CONCAT('CASE WHEN ' ,GROUP_CONCAT(DISTINCT CONCAT('LENGTH(M_SOURCE.',DWH_Fields) SEPARATOR ' ) = 0 OR '), ' ) = 0 THEN TRUE ELSE FALSE END ERR_IS_BLANK_VALUE,') AS RULES	
                        FROM ${DATABASE_NAME}_STG.STG_DWH_MAPPING_STG_V1
                        WHERE DWH_TABLE = '${DWH_TABLE_NAME}' AND KEYTYPE NOT IN ('PK','UK') AND LENGTH(TRIM(STG_FIELDS)) > 0
                        UNION
                        SELECT
                                CONCAT('CASE WHEN ' ,
	                                GROUP_CONCAT(
	                                                CASE
	                                                    WHEN CONF.DataType LIKE 'String(%'  THEN CONCAT('LENGTH(M_SOURCE.',CONF.DWH_Fields,') > ', REGEXP_REPLACE(DATATYPE, '[A-Z-a-z\_\(\)]',''))
	                                                    WHEN CONF.DataType LIKE 'INT(%'     THEN CONCAT('       M_SOURCE.',CONF.DWH_Fields,' > ',' 2147483647       OR M_SOURCE.',CONF.DWH_Fields, ' < -2147483647')
	                                                    WHEN CONF.DataType LIKE 'DECIMAL(%' THEN CONCAT('       M_SOURCE.',CONF.DWH_Fields,' > ',' 999999999999.999 OR M_SOURCE.',CONF.DWH_Fields, ' < -999999999999.999')
	                                                END
	                                SEPARATOR ' OR '),
                                ' THEN TRUE ELSE FALSE END ERR_LENGTH_OR_VALUE_OVERFLOW,') AS RULES
                        FROM ${DATABASE_NAME}_STG.STG_DWH_MAPPING_STG_V1 CONF 
                        WHERE DWH_TABLE = '${DWH_TABLE_NAME}' AND CONF.DWH_Fields not like 'STG_%' AND LENGTH(TRIM(STG_FIELDS)) > 0
                        UNION
                        SELECT 
                            CONCAT(GROUP_CONCAT(CONCAT('CASE WHEN ',' REF_',REF_TABLE,'.',REF_FIELDS,' IS NULL THEN TRUE ELSE FALSE END WRN_IS_',DWH_FIELDS,'_NULL') SEPARATOR ','),',') AS RULES
                        FROM ${DATABASE_NAME}_STG.STG_DWH_MAPPING_STG_V1
                        WHERE DWH_TABLE = '${DWH_TABLE_NAME}' AND (REF_TABLE IS NULL OR LENGTH(REF_TABLE) > 0)
                        UNION
                        SELECT
                            CONCAT('CASE WHEN ' ,GROUP_CONCAT(DISTINCT CONCAT('M_SOURCE.',DWH_Fields) SEPARATOR ' > CURRENT_DATE '), ' > CURRENT_DATE THEN TRUE ELSE FALSE END ERR_SOURCE_DT_MT_CURRENT_DATE') AS RULES	
                        FROM ${DATABASE_NAME}_STG.STG_DWH_MAPPING_STG_V1
                        WHERE DWH_TABLE = '${DWH_TABLE_NAME}' AND DWH_Fields = 'STG_SOURCE_DT'
                    ) LIST_RULES
                    WHERE LIST_RULES.RULES IS NOT NULL;"

SQL_GET_LIST_PK="
                    SELECT
                        GROUP_CONCAT(DISTINCT DWH_Fields SEPARATOR ',') LIST_FiED_DUP
                    FROM ${DATABASE_NAME}_STG.STG_DWH_MAPPING_STG_V1
                    WHERE DWH_TABLE = '${DWH_TABLE_NAME}' AND DWH_TYPE = 'TYPE_2' AND IDENTIFY = 'STG_PK';"

SQL_GET_LIST_UK="
                    SELECT
                        GROUP_CONCAT(DISTINCT DWH_Fields SEPARATOR ',') LIST_FiED_DUP
                    FROM ${DATABASE_NAME}_STG.STG_DWH_MAPPING_STG_V1
                    WHERE DWH_TABLE = '${DWH_TABLE_NAME}' AND KEYTYPE = 'UK'"

SQL_GET_LIST_CDN="
                    SELECT
                        GROUP_CONCAT(DISTINCT DWH_Fields SEPARATOR ',') LIST_FiED_DUP
                    FROM ${DATABASE_NAME}_STG.STG_DWH_MAPPING_STG_V1
                    WHERE DWH_TABLE = '${DWH_TABLE_NAME}' AND IDENTIFY = 'CDN';"                    

SQL_ON_JOIN_FiED_DUP_PK="
                    SELECT
                        GROUP_CONCAT(DISTINCT CONCAT('${ALIAS_DUP}.',DWH_Fields ,' = M_SOURCE.',DWH_Fields) SEPARATOR ' AND ')  LIST_FiED_DUP
                    FROM ${DATABASE_NAME}_STG.STG_DWH_MAPPING_STG_V1
                    WHERE DWH_TABLE = '${DWH_TABLE_NAME}' AND DWH_TYPE = 'TYPE_2' AND IDENTIFY = 'STG_PK'
                    UNION
                    SELECT CONCAT(' AND ${ALIAS_DUP}.STG_SOURCE_FILE = M_SOURCE.STG_SOURCE_FILE');"

SQL_ON_JOIN_FiED_DUP_UK="
                     SELECT
                         GROUP_CONCAT(DISTINCT CONCAT('${ALIAS_DUP}.',DWH_Fields ,' = M_SOURCE.',DWH_Fields) SEPARATOR ' AND ')  LIST_FiED_DUP
                     FROM ${DATABASE_NAME}_STG.STG_DWH_MAPPING_STG_V1
                     WHERE DWH_TABLE = '${DWH_TABLE_NAME}' AND KEYTYPE = 'UK'
                     UNION
                     SELECT CONCAT(' AND ${ALIAS_DUP}.STG_SOURCE_FILE = M_SOURCE.STG_SOURCE_FILE');"

SQL_DUP_FIELD="
                    LEFT JOIN (
                        SELECT 
                                 ${LIST_FiED_DUP}, STG_SOURCE_FILE
                        FROM ${DATABASE_NAME}_STG.${STG_TABLE_NAME}
                        GROUP BY ${LIST_FiED_DUP}, STG_SOURCE_FILE
                        HAVING COUNT(*) > 1) ${ALIAS_DUP} 
                        ON 
                            ${LIST_ONJOIN_FiED_DUP}"

SQL_REF_FIELD="
                    SELECT 
                        CONCAT('LEFT JOIN ', A.REF_TABLE, ' REF_', A.REF_TABLE,' ON REF_',A.REF_TABLE,'.', A.REF_FIELDS , ' = M_SOURCE.',A.DWH_FIELDS)
                    FROM ${DATABASE_NAME}_STG.STG_DWH_MAPPING_STG_V1 A 
                    WHERE A.DWH_TABLE = '${DWH_TABLE_NAME}' AND (A.REF_TABLE IS NULL OR LENGTH(A.REF_TABLE) > 0)"

SQL_GET_LIST_FIELD_ERR="
                        SELECT 
                            CONCAT(GROUP_CONCAT(DISTINCT C.COLUMN_NAME SEPARATOR ' OR '))
                        FROM INFORMATION_SCHEMA.COLUMNS C
                        WHERE
                            C.COLUMN_NAME LIKE 'ERR_%' AND
                            C.DATA_TYPE  = 'INT' AND
                            C.TABLE_NAME = '${DWH_TABLE_NAME}_CLASSIFICATION';"

SQL_GET_LIST_FIELD_WRN="
                        SELECT 
                            CONCAT(GROUP_CONCAT(DISTINCT C.COLUMN_NAME SEPARATOR ' OR '))
                        FROM INFORMATION_SCHEMA.COLUMNS C
                        WHERE
                            C.COLUMN_NAME LIKE 'WRN_%' AND
                            C.DATA_TYPE  = 'INT' AND
                            C.TABLE_NAME = '${DWH_TABLE_NAME}_CLASSIFICATION';"                            

SQL_LIST_CND_NOT_IN_PK="
                    SELECT                        
                        GROUP_CONCAT(DISTINCT CONCAT('B.',DWH_Fields ,' = A.',DWH_Fields) SEPARATOR ' AND ')  LIST_FiED_DUP
                    FROM ${DATABASE_NAME}_STG.STG_DWH_MAPPING_STG_V1
                    WHERE DWH_TABLE = '${DWH_TABLE_NAME}' AND IDENTIFY = 'STG_PK';"

SQL_LIST_CND_NOT_IN="
                        SELECT 
                            CONCAT('(',GROUP_CONCAT(DISTINCT CONCAT('(A.',C.COLUMN_NAME,' = B.',C.COLUMN_NAME, ' OR A.',C.COLUMN_NAME,' IS NULL)') SEPARATOR ' AND '),')')                            
                        FROM INFORMATION_SCHEMA.COLUMNS C
                        LEFT JOIN ${DATABASE_NAME}_STG.STG_DWH_MAPPING_STG_V1 R on R.DWH_TABLE = C.TABLE_NAME and R.DWH_FIELDS = C.COLUMN_NAME
                        WHERE
                            C.COLUMN_NAME NOT LIKE 'STG_%' AND
                            C.COLUMN_NAME NOT LIKE 'DWH_%' AND                            
                            C.COLUMN_NAME NOT LIKE REPLACE('${DWH_TABLE_NAME}_%','MASTER_','') AND
                            C.COLUMN_NAME NOT LIKE '%HIST_START_DATE' AND
                            C.COLUMN_NAME NOT LIKE '%HIST_END_DATE' AND
                            LENGTH(TRIM(R.STG_FIELDS)) > 0 AND
                            C.COLUMN_NAME <> 'LAST_MODIFIED_DT' AND
                            C.TABLE_NAME = '${DWH_TABLE_NAME}';"

SQL_INSERT_ERR="
                    INSERT INTO   ${DWH_TABLE_NAME}_ERR
                    SELECT * FROM ${DWH_TABLE_NAME}_CLASSIFICATION A
                    WHERE 
                        (
                            ${LIST_FLAG_ERR}
                        ) AND NOT EXISTS (
                            SELECT
                                1
                            FROM
                                ${DWH_TABLE_NAME}_ERR B
                            WHERE
                                ${LIST_CND_NOT_IN}
                        );"

SQL_INSERT_WRN="
                    INSERT INTO   ${DWH_TABLE_NAME}_WRN
                    SELECT * FROM ${DWH_TABLE_NAME}_CLASSIFICATION A
                    WHERE 
                        !(
                            ${LIST_FLAG_ERR}
                        ) and
                        (
                            (${LIST_FLAG_WRN})
                        ) AND NOT EXISTS (
                            SELECT
                                1
                            FROM
                                ${DWH_TABLE_NAME}_WRN B
                            WHERE
                                ${LIST_CND_NOT_IN}
                        );"                        

# -- SQL_LIST_FIELD_INSERT="
# --                         SELECT 
# --                             GROUP_CONCAT(DISTINCT C.COLUMN_NAME SEPARATOR ', ')                            
# --                         FROM INFORMATION_SCHEMA.COLUMNS C
# --                         WHERE
# --                             C.COLUMN_NAME NOT LIKE 'DWH_%' AND
# --                             C.COLUMN_NAME <> 'LAST_MODIFIED_DT' AND
# --                             C.TABLE_NAME = '${DWH_TABLE_NAME}';"

SQL_LIST_FIELD_INSERT_STG="
                            SELECT
                                GROUP_CONCAT(DISTINCT CONCAT('A.',A.STG_FIELDS) SEPARATOR ', ')
                            FROM ${DATABASE_NAME}_STG.STG_DWH_MAPPING_STG_V1 A
                            WHERE
                                A.DWH_TABLE = '${DWH_TABLE_NAME}' AND LENGTH(TRIM(A.STG_FIELDS)) > 0
                            ORDER BY CAST(DWH_NO AS INT);"

SQL_LIST_FIELD_INSERT_DWH="
                            SELECT
                                GROUP_CONCAT(DISTINCT A.DWH_FIELDS SEPARATOR ', ')
                            FROM ${DATABASE_NAME}_STG.STG_DWH_MAPPING_STG_V1 A
                            WHERE
                                A.DWH_TABLE = '${DWH_TABLE_NAME}' AND LENGTH(TRIM(A.STG_FIELDS)) > 0
                            ORDER BY CAST(DWH_NO AS INT);"

SQL_INSERT_WRN_LAST="
                    INSERT INTO   ${DWH_TABLE_NAME}_WRN_LAST 
                           (${LIST_FIELD_SOURCE_DWH})
                    SELECT  ${LIST_FIELD_SOURCE_STG}  FROM ${DWH_TABLE_NAME}_WRN A
                    WHERE
                        !(
                            ${LIST_FLAG_ERR}
                        ) AND
                        (
                            (${LIST_FLAG_WRN})
                        );"


SQL_DELETE_WRN="         DELETE FROM ${DWH_TABLE_NAME}_WRN;"
SQL_DELETE_WRN_LAST="    DELETE FROM ${DWH_TABLE_NAME}_WRN_LAST;"

SQL_GET_SOURCEFILE_PK="
                        SELECT ${LIST_FIELD_SOURCE_STG}, MAX(STG_SOURCE_FILE)
                        FROM ${DWH_TABLE_NAME}_CLASSIFICATION
                        GROUP BY ${LIST_FIELD_SOURCE_STG}"

#-- and (${LIST_FIELD_PK}, STG_SOURCE_FILE) IN (SELECT ${LIST_FIELD_PK}, MAX(STG_SOURCE_FILE)
#-- 													                  FROM ${DWH_TABLE_NAME}_CLASSIFICATION
#-- 													                  GROUP BY ${LIST_FIELD_PK})                        

SQL_INSERT_NEW="
                    INSERT INTO   ${DWH_TABLE_NAME} 
                           (${LIST_FIELD_SOURCE_DWH})
                    SELECT  ${LIST_FIELD_SOURCE_STG_TRANSFORM}  FROM ${DWH_TABLE_NAME}_CLASSIFICATION A
                    WHERE 
                        !(
                            (${LIST_FLAG_ERR}) OR
                            (${LIST_FLAG_WRN})
                        ) AND NOT EXISTS (
                            SELECT
                                1
                            FROM
                                ${DWH_TABLE_NAME} B
                            WHERE
                                ${LIST_CND_NOT_IN_PK}
                        ) AND EXISTS  (
                            SELECT 1
                            FROM
                            (
                                SELECT ${LIST_FIELD_PK}, MAX(STG_SOURCE_FILE) STG_SOURCE_FILE
                                FROM ${DWH_TABLE_NAME}_CLASSIFICATION
                                GROUP BY ${LIST_FIELD_PK}
                            ) T
                            WHERE ${LIST_CND_EXISTS_PK} and T.STG_SOURCE_FILE = A.STG_SOURCE_FILE
                        );"

SQL_CND_UPDATE_TYPE2="
                        SELECT
                            CONCAT('(',GROUP_CONCAT(
                                                    DISTINCT 
                                                        CASE
                                                            WHEN TRANSFORM_FUNC LIKE 'CNV_DATE%' THEN
                                                                CASE
                                                                    WHEN TRANSFORM_FUNC LIKE '%\(MM/DD/YYYY HH:MM\)' THEN
                                                                        CONCAT('B.',DWH_Fields,' = ','CAST(REGEXP_REPLACE(A.',STG_FIELDS,',''^([0-9]*)/([0-9]*)/([0-9]*) ([0-9]*):([0-9]*)'',''\\\\3/\\\\1/\\\\2 \\\\4:\\\\5:00'') AS DATETIME)')
                                                                    ELSE
                                                                        CONCAT('B.',DWH_Fields,' = A.',STG_FIELDS)
                                                                END
                                                            ELSE
                                                                CONCAT('B.',DWH_Fields,' = A.',STG_FIELDS)
                                                        END
                                                    SEPARATOR ' OR '),') AND ') LIST_FiED_PK
                        FROM ${DATABASE_NAME}_STG.STG_DWH_MAPPING_STG_V1
                        WHERE DWH_TABLE = '${DWH_TABLE_NAME}' AND (Identify = 'STG_PK')
                        UNION
                        SELECT
                            CONCAT('(',GROUP_CONCAT(
                                                    DISTINCT 
                                                        CASE
                                                            WHEN TRANSFORM_FUNC LIKE 'CNV_DATE%' THEN
                                                                CASE
                                                                    WHEN TRANSFORM_FUNC LIKE '%\(MM/DD/YYYY HH:MM\)' THEN
                                                                        CONCAT('B.',DWH_Fields,' <> ','CAST(REGEXP_REPLACE(A.',STG_FIELDS,',''^([0-9]*)/([0-9]*)/([0-9]*) ([0-9]*):([0-9]*)'',''\\\\3/\\\\1/\\\\2 \\\\4:\\\\5:00'') AS DATETIME)')
                                                                    ELSE
                                                                        CONCAT('B.',DWH_Fields,' <> A.',STG_FIELDS)
                                                                END
                                                            ELSE
                                                                CONCAT('B.',DWH_Fields,' <> A.',STG_FIELDS)
                                                        END
                                                    SEPARATOR ' OR '),') AND ') LIST_FiED_CDC                            
                        FROM ${DATABASE_NAME}_STG.STG_DWH_MAPPING_STG_V1
                        WHERE DWH_TABLE = '${DWH_TABLE_NAME}' AND (Identify = 'CDC')
                        UNION
                        SELECT
                        	CONCAT('(B.STG_SOURCE_DT < A.STG_SOURCE_DT) AND (B.STG_SOURCE_FILE < A.STG_SOURCE_FILE) ');"

SQL_JOIN_UPDATE_TYPE2="
                        SELECT
                            GROUP_CONCAT(DISTINCT CONCAT('STG.',DWH_Fields,' = DWH_DES.',DWH_Fields) SEPARATOR ' AND ') LIST_FiED_PK                            
                        FROM ${DATABASE_NAME}_STG.STG_DWH_MAPPING_STG_V1
                        WHERE DWH_TABLE = '${DWH_TABLE_NAME}' AND (Identify = 'STG_PK')"

SQL_UPDATE_TYPE2_VALUE="
                        SELECT 'DWH_DES.${FIELD_END_DATE} = DATE_ADD(CAST(STG.STG_SOURCE_DT AS DATE), INTERVAL -1 SECOND),'
                        UNION
                        SELECT 'DWH_DES.LAST_MODIFIED_DT = current_timestamp()'"


SQL_UPDATE_TYPE2_END_DATE_LAST_RECORD="
                        UPDATE ${DWH_TABLE_NAME} AS DWH_DES
                        INNER JOIN (
                                        SELECT ${LIST_FIELD_SOURCE_STG}
                                        FROM ${DWH_TABLE_NAME}_CLASSIFICATION A
                                        JOIN (
                                                SELECT 
                                                    ${LIST_FIELD_PK}, MAX(${GET_FIELD_SEQ}) MAX_SEQ
                                                FROM ${DWH_TABLE_NAME}
                                                GROUP BY ${LIST_FIELD_PK}
                                            ) LAST_REC
                                            ON 
                                                ${LIST_ONJOIN_FIELD_LAST_RECORD}                                        
                                        WHERE
                                        !(
                                           (${LIST_FLAG_ERR}) OR
                                           (${LIST_FLAG_WRN})
                                        ) AND EXISTS (
                                                SELECT
                                                    1
                                                FROM
                                                    ${DWH_TABLE_NAME} B
                                                WHERE
                                                    ((B.${GET_FIELD_SEQ} = LAST_REC.MAX_SEQ) AND ${LIST_CND_UPDATE_TYPE2})
                                            )
                            ) AS STG ON ${LIST_JOIN_UPDATE_PK} AND DWH_DES.${FIELD_END_DATE} = CAST('9999-12-31 23:59:59' as DATETIME)
                            SET
                                ${FIELDS_UPDATE_TYPE2_VALUE};"

SQL_GET_HIST_FIELDS_NAME="
                    SELECT
                        GROUP_CONCAT( DWH_Fields SEPARATOR ', ')
                    FROM DATA_TESTING_STG.STG_DWH_MAPPING_STG_V1
                    WHERE DWH_TABLE = '${DWH_TABLE_NAME}' AND (Identify = 'HIST');"

SQL_GET_HIST_FIELDS_VALUE="
                    SELECT
                        GROUP_CONCAT(
                                        CASE
                                            WHEN DataType LIKE 'DateTime'  THEN 
                                                CASE	
                                                    WHEN DWH_Fields LIKE '%START_DATE' THEN CONCAT('CONCAT(CAST(A.STG_SOURCE_DT as DATE),'' 00:00:00'') ', DWH_Fields)
                                                    WHEN DWH_Fields LIKE '%END_DATE'   THEN CONCAT('''9999-12-31 23:59:59'' ', DWH_Fields)
                                                END
                                        END SEPARATOR ', ')
                    FROM ${DATABASE_NAME}_STG.STG_DWH_MAPPING_STG_V1
                    WHERE DWH_TABLE = '${DWH_TABLE_NAME}' AND (Identify = 'HIST');"

SQL_JOIN_LAST_RECORD="
                    SELECT
                        GROUP_CONCAT(DISTINCT CONCAT('LAST_REC.',DWH_Fields,' = ','A.',DWH_Fields) SEPARATOR ' AND ') LIST_FiED_DUP
                    FROM ${DATABASE_NAME}_STG.STG_DWH_MAPPING_STG_V1
                    WHERE DWH_TABLE = '${DWH_TABLE_NAME}' AND DWH_TYPE = 'TYPE_2' AND IDENTIFY = 'STG_PK';"

SQL_GET_LIST_SEQ="
                    SELECT
                        DWH_Fields LIST_FIELD_SEQ
                    FROM ${DATABASE_NAME}_STG.STG_DWH_MAPPING_STG_V1
                    WHERE DWH_TABLE = '${DWH_TABLE_NAME}' AND DWH_TYPE = 'TYPE_2' AND IDENTIFY = 'SEQUENCE';"

SQL_INSERT_CNG_RECORD="
                    INSERT INTO   ${DWH_TABLE_NAME} 
                           (${LIST_FIELD_SOURCE_DWH},${FIELDS_HIST_NAME})
                    SELECT  ${LIST_FIELD_SOURCE_STG_TRANSFORM},${FIELDS_HIST_VALUE}  
                    FROM ${DWH_TABLE_NAME}_CLASSIFICATION A
                    JOIN (
                            SELECT 
                                ${LIST_FIELD_PK}, MAX(${GET_FIELD_SEQ}) MAX_SEQ
                            FROM ${DWH_TABLE_NAME}
                            GROUP BY ${LIST_FIELD_PK}
                        ) LAST_REC
                        ON 
                            ${LIST_ONJOIN_FIELD_LAST_RECORD}
                    WHERE 
                        !(
                            (${LIST_FLAG_ERR}) OR
                            (${LIST_FLAG_WRN})
                        ) AND EXISTS (
                            SELECT
                                1
                            FROM
                                ${DWH_TABLE_NAME} B
                            WHERE                                
                                ((B.${GET_FIELD_SEQ} = LAST_REC.MAX_SEQ) AND ${LIST_CND_UPDATE_TYPE2})
                        ) AND EXISTS  (
                            SELECT 1
                            FROM
                            (
                                SELECT ${LIST_FIELD_PK}, MAX(STG_SOURCE_FILE) STG_SOURCE_FILE
                                FROM ${DWH_TABLE_NAME}_CLASSIFICATION
                                GROUP BY ${LIST_FIELD_PK}
                            ) T
                            WHERE ${LIST_CND_EXISTS_PK} and T.STG_SOURCE_FILE = A.STG_SOURCE_FILE
                        );"
