#!/bin/bash

#--------------------------------------------------------------------
#              Copyright C - 2012 All rights reserved
#                        G-Able Thailand.
#
#       No part of this program may be reproduced or adapted in any
#             form or by any means, electronic or mechanical,
#       without permission from G-Able Thailand Thailand.
#          This program is confidential and may not be disclosed,
#          decompiled or reverse engineered without permission in
#             writing from G-Able Thailand Thailand.
#
#--------------------------------------------------------------------
# DESCRIPTION:
#    create table/view/object on database on area staging
#
#--------------------------------------------------------------------
# Revision   Date        CR No.   Author    Description
#====================================================================
#
#
#
#
#
#


cd $HOME_ETL/external/python/
python3.6 cnv_Excel2CSV.py

RESULT=$?

if [ $RESULT -ne 0 ]
then

    echo "Terminate error return code [$RESULT]"
    exit 1
fi

$HOME/Framework/shell/0_initial/initial_stg_from_lan.sh -f dwh_mapping_stg_V1.csv -Y
$HOME/Framework/shell/1_deploy/deploy_stg.sh -f STG_DWH_MAPPING_STG_V1.sql
$HOME/Framework/shell/stg/stg_load.sh -t STG_DWH_MAPPING_STG_V1 -d 19990101

$HOME/Framework/shell/0_initial/initial_stg_from_lan.sh -f REPO_ETL_RULES_UNIT_TEST_STD.csv -Y
$HOME/Framework/shell/1_deploy/deploy_stg.sh -f STG_REPO_ETL_RULES_UNIT_TEST_STD.sql
$HOME/Framework/shell/stg/stg_load.sh -t STG_REPO_ETL_RULES_UNIT_TEST_STD -d 19990101

$HOME/Framework/shell/0_initial/initial_stg_from_lan.sh -f REPO_ETL_RULES_UNIT_TEST_PER_TABLE.csv -Y
$HOME/Framework/shell/1_deploy/deploy_stg.sh -f STG_REPO_ETL_RULES_UNIT_TEST_PER_TABLE.sql
$HOME/Framework/shell/stg/stg_load.sh -t STG_REPO_ETL_RULES_UNIT_TEST_PER_TABLE -d 19990101

