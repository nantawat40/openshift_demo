-- THIS FILE Generate by G-Able Framework
-- COMMENT Generate by G-Able Framework @${NOW}

-- SELECTED DATABASE
USE ${DATABASE_NAME}_STG;

-- DROP TABLE IF FOUND
DROP TABLE IF EXISTS `${STG_TABLENAME}`;

-- CREATE TABLE STG
CREATE TABLE `${STG_TABLENAME}` (
--<EOF Fields>

    -- Standard Field by G-Able Framework
    STG_SOURCE_DT       VARCHAR(128)   DEFAULT NULL  COMMENT 'Generate this field by G-Able Framework for DateTime on Filename of Source',
    STG_SOURCE_FILE     VARCHAR(128)   DEFAULT NULL  COMMENT 'Generate this field by G-Able Framework for Filename of Source',
    STG_LOAD_DT     DATETIME       DEFAULT NOW() COMMENT 'Generate this field by G-Able Framework for DateTime Stage load'
) 
CHARACTER SET UTF8 
COLLATE UTF8_GENERAL_CI;
