DROP DATABASE IF EXISTS ${DATABASE_NAME}_STG;
DROP DATABASE IF EXISTS ${DATABASE_NAME}_DWH;
DROP DATABASE IF EXISTS ${DATABASE_NAME}_DM;

CREATE DATABASE         ${DATABASE_NAME}_STG CHARACTER SET = 'utf8' COLLATE = 'utf8_general_ci';
CREATE DATABASE         ${DATABASE_NAME}_DWH CHARACTER SET = 'utf8' COLLATE = 'utf8_general_ci';
CREATE DATABASE         ${DATABASE_NAME}_DM  CHARACTER SET = 'utf8' COLLATE = 'utf8_general_ci';

-- Owner object
DROP USER IF EXISTS stg@localhost, dwh@localhost, dm@localhost;

DROP USER IF EXISTS etl@localhost;
DROP USER IF EXISTS etl@'%';

CREATE USER stg@localhost IDENTIFIED BY '${DATABASE_STG_PASSWORD}';
CREATE USER dwh@localhost IDENTIFIED BY '${DATABASE_DWH_PASSWORD}';
CREATE USER  dm@localhost IDENTIFIED BY '${DATABASE_DM_PASSWORD}';

-- Process Data
CREATE USER etl@localhost IDENTIFIED BY '${DATABASE_ETL_PASSWORD}';
CREATE USER etl@'%'       IDENTIFIED BY '${DATABASE_ETL_PASSWORD}';

GRANT ALL privileges ON ${DATABASE_NAME}_STG.* TO stg@localhost;
GRANT ALL privileges ON ${DATABASE_NAME}_DWH.* TO dwh@localhost;
GRANT ALL privileges ON ${DATABASE_NAME}_DM.*  TO  dm@localhost;

-- DROP support statement Truncate Table STG_XXX
GRANT SELECT, INSERT, UPDATE, DELETE, DROP ON ${DATABASE_NAME}_STG.* TO etl@localhost;
GRANT SELECT, INSERT, UPDATE, DELETE       ON ${DATABASE_NAME}_DWH.* TO etl@localhost;
GRANT SELECT, INSERT, UPDATE, DELETE       ON ${DATABASE_NAME}_DM.*  TO etl@localhost;

GRANT SELECT, INSERT, UPDATE, DELETE, DROP ON ${DATABASE_NAME}_STG.* TO etl@'%';
GRANT SELECT, INSERT, UPDATE, DELETE       ON ${DATABASE_NAME}_DWH.* TO etl@'%';
GRANT SELECT, INSERT, UPDATE, DELETE       ON ${DATABASE_NAME}_DM.*  TO etl@'%';

-- Last version must move GRANT FILE to GLOBAL PRIVILEGES
GRANT FILE ON *.*  TO etl@localhost;
GRANT FILE ON *.*  TO etl@'%';

-- Support Deploy object related between STG <-> DWN or DWH <-> DM
GRANT SELECT                         ON ${DATABASE_NAME}_STG.* TO dwh@localhost;
GRANT SELECT                         ON ${DATABASE_NAME}_DWH.* TO  dm@localhost;

FLUSH PRIVILEGES;

-- #SHOW GRANTS FOR stg@localhost;
-- #SHOW GRANTS FOR dwh@localhost;
-- #SHOW GRANTS FOR  dm@localhost;

-- #SHOW GRANTS FOR etl@localhost;
