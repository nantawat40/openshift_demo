-- THIS FILE Generate by G-Able Framework
-- COMMENT Generate by G-Able Framework @2020-01-20 10:01:05

-- SELECTED DATABASE
USE EMPLOYEE_TESTING_STG;

-- DROP TABLE IF FOUND
DROP TABLE IF EXISTS `STG_EMPLOYEE`;

-- CREATE TABLE STG
CREATE TABLE `STG_EMPLOYEE` (
	EMPOYEE_NO 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',
	EMPOYEE_NAME 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',
	FIRSTNAME 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',
	LASTNAME 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',
	JOBGRADE 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',
	CONTRACT_TYPE 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',
	BIRTHDAY 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',
	STATUS 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',
	SEX 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',
	AGE 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',
	UPDATE_DT 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',
	CREATE_DT 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',


    -- Standard Field by G-Able Framework
    STG_SOURCE_DT       VARCHAR(128)   DEFAULT NULL  COMMENT 'Generate this field by G-Able Framework for DateTime on Filename of Source',
    STG_SOURCE_FILE     VARCHAR(128)   DEFAULT NULL  COMMENT 'Generate this field by G-Able Framework for Filename of Source',
    STG_LOAD_DT     DATETIME       DEFAULT NOW() COMMENT 'Generate this field by G-Able Framework for DateTime Stage load'
);
