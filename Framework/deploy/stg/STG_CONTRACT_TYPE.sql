-- THIS FILE Generate by G-Able Framework
-- COMMENT Generate by G-Able Framework @2020-01-20 12:04:07

-- SELECTED DATABASE
USE EMPLOYEE_TESTING_STG;

-- DROP TABLE IF FOUND
DROP TABLE IF EXISTS `STG_CONTRACT_TYPE`;

-- CREATE TABLE STG
CREATE TABLE `STG_CONTRACT_TYPE` (
	CONTRACT_TYPE 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',
	CONTRACT_NAME 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',
	DESCRIPTION 	VARCHAR(1024) 	DEFAULT NULL 	COMMENT 'Generate this field by G-Able Framework',


    -- Standard Field by G-Able Framework
    STG_SOURCE_DT       VARCHAR(128)   DEFAULT NULL  COMMENT 'Generate this field by G-Able Framework for DateTime on Filename of Source',
    STG_SOURCE_FILE     VARCHAR(128)   DEFAULT NULL  COMMENT 'Generate this field by G-Able Framework for Filename of Source',
    STG_LOAD_DT     DATETIME       DEFAULT NOW() COMMENT 'Generate this field by G-Able Framework for DateTime Stage load'
);
