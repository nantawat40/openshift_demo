#!/bin/bash

#--------------------------------------------------------------------
#              Copyright C - 2012 All rights reserved
#                        G-Able Thailand.
#
#       No part of this program may be reproduced or adapted in any
#             form or by any means, electronic or mechanical,
#       without permission from G-Able Thailand Thailand.
#          This program is confidential and may not be disclosed,
#          decompiled or reverse engineered without permission in
#             writing from G-Able Thailand Thailand.
#
#--------------------------------------------------------------------
# DESCRIPTION:
#    initial database and any object on platform data analysis (initiai_db)
#
#--------------------------------------------------------------------
# Revision   Date        CR No.   Author         Description
#====================================================================
#  0.2       16/01/2020  XXX      Suebsakul.A    Project Push to SEE, Your DATA
#
#
#
#


# Parameters
# -f [Filename.sql] : new initial database, tables and object for data processing

#Force run profile for support Job Schduler
. $HOME/.profile

# Load global variables and function library
GLOBAL_LIB_CONFIG=$HOME/Framework/configs/lib/global.inc
. $GLOBAL_LIB_CONFIG

EXEC_NAME="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")"
EXEC_PARA='-f <FILE CSV> [ -d "," -Y -L ]'
INITIAL='initial'
STAGE='stg'

# Load global variables and function library
. $MAIN_CONFIG

#Check number of argment
if [ $# -ne 1 -a $# -ne 2 -a $# -ne 3 -a $# -ne 4  -a $# -ne 5 ]
then
  _usage
else
  let i=1
  for p in $*;
  do
    case "$p" in
      "-f")
        FILE_CSV=${@:$((i+1)):1}
      ;;
      "-d")
        FILE_DELIMETER=${@:$((i+1)):1}
      ;;
      "-Y")
        FORCE_CONFIRM="YES"
      ;;
      "-L")
        MODE="LIST_FILE"
      ;;      
      *)
        if [ $((i%2)) -eq 1 ]
        then
          _usage
        fi
      ;;
    esac
    let i=i+1
  done;

  if [ "$MODE" == "LIST_FILE" ]
  then

    _logger "List File on landing : $LND_PATH" $LOG_FILE
    ls -l $LND_PATH/

    exit 1
  fi

  if [ -z $FILE_CSV ]
  then
    _usage
  fi

  if [ -z $FORCE_CONFIRM ]
  then
    FORCE_CONFIRM="NO"
  fi

  if [ -z $FILE_DELIMETER ]
  then
    #_usage

    # set default when use not define
    FILE_DELIMETER=","
  fi


  
fi

# echo $FILE_DDL

LOG_PATH=$LOG_PATH/$INITIAL
LOG_FILE=$LOG_PATH/${FILE_CSV}.log
INPUT_PATH=$INPUT_PATH/$STAGE

if [ ! -d $LOG_PATH ]
then
  mkdir $LOG_PATH
fi



#Define start time
START_TIME="$(date +%s%N)"

clear
_logger "Started FILE_CSV=${FILE_CSV} DELIMETER=${FILE_DELIMETER}" $LOG_FILE




#Add Full path
FULLPATH_FILE_CSV=$LND_PATH/$FILE_CSV

if [ ! -f $FULLPATH_FILE_CSV ]
then    
  _logger "Unable to access file: $FULLPATH_FILE_CSV" $LOG_FILE
  exit $ST_UNEXPECTED_ERROR

else
  # clear old temp file
  rm -f $TMP_PATH/create_table_stg_out.sql
fi

# full path tempalte file
FULLPATH_TEMPLATE_STG_TABLE=$CREATE_PATH/stg_table.sql
FULLPATH_TEMPLATE_STG_CONF=$CREATE_PATH/STG_template.conf
FULLPATH_TEMPLATE_STG_CONTROL=$CREATE_PATH/STG_template.control


if [ ! -f $FULLPATH_TEMPLATE_STG_TABLE ]
then    
  _logger "Unable to access file: $FULLPATH_TEMPLATE_STG_TABLE" $LOG_FILE
  exit $ST_UNEXPECTED_ERROR
fi

#Get Header
#--------------------------------------
# STEP 1. Get Header of File CSV
#--------------------------------------
NOW=$(date '+%F %T')
HEADER_FILE="$(head -1 $FULLPATH_FILE_CSV)"
FILENAME=$(_getFilename $FULLPATH_FILE_CSV)
EXTENSION=$(_getExtension $FULLPATH_FILE_CSV)
DEFAULT_INPUT_FILE=${FILENAME}"_YYYYMMDD.csv"


# Check prefix is STG and reformat follow as STG_<FILENAME>
if [ ${FILENAME} == STG_* ]
then
  FILENAME_DDL=${FILENAME^^}.sql
  
else
  FILENAME=STG_${FILENAME^^}
  FILENAME_DDL=${FILENAME^^}.sql
fi

# echo "$FILENAME"
# echo "$TODAY"

_logger "Copy File $FULLPATH_TEMPLATE_STG_TABLE to $TMP_PATH/$FILENAME.sql" $LOG_FILE
cp $FULLPATH_TEMPLATE_STG_TABLE $TMP_PATH/$FILENAME.sql

FULLPATH_FILE_DDL=$DEPLOY_PATH/stg/$FILENAME_DDL

#${FILENAME^^} is The ^ operator converts to uppercase, while , converts to lowercase.
sed -e "s/\${STG_TABLENAME\}/${FILENAME^^}/" \
    -e "s/\${DATABASE_NAME\}/${DATABASE_NAME}/" \
    -e "s/\${NOW\}/$NOW/g" $TMP_PATH/$FILENAME.sql > $TMP_PATH/create_table_stg_out.sql

# cat $TMP_PATH/create_table_stg_out.sql

#Transform String to Array
IFS=$FILE_DELIMETER read -r -a arr_HEADER_FILE <<< "$HEADER_FILE"
Columns_No="${#arr_HEADER_FILE[@]}"

_logger "-------------------------------" $LOG_FILE
_logger "Header[$Columns_No] : $HEADER_FILE" $LOG_FILE

# Add header field follow as file csv
COUNT=0
FIELDS=""
NEW_HEADER_FILE=""

# Set Default when mission loading from library
if [ -z $MAX_STG_LENGTH__FIELDS ]
then
  MAX_STG_LENGTH__FIELDS=256
fi

_logger "-------------------------------" $LOG_FILE
for element in "${arr_HEADER_FILE[@]}"
do
    # echo "$element"
    COUNT=`expr $COUNT + 1`

    if [ $COUNT -le 9 ]
    then
      COUNT=" $COUNT"
    fi

    # sugguest new field name
    # - remove ",()
    # - Upper case
    # - replace space to _    
    element=$(echo $element | tr -d [\(\)\"] | tr '[:lower:]' '[:upper:]' | tr ' ' '_' )

    _logger "Column[$COUNT] : $element" $LOG_FILE

    # Keep new Header
    NEW_HEADER_FILE=$(echo $NEW_HEADER_FILE$element,)

    # Generate field from header
    # ADD_FIELD="\t$element \tVARCHAR(1024) \tDEFAULT NULL \tCOMMENT 'Generate this field by G-Able Framework'"

    # add more lenght for REPO Config
    if [ "$element" == "SQL_STATEMENT" ]
    then
      ADD_FIELD="\t$element \tVARCHAR(4096) \tDEFAULT NULL \tCOMMENT 'Generate this field by G-Able Framework'"
    else
      ADD_FIELD="\t$element \tVARCHAR($MAX_STG_LENGTH__FIELDS) \tDEFAULT NULL \tCOMMENT 'Generate this field by G-Able Framework'"
    fi
        
    FIELDS="${FIELDS}$ADD_FIELD,\n"
done
_logger "-------------------------------" $LOG_FILE

# Remove new line & comma(,)
NEW_HEADER_FILE=$(echo "$NEW_HEADER_FILE" | tr -d '\r\n' )
NEW_HEADER_FILE=${NEW_HEADER_FILE::-1}


# Check duplicate header field
uniqueNum=$(printf '%s\n' "${arr_HEADER_FILE[@]}"|awk '!($0 in seen){seen[$0];c++} END {print c}')
#(( uniqueNum != ${#arr_HEADER_FILE[@]} )) && echo "Found duplicate(s) header on file:$FILE_CSV"

# Error when number of uquiqe <> number of columns
if [ $uniqueNum != ${#arr_HEADER_FILE[@]} ] 
then
  Msg=$(printf '%s,\n' "${arr_HEADER_FILE[@]}"|awk '!($0 in seen){seen[$0];next} 1')
  
  Msg=${Msg::-1}

  _logger "ERROR Found duplicate(s) header on file:$FILE_CSV" $LOG_FILE
  _logger "-------------------------------" $LOG_FILE
  _logger "$Msg" $LOG_FILE
  _logger "-------------------------------" $LOG_FILE

  exit $ST_UNEXPECTED_ERROR
fi

# Remove string & new line on position start & end string
# FIELDS=$(echo "$FIELDS" | sed -e 's/^,\\n//' -e 's/,\\n$//')
FIELDS=$(echo "$FIELDS" | sed -e 's/^,\\n//')

#FIELDS=$(echo -e "$FIELDS")

_logger "Generate File DDL : $TMP_PATH/create_table_stg_out.sql" $LOG_FILE
cp $TMP_PATH/create_table_stg_out.sql $TMP_PATH/create_table_stg_out_temp1.sql

_logger "Replace list of Fields to file ddl" $LOG_FILE
sed -e "s/--<EOF Fields>/${FIELDS}/" \
        $TMP_PATH/create_table_stg_out_temp1.sql > \
        $TMP_PATH/create_table_stg_out.sql

_logger "Copy File : $TMP_PATH/create_table_stg_out.sql to $FULLPATH_FILE_DDL" $LOG_FILE



_logger "Generate File Config STG : $TMP_PATH/create_table_stg_out.sql" $LOG_FILE
cp $TMP_PATH/create_table_stg_out.sql $TMP_PATH/create_table_stg_out_temp1.sql
mv $TMP_PATH/create_table_stg_out.sql $TMP_PATH/$FILENAME.sql



# Generate config for stg load
CONF_FILE=$CONF_PATH/stg/$FILENAME.conf

if [ -f $CONF_FILE ]
then
  cp $CONF_FILE $TMP_PATH/$FILENAME.conf
  . $CONF_FILE

else
  cp $FULLPATH_TEMPLATE_STG_CONF $TMP_PATH/$FILENAME.conf
fi

STG_TABLE_NAME=$FILENAME
#. $TMP_PATH/$FILENAME.conf

_logger "-------------------------------" $LOG_FILE
_logger "Input of STG Load Config" $LOG_FILE
_logger "-------------------------------" $LOG_FILE
# get variable from template file
cat $FULLPATH_TEMPLATE_STG_CONF | grep "=" | grep -v ^'#' | grep -v 'INPUT_TYPE\|INPUT_FREQUENCY' | awk -F'=' '{print $1}' | while read variable
do

  DEFAULT=$(eval echo \${$variable})

  if [ "$FORCE_CONFIRM" != "YES" ]
  then
    echo "$variable : [$DEFAULT] " | tr -d '\r\n'
    #read inputKeyboard
    read KEYINPUT < /dev/tty
  fi

  if [ -z $KEYINPUT ]
  then

    if   [ -z $DEFAULT ] && [ "$variable" = "GROUP_NAME" ]
    then
      DEFAULT="TBD"
    elif [ -z $DEFAULT ] && [ "$variable" = "INPUT_FILE" ]
    then
      DEFAULT=$DEFAULT_INPUT_FILE
    elif [ -z $DEFAULT ] && [ "$variable" = "FILE_DELIMETER" ]
    then
      DEFAULT=$FILE_DELIMETER      
    fi

    NEW_VALUE=${DEFAULT}
  else
    NEW_VALUE=${KEYINPUT}
  fi

  # replace value on file conf
  sed -i "s/$variable=\".*\"/$variable=\"$NEW_VALUE\"/" $TMP_PATH/$FILENAME.conf

done

_logger "-------------------------------" $LOG_FILE
# display contect in fiel temp
cat $TMP_PATH/$FILENAME.conf
_logger "-------------------------------" $LOG_FILE

KEYCONFIRM="N/A"
until [ "$KEYCONFIRM" == "Y" -o  "$KEYCONFIRM" == "y" -o "$KEYCONFIRM" == "N" -o  "$KEYCONFIRM" == "n" -o "$FORCE_CONFIRM" == "YES" ]
do
  echo "-------------------------------"
  read -p 'Confrim Generate Deploy/Config file? (Y/N) ' KEYCONFIRM < /dev/tty
  echo "-------------------------------"
done

# When use key 'Y' or 'y'
# Generate file deploy & config
if [ "$KEYCONFIRM" == "Y" -o "$KEYCONFIRM" == "y" -o "$FORCE_CONFIRM" == "YES" ]
then
  #_logger "Create File DDL : $FULLPATH_FILE_DDL from File landing : $FULLPATH_FILE_CSV" $LOG_FILE
  _logger "Create File DDL : $FULLPATH_FILE_DDL" $LOG_FILE
  _logger "Create File CONFIG : $CONF_FILE" $LOG_FILE
    
  # move temp to deploy and config script stg
  mv $TMP_PATH/$FILENAME.sql $FULLPATH_FILE_DDL
  mv $TMP_PATH/$FILENAME.conf $CONF_FILE

  _logger "Create Table by script : \$HOME_ETL/shell/1_deploy/deploy_stg.sh -f $FILENAME_DDL" $LOG_FILE

else
  _logger "Delete Temp File DDL : $TMP_PATH/$FILENAME.sql" $LOG_FILE
  _logger "Delete Temp File CONFIG STG Load : $TMP_PATH/$FILENAME.conf" $LOG_FILE

  # delete temp files
  rm $TMP_PATH/$FILENAME.sql
  rm $TMP_PATH/$FILENAME.conf
fi

KEYCONFIRM="N/A"
until [ "$KEYCONFIRM" == "Y" -o  "$KEYCONFIRM" == "y" -o "$KEYCONFIRM" == "N" -o  "$KEYCONFIRM" == "n" -o "$FORCE_CONFIRM" == "YES" ]
do
  echo "-------------------------------"
  read -p "Confrim Copy file: ${FILE_CSV} to Input path? (Y/N) " KEYCONFIRM < /dev/tty
  echo "-------------------------------"
done

# Copy file csv to input path
if [ "$KEYCONFIRM" == "Y" -o "$KEYCONFIRM" == "y" -o "$FORCE_CONFIRM" == "YES" ]
then

  FILE_CSV_COPY=${FILE_CSV/.csv/_${TEST_DATE_YYYYMMDD}.csv}
  FILE_CON_COPY=${FILE_CSV/.csv/_${TEST_DATE_YYYYMMDD}.control}

  # create temp file
  cp $FULLPATH_TEMPLATE_STG_CONTROL $TMP_PATH/$FILE_CSV.control

  # replace value on file conf
  sed -i "s/\${FILENAME_CSV}/$FILE_CSV_COPY/" $TMP_PATH/$FILE_CSV.control
  sed -i "s/\${HEADER_FILE}/$HEADER_FILE/" $TMP_PATH/$FILE_CSV.control
  sed -i "s/\${NEW_HEADER_FILE}/$NEW_HEADER_FILE/" $TMP_PATH/$FILE_CSV.control
  
  _logger "Copy File:$FILE_CSV_COPY to Input Path:$INPUT_PATH" $LOG_FILE    

  # add blank line to EOF when missing blank line
  if ! file_ends_with_newline $FULLPATH_FILE_CSV
  then
      echo "" >> $FULLPATH_FILE_CSV
  fi

  cp $FULLPATH_FILE_CSV $INPUT_PATH/${FILE_CSV_COPY}

  # Get Number of line of input file
  # LINE_NO=$(wc -l $INPUT_PATH/${FILE_CSV_COPY} | awk '{print $1}' )
  LINE_NO=$(cat $INPUT_PATH/${FILE_CSV_COPY} | python3.6 -c "import csv; import sys; print(sum(1 for i in csv.reader(sys.stdin)))")
  sed -i "s/\${LINE_NO}/$LINE_NO/" $TMP_PATH/$FILE_CSV.control    
  #cat $TMP_PATH/$FILE_CSV.control
  _logger "Number of line File: $FILE_CSV_COPY = ${LINE_NO} line(s)" $LOG_FILE    
  _logger "Copy File: $FILE_CON_COPY to Input Path:$INPUT_PATH" $LOG_FILE  
  cp $TMP_PATH/$FILE_CSV.control $INPUT_PATH/${FILE_CON_COPY}

fi

#--------------------------------------
# Summary
#--------------------------------------
ELAPSED=$(_calcElapsedTime $START_TIME)

_logger "Elapsed $ELAPSED sec." $LOG_FILE
_logger "STATUS=$ST_CODE $ST_NAME" $LOG_FILE
_logger "END" $LOG_FILE
echo " " >> $LOG_FILE

#--------------------------------------
# Verify result command for support Job Schduler
#--------------------------------------
#for support Job Schduler
if [ $ST_CODE -eq $ST_RECONCILE_REC_ERROR -o $ST_CODE -eq $ST_RECONCILE_SUM_ERROR ]
then
  exit $ST_COMPLETE
else
  exit $ST_CODE
fi

